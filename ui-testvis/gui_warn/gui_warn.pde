import java.awt.Point;

int w, h, max_width, curr_width, width_speed, gradient_fine, strength;
int spot_strength, spot_height, spot_fine, spot_width_reduce;
float spot_tilt_factor;
int framerate = 30;

// Demo stuff
boolean right = true, spotlight_cone = false;
Point light_pos;

void setup () {
  w = 800;
  h = 400;
  frameRate(framerate);
  size (w, h);
  curr_width = 0;
  light_pos = new Point(w / 2, h / 2);
  customize ();
}

void customize () {
  width_speed = 20;
  gradient_fine = 4;
  max_width = w / 10;
  strength = 8;
  spot_strength = 10;
  spot_height = 20;
  spot_fine = 10;
  spot_tilt_factor = .5;
  spot_width_reduce = 10;
}

void draw () {
  background (0, 0, 0);

  drawAlert ();
  moveSpotlight ();
  drawSpotlight ();
}

void moveSpotlight () {
  if (mousePressed && (mouseButton == LEFT)) {
    light_pos.x = mouseX;
    light_pos.y = mouseY;
  }
}

void drawAlert () {
  ellipseMode (CORNER);

  if (curr_width > max_width || curr_width < 0) {
    width_speed *= -1;
  }
  curr_width += width_speed * (60.0 / framerate);

  for (int i = 0; i < curr_width; i += gradient_fine) {
    noStroke ();
    fill (255, 0, 0, strength);
    int startx = right ? w - i : -i;
    rect (startx, 0, i * 2, h);
  }
}

void drawSpotlight () {

  ellipseMode (CENTER);
  noStroke ();

  int mWidth = max_width * 3;

  for (int y = light_pos.y; y > -10; y-= spot_height) {
    mWidth -= spot_width_reduce;
    float alpha = spot_strength * (mWidth / (max_width * 3.0));
    fill (255, 255, 255, alpha);
    for (int r = 0; r < mWidth; r += spot_fine) {
      ellipse (light_pos.x, y, r, r * spot_tilt_factor);
    }

    if (!spotlight_cone)
      break;
  }
}

void keyPressed () {
  if (keyCode == LEFT)
    right = false;
  else if (keyCode == RIGHT)
    right = true;

  if (keyCode == 'S')
    spotlight_cone = !spotlight_cone;
}

