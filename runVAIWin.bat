@echo off
rem This is a startup script for the ProcessMonitor class of Viewpoints AI Installation.
 
setlocal

set HERE=%~sp0

if not defined VAI_MAIN (
   set VAI_MAIN=Viewpoints.Monitoring.ProcessMonitor
)
if not defined VAI_CLASSPATH (
   set VAI_CLASSPATH=%HERE%bin\
)

echo Running: java -cp "%VAI_CLASSPATH%" %VAI_MAIN
java -cp "%VAI_CLASSPATH%" "%VAI_MAIN%"