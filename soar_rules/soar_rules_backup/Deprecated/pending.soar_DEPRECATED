# vary_body_symmetry : DONE AS A PART OF vary.soar

sp { propose*vp-body_symmetric-vary
  (state <s> ^name viewpoints-predicates-vary)
  -->
  (<s> ^operator <o> + =)
  (<o> ^name vp-body_symmetric-vary)
}

sp { apply*vp-body_symmetric-vary*body_symmetric
  (state <s> ^name viewpoints-predicates-vary
             ^operator.name vp-body_symmetric-vary
             ^io.output.link <olink>
             ^superstate.superstate.superstate <ssss>
             ^history-index <hindex>)
  (<ssss> ^working-copy.all_predicate_history.node <node>
          ^body_symmetric <body_symmetric-table>)
  (<node> ^body_symmetric <wc-body_symmetric>)
  (<body_symmetric-table> ^value <wc-body_symmetric>
                          ^opposite <varied-body_symmetric>)
  -->
  (<node> ^body_symmetric <wc-body_symmetric> -)
  (<node> ^body_symmetric <varied-body_symmetric>)
  (write (crlf) |VARY BODY_SYMMETRIC - Node - | <node>
  | - OLD BODY_SYMMETRIC - | <wc-body_symmetric>
  | - NEW BODY_SYMMETRIC - | <varied-body_symmetric>)
}

# Vary Size

sp { propose*vp-size-vary
    (state <s> ^name viewpoints-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name vp-size-vary)
}

sp { apply*vp-size-vary*size
    (state <s> ^name viewpoints-predicates-vary
               ^operator.name vp-size-vary
               ^io.output.link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^working-copy.all_predicate_history.node <node>
            ^size <size-table>)
    (<node> ^size <wc-size>)
    (<size-table> ^value <wc-size>
                            ^opposite <varied-size>)
    -->
    (<node> ^size <wc-size> -)
    (<node> ^size <varied-size>)
    (write (crlf) |VARY size - Node - | <node>
    | - OLD size - | <wc-size>
    | - NEW size - | <varied-size>) 
}


# Vary_facing_human

sp { propose*gesture-facing-vary-human
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-facing-vary-human)
}

sp { apply*gesture-facing-vary-human*facing-human
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-facing-vary-human
               ^superstate.superstate.superstate <ssss>)
    (<ssss> ^working-copy.all_predicate_history.node <node>
            ^facing <facing-table>)
    (<node> ^facing <wc-facing>)
    (<facing-table> ^value <wc-facing>
                    ^opposite <varied-facing>)
    -->
    (<node> ^facing <wc-facing> -)
    (<node> ^facing <varied-facing>)
    (write (crlf) |VARY HUMAN FACING- WC Node - | <node>
    | - OLD HUMAN FACING - | <wc-facing>
    | - NEW HUMAN FACING - | <varied-facing>)
}

# Debugging for vary_facing_human
# sp { select*gesture-facing-vary-human
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-facing-vary-human)
#     -->
#     (<s> ^operator <o> >) 
# }


# Reflect limb same set
sp { propose*gesture-reflect-limb-same-set-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-reflect-limb-same-set-vary)
}

sp { apply*gesture-reflect-limb-same-set-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-reflect-limb-same-set-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.reflect-limb.limb-set SAME)
    (<node> ^output <olink>
            ^response.vary.reflect-limb.limb-set SAME)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}

# Debugging
# sp { select*gesture-reflect-limb-same-set-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-reflect-limb-same-set-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }

# Reflect limb other set

sp { propose*gesture-reflect-limb-other-set-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-reflect-limb-other-set-vary)
}

sp { apply*gesture-reflect-limb-other-set-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-reflect-limb-other-set-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.reflect-limb.limb-set OTHER)
    (<node> ^output <olink>
            ^response.vary.reflect-limb.limb-set OTHER)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}


# Debugging
# sp { select*gesture-reflect-limb-other-set-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-reflect-limb-other-set-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }

# Both limb same set

sp { propose*gesture-both-limb-same-set-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-both-limb-same-set-vary)
}

sp { apply*gesture-both-limb-same-set-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-both-limb-same-set-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.both-limb.limb-set SAME)
    (<node> ^output <olink>
            ^response.vary.both-limb.limb-set SAME)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}

# Debugging
# sp { select*gesture-both-limb-same-set-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-both-limb-same-set-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }


# Both limb other set

sp { propose*gesture-both-limb-other-set-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-both-limb-other-set-vary)
}

sp { apply*gesture-both-limb-other-set-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-both-limb-other-set-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.both-limb.limb-set OTHER)
    (<node> ^output <olink>
            ^response.vary.both-limb.limb-set OTHER)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}


# Debugging
# sp { select*gesture-both-limb-other-set-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-both-limb-other-set-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }


# Add jumping

sp { propose*gesture-add-jumping-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-add-jumping-vary)
}

sp { apply*gesture-add-jumping-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-add-jumping-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.jumping TRUE)
    (<node> ^output <olink>
            ^response.vary.jumping TRUE)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}

# Add crouching

sp { propose*gesture-add-crouching-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-add-crouching-vary)
}

sp { apply*gesture-add-crouching-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-add-crouching-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.crouching TRUE)
    (<node> ^output <olink>
            ^response.vary.crouching TRUE)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}

# Add spin

sp { propose*gesture-add-spin-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-add-spin-vary)
}

sp { apply*gesture-add-crouching-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-add-spin-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.spin TRUE)
    (<node> ^output <olink>
            ^response.vary.spin TRUE)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}


#Debugging
# sp { select*gesture-add-spin-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-add-spin-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }


# Add reflected direction circular movement around the center

sp { propose*gesture-add-reflected-circular-around-center-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-add-reflected-circular-around-center-vary)
}

sp { apply*gesture-add-reflected-circular-around-center-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-add-reflected-circular-around-center-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.reflected-circular-around-center-vary TRUE)
    (<node> ^output <olink>
            ^response.vary.reflected-circular-around-center-vary TRUE)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}


#Debugging
# sp { select*gesture-add-reflected-circular-around-center-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-add-reflected-circular-around-center-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }


# Repeat N times

sp { propose*gesture-add-repeat-n-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-add-repeat-n-vary)
}

sp { apply*gesture-add-repeat-n-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-add-repeat-n-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.gesture-add-repeat-n-vary TRUE)
    (<node> ^output <olink>
            ^response.vary.gesture-add-repeat-n-vary TRUE)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}

#Debugging
# sp { select*gesture-add-repeat-n-vary
#     (state <s> ^name gesture-predicates-vary)
#     (<s> ^operator <o> +)
#     (<o> ^name gesture-add-repeat-n-vary)
#     -->
#     (<s> ^operator <o> >) 
#     }


# Vary distance from human

sp { propose*gesture-dist-human-vary
    (state <s> ^name gesture-predicates-vary)
    -->
    (<s> ^operator <o> + =)
    (<o> ^name gesture-dist-human-vary)
}

sp { apply*gesture-dist-human-vary
    (state <s> ^name gesture-predicates-vary
               ^operator.name gesture-dist-human-vary
               ^io.output-link <olink>
               ^superstate.superstate.superstate <ssss>
               ^history-index <hindex>)
    (<ssss> ^varied-features-count <varied-features-count>
            ^history.node <node>)
    (<node> ^index <hindex>)
    -->
    (<olink> ^outputs.vary.gesture-dist-human-vary TRUE)
    (<node> ^output <olink>
            ^response.vary.gesture-dist-human-vary TRUE)
    (<ssss> ^varied-features-count <varied-features-count> -)
    (<ssss> ^varied-features-count (+ <varied-features-count> 1))
}

#Debugging
sp { select*gesture-dist-human-vary
    (state <s> ^name gesture-predicates-vary)
    (<s> ^operator <o> +)
    (<o> ^name gesture-dist-human-vary)
    -->
    (<s> ^operator <o> >) 
    }

