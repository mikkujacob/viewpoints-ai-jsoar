#Rules used to update the internal variables used in the AI

sp { propose*update*internal-variables
	(state <s> ^name viewpoints-agent)
	(<s> ^phase update-internal-variables)
	-->
	(<s> ^operator <o> +)
	(<o> ^name update-agent)
}

sp { apply*update*internal-variables*first*run
	(state <s> ^name viewpoints-agent
			   ^operator.name update-agent
			   ^io.input-link <ilink>
			   ^history <hist>
			   ^history-count <count>
			   ^history-index <hindex>
			   ^history-mode <hmode>
			   ^phase <p>
			   ^response-mode <rmode>
			   ^random-number-two <rand2>
			   ^varied-features-count <varied-features-count>
			   ^is-pattern-lhs-set <is-pattern-lhs-set>
			   ^is-pattern-rhs-set <is-pattern-rhs-set>
			   ^is-response-executed <is-resp-exec>
			   ^current-vary-predicate <current-vary>
			   ^current-vary-in-progress <curr-vary-in-progress>
			   ^personality <persona>
			   )
	(<s> ^history-count 0)
	(<persona> ^input-emotion <inp-old-emotion>)
	(<ilink> ^gesture <gesture>)
	(<gesture> ^emotion <emote>)
	-->
	(<s> ^history-count <count> -)
	(<s> ^history-count (+ <count> 1))
	(<s> ^history-mode <hmode> -)
	(<s> ^history-mode null)
	(<s> ^is-pattern-lhs-set <is-pattern-lhs-set> -)
	(<s> ^is-pattern-lhs-set FALSE)
	(<s> ^is-pattern-rhs-set <is-pattern-rhs-set> -)
	(<s> ^is-pattern-rhs-set FALSE)
	(<s> ^is-response-executed <is-resp-exec> -)
	(<s> ^is-response-executed FALSE)
	(<hist> ^node <node>)
	(<node> ^input (deep-copy <ilink>)
		 	^index <count>)
	(<s> ^random-number-two <rand2> -)
	(<s> ^random-number-two 0)
	(<s> ^varied-features-count <varied-features-count> -)
	(<s> ^varied-features-count 0)
	(<s> ^history-index <hindex> -)
	(<s> ^history-index <count>)
	(<s> ^phase <p> -)
	(<s> ^phase choose-personality-mode)
	(<s> ^response-mode <rmode> -)
	(<s> ^response-mode null)
	(<s> ^current-vary-predicate <current-vary> -)
	(<s> ^current-vary-predicate START_VARY)
	(<s> ^current-vary-in-progress <curr-vary-in-progress> -)
	(<s> ^current-vary-in-progress FALSE)
	(<persona> ^input-emotion <inp-old-emotion> -)
	(<persona> ^input-emotion <emote>)
	(write (crlf) | REACHED APPLY UPDATE INTERNAL VARIABLES FIRST RUN |)
}

# Split apply update Part 1
sp { apply*update*internal-variables*normal*part*one
	(state <s> ^name viewpoints-agent
			   ^operator.name update-agent
			   ^io.input-link <ilink>
			   ^history <hist>
			   ^history-count <count>
			   ^history-index <hindex>
			   ^history-mode <hmode>
			   ^phase <p>
			   ^response-mode <rmode>
			   )
	(<s> ^history-count > 0)
	-->
	(<s> ^history-count <count> -)
	(<s> ^history-count (+ <count> 1))
	(<s> ^history-mode <hmode> -)
	(<s> ^history-mode null)
	(<hist> ^node <node>)
	(<node> ^input (deep-copy <ilink>)
		 	^index <count>)
	(<s> ^history-index <hindex> -)
	(<s> ^history-index <count>)
	(<s> ^phase <p> -)
	(<s> ^phase create-pattern-rhs)
	(<s> ^response-mode <rmode> -)
	(<s> ^response-mode null)
	(write (crlf) | REACHED APPLY UPDATE INTERNAL VARIABLES NORMAL PART 1 |)
}

# Split apply update Part 2
sp { apply*update*internal-variables*normal*part*two
	(state <s> ^name viewpoints-agent
			   ^operator.name update-agent
			   ^varied-features-count <varied-feature-count>
			   ^is-response-executed <is-resp-exec>
			   ^working-copy <wc>
			   ^current-vary-predicate <current-vary>
			   ^current-vary-in-progress <curr-vary-in-progress>
			   ^new-gesture-selected <new-gesture-selected>
			   )
	(<s> ^history-count > 0)
	-->
	(<s> ^working-copy <wc> -)
	(<s> ^working-copy null)
	(<s> ^current-vary-predicate <current-vary> -)
	(<s> ^current-vary-predicate START_VARY)
	(<s> ^current-vary-in-progress <curr-vary-in-progress> -)
	(<s> ^current-vary-in-progress FALSE)
	(<s> ^is-response-executed <is-resp-exec> -)
	(<s> ^is-response-executed FALSE)
	(<s> ^varied-features-count <varied-features-count> -)
	(<s> ^varied-features-count 0)
	(<s> ^new-gesture-selected <new-gesture-selected> -)
	(<s> ^new-gesture-selected FALSE)
	(write (crlf) | REACHED APPLY UPDATE INTERNAL VARIABLES NORMAL PART 2 |)
}

# Split apply update Part 3
sp { apply*update*internal-variables*normal*part*three
	(state <s> ^name viewpoints-agent
			   ^operator.name update-agent
			   ^io.input-link <ilink>
			   ^personality <persona>
			   ^emotion-selected <selected>
			   )
	(<s> ^history-count > 0)
	(<ilink> ^gesture.emotion <emote>)
	(<persona> ^age <agecount>
			   ^input-emotion <inp-old-emotion>
			   ^cross-threshold <cross>
			   ^output-emotion <out-old-emotion>)
	-->
	(<s> ^emotion-selected <selected> -)
	(<s> ^emotion-selected FALSE)
	(<persona> ^age <agecount> -)
	(<persona> ^age (+ <agecount> 1))
	(<persona> ^input-emotion <inp-old-emotion> -)
	(<persona> ^input-emotion <emote>)
	(<persona> ^output-emotion <out-old-emotion> -)
	(<persona> ^output-emotion NONE)
	(<persona> ^cross-threshold <cross> -)
	(<persona> ^cross-threshold 0)
	(write (crlf) | REACHED APPLY UPDATE INTERNAL VARIABLES NORMAL PART 3 |)
}