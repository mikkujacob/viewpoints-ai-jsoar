#Rules for executing a 'emotion' response - sad/joy/fear/angry.

sp { propose*apply*emotion*response
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^output-emotion NONE)
	-->
	(<s> ^operator <o> + >)
	(<o> ^name select-output-emotion)
	(write (crlf) | REACHED PROPOSE APPLY EMOTION RESPONSE|)
}

#Angry to angry/fear
sp { apply*select*output*emotion*angry*arousal
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion ANGRY)
	(<person> ^output-emotion <output-emotion>)
	(<person> ^mood arousal)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion ANGRY)
	(write (crlf) | REACHED APPLY EMOTION CHANGE ANGRY TO ANGRY |)
}

sp { apply*select*output*emotion*angry*pleasure
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion ANGRY)
	(<person> ^output-emotion <output-emotion>)
	(<person> ^mood pleasure)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion FEAR)
	(write (crlf) | REACHED APPLY EMOTION CHANGE ANGRY TO FEAR |)
}

#Fear to  angry/joy
sp { apply*select*output*emotion*fear*arousal
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion FEAR)
	(<person> ^output-emotion <output-emotion>)
	(<person> ^mood arousal)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion ANGRY)
	(write (crlf) | REACHED APPLY EMOTION CHANGE FEAR TO ANGRY |)
}

sp { apply*select*output*emotion*fear*pleasure
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion FEAR)
	(<person> ^output-emotion <output-emotion>)
	(<person> ^mood pleasure)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion JOY)
	(write (crlf) | REACHED APPLY EMOTION CHANGE FEAR TO JOY |)
}

#sad to sad/joy
sp { apply*select*output*emotion*sad*arousal
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion SAD)
	(<person> ^output-emotion <output-emotion>)
	(<person> ^mood arousal)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion SAD)
	(write (crlf) | REACHED APPLY EMOTION CHANGE SAD TO SAD |)
}

sp { apply*select*output*emotion*sad*pleasure
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion SAD)
	(<person> ^output-emotion <output-emotion>)
	(<person> ^mood pleasure)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion JOY)
	(write (crlf) | REACHED APPLY EMOTION CHANGE SAD TO JOY |)
}

#joy to joy
sp { apply*select*output*emotion*love*joy
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^operator.name select-output-emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^input-emotion JOY)
	(<person> ^output-emotion <output-emotion>)
	-->
	(<person> ^output-emotion <output-emotion> -)
	(<person> ^output-emotion JOY)
	(write (crlf) | REACHED APPLY EMOTION CHANGE JOY TO JOY |)
}

#Following propose and select takes care of selecting best emotion. 
sp { propose*select*best*emotion*response
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<ss> ^emotion-selected FALSE)
	(<person> ^output-emotion <> NONE)
	-->
	(<s> ^operator <o> + >)
	(<o> ^name select-best-emotion-output)
	(write (crlf) | REACHED PROPOSE SELECT BEST EMOTION RESPONSE|)
}

sp { apply*select*best*emotion*response
	(state <s> ^name execute-response
			   ^response-mode emotion
			   ^operator.name select-best-emotion-output
			   ^superstate <ss>
			   ^history-index <hindex>)
	(<ss> ^history.node <node1>
		  ^personality <person>)
	(<ss> ^history <histo>)
	(<ss> ^emotion-selected <emotion-select>)
	(<person> ^output-emotion <out-emotion>)
	(<node1> ^index <hindex>
		^input.gesture <inp-gesture>)
    -->
	(<node1> ^output (deep-copy (select-best-emotion <inp-gesture> <histo> <out-emotion>))
		    ^response.emotion.value TRUE)
	(<ss> ^emotion-selected <emotion-select> -)
	(<ss> ^emotion-selected TRUE)
	(write (crlf) | REACHED APPLY SELECT BEST EMOTION RESPONSE|)
}

#Following propose and select throw the output out... 
sp { propose*output*best*emotion*response
	(state <s> ^name execute-response
			   ^response-mode emotion
			    ^superstate <ss>)
	(<ss> ^personality <person>)
	(<person> ^output-emotion <> NONE)
	(<ss> ^emotion-selected TRUE)
	-->
	(<s> ^operator <o> + >)
	(<o> ^name output-best-emotion)
	(write (crlf) | REACHED PROPOSE OUTPUT BEST EMOTION RESPONSE|)
}

sp { apply*output*best*emotion*response
	(state <s> ^name execute-response
			   ^response-mode emotion
			   ^operator.name output-best-emotion
			   ^io.output-link <olink>
			   ^superstate <ss>
			   ^history-index <hindex>)
	(<ss> ^is-response-executed <is-resp-exec>
		^history.node <node1>)
	(<node1> ^index <hindex>
		^output <out-gesture>)
	(<out-gesture> ^emotion <emotion>)
    -->
	(<olink> ^outputs <outs>)
	(<outs> ^emotion <emo>)
	(<emo> ^value <emotion>
		   ^gesture <out-gesture>)
	(<ss> ^is-response-executed <is-resp-exec> -)
	(<ss> ^is-response-executed TRUE)
	(write (crlf) | REACHED APPLY OUTPUT BEST EMOTION RESPONSE| <out-gesture> (crlf))
}
