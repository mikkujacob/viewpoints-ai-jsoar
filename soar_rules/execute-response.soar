#Rules for executing a response to an input gesture

# proposes execute-response, which is used in a substate by whichever
# 	mode is selected. Remember that substates are named according
# 	to the operator, so "state.name execute-response" is checked
# 	in those files rather than a phase
sp { propose*execute-response
	(state <s> ^name viewpoints-agent
			   ^phase choose-response-predicates-mode
			   ^is-response-executed FALSE)
	-->
	(<s> ^operator <o> +)
	(<o> ^name execute-response)
}

sp { propose*change*phase*create-pattern-lhs
	(state <s> ^name viewpoints-agent
			   ^phase choose-response-predicates-mode
			   ^is-response-executed TRUE)
	-->
	(<s> ^operator <o> +)
	(<o> ^name change-phase-create-pattern-lhs)
}

sp { apply*change*phase*create-pattern-lhs
	(state <s> ^name viewpoints-agent
			   ^operator.name change-phase-create-pattern-lhs
			   ^phase <phase>)
	-->
	(<s> ^phase <phase> -)
	(<s> ^phase create-pattern-lhs)
}

#Only variations to roughly half features per round
sp { propose*change-response-executed
	(state <s> ^name viewpoints-agent
			   ^phase choose-response-predicates-mode
			   ^is-response-executed FALSE
			   ^transform-count <count>)
	(<s> ^varied-features-count >= <count>)
	-->
	(<s> ^operator <o> +)
	(<o> ^name change-response-executed)
	(write (crlf) | REACHED PROPOSE CHANGE RESPONSE EXECUTED | )
}

sp { select*change-response-executed
	(state <s> ^name viewpoints-agent)
	(<s> ^operator <o> +)
	(<o> ^name change-response-executed)
	-->
	(<s> ^operator <o> >)
}

sp { apply*change-response-executed
	(state <s> ^name viewpoints-agent
			   ^operator.name change-response-executed
			   ^is-response-executed <is-resp-exec>
			   ^varied-features-count <vfcount>
			   ^working-copy <wc>
			   ^history-index <hindex>
			   ^history.node <node>
			   ^io.output-link <olink>)
	(<node> ^index <hindex>)
	-->
	(<node> ^output (deep-copy <wc>))
	(<olink> ^outputs.gesture <wc>)
	(<s> ^is-response-executed <is-resp-exec> -)
	(<s> ^is-response-executed TRUE)
	(<s> ^varied-features-count <vfcount> -)
	(<s> ^varied-features-count 0)
	(write | WORKING-COPY - | <wc>)
	(write (crlf) | REACHED APPLY CHANGE RESPONSE EXECUTED | (crlf))
}
