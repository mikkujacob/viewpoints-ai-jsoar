import numpy as np
from support.scorer import classifierCvOnLibrary

def fMeasureCriterion(result):
    return result['f1']

def harmonicMeanFMeasureCriterion(result):
    fmeasure0 = result[0]['f1']
    fmeasure2 = result[2]['f1']
    return 2 * fmeasure0 * fmeasure2 / (fmeasure0 + fmeasure2)

def autotuneIntOnLibrary(classifier, library, tuneRange, tuneCriterion = fMeasureCriterion, loud = True):
    if len(tuneRange) != 1:
        return NotImplementedError
    tuneParam, (tuneRangeDn, tuneRangeUp) = tuneRange[0]
    params = classifier.get_params()
    results = []
    for tuneValue in xrange(tuneRangeDn, tuneRangeUp + 1):
        if loud:
            print(tuneValue)
        params[tuneParam] = tuneValue
        newTune = classifier.__class__(**params)
        result = classifierCvOnLibrary(newTune, library, False)
        results.append((tuneValue, result))
    return max(results, key = lambda (tuneValue, result) : tuneCriterion(result))
        
def autotuneFloatOnLibrary(classifier, library, tuneRange, tolerance, tuneCriterion = fMeasureCriterion, loud = True):
    if len(tuneRange) != 1:
        return NotImplementedError
    params = classifier.get_params()
    tuneParam, (tuneRangeDn, tuneRangeUp) = tuneRange[0]
    intervalDn = tuneRangeDn
    intervalUp = tuneRangeUp
    def applyTuneValue(tuneValue):
        params[tuneParam] = tuneValue
        newTune = classifier.__class__(**params)
        return classifierCvOnLibrary(newTune, library, False)
    intervalResultUp = applyTuneValue(intervalUp)
    intervalResultDn = applyTuneValue(intervalDn)
    def tuneStep(iDn, iUp, dnResult, upResult):
        valDn, valUp = map(tuneCriterion, (dnResult, upResult))
        if abs(valDn - valUp) < tolerance:
            return (iDn, dnResult) if valDn >= valUp else (iUp, upResult)
        if loud:
            print("%.4f - %.4f" % (iDn, iUp))
        delta = iUp - iDn
        iThird = iDn + delta * (1 / 3.0)
        iTwoThirds = iDn + delta * (2 / 3.0)
        resultThird = applyTuneValue(iThird)
        resultTwoThirds = applyTuneValue(iTwoThirds)
        if tuneCriterion(resultThird) > tuneCriterion(resultTwoThirds):
            return tuneStep(iDn, iTwoThirds, dnResult, resultTwoThirds)
        elif tuneCriterion(resultThird) < tuneCriterion(resultTwoThirds):
            return tuneStep(iThird, iUp, resultThird, upResult)
        else:
            (iA, resultA) = tuneStep(iDn, iTwoThirds, dnResult, resultTwoThirds)
            (iB, resultB) = tuneStep(iThird, iUp, resultThird, upResult)
            if tuneCriterion(resultA) > tuneCriterion(resultB):
                return (iA, resultA)
            else:
                return (iB, resultB)
    return tuneStep(intervalDn, intervalUp, intervalResultUp, intervalResultDn)