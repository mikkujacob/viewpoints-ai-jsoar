from itertools import izip

def errorRateOf(classifier, on):
    errors = 0
    for parameters, label in on:
        if label != classifier.classify(parameters):
            errors += 1
    return float(errors) / len(on)

def precisionRecallFMeasureOf(classifier, on):
    result = _precisionRecallFMeasureMLabel([classifier], [on])
    if 2 == classifier.nlabels():
        return result[1]
    else:
        return result

def _precisionRecallFMeasureMLabel(classifierSerie, onSerie):
    nlabels = None
    evalrundict = None
    for classifier, on in izip(classifierSerie, onSerie):
        if None == nlabels:
            nlabels = classifier.nlabels()
            evalrundict = {label : {'tp' : 0.0, 'fp' : 0.0, 'fn' : 0.0} for label in xrange(nlabels)}
        for parameters, label in on:
            predictedLabel = classifier.predict(parameters)[0]
            if predictedLabel == label:
                evalrundict[label]['tp'] += 1 
            else:
                evalrundict[label]['fn'] += 1
                evalrundict[predictedLabel]['fp'] += 1
    evalresdict = {label : {} for label in xrange(nlabels)}
    for label in xrange(nlabels):
        theresdict = evalresdict[label]
        therundict = evalrundict[label]
        if therundict['tp'] > 0:
            theresdict['precision'] = therundict['tp'] / (therundict['tp'] + therundict['fp'])
            theresdict['recall'] = therundict['tp'] / (therundict['tp'] + therundict['fn'])
            theresdict['f1'] = 2 * theresdict['precision'] * theresdict['recall'] / (theresdict['precision'] + theresdict['recall'])
        else:
            theresdict['precision'] = 0
            theresdict['recall'] = 0
            theresdict['f1'] = 0
    return evalresdict

def classifierCvOnLibrary(classifier, library, loud = True):
    result = _mLabelClassifierCvOnLibrary(classifier, library, loud)
    if 2 == classifier.nlabels():
        return result[1]
    else:
        return result
    
def _mLabelClassifierCvOnLibrary(classifier, library, loud):
    X, y = classifier._formatLibrary(library)
    def classifierSerie():
        for i in xrange(len(X)):
            if loud:
                print(i)
            rest = [j for j in xrange(len(X)) if j != i]
            Xtrain, ytrain = (X[rest], y[rest])
            modClassifier = classifier.clone()
            modClassifier.fit(Xtrain, ytrain)
            yield modClassifier
    def onSerie():
        for i in xrange(len(X)):
            yield [(X[[i]], y[i])]
    return _precisionRecallFMeasureMLabel(classifierSerie(), onSerie())
