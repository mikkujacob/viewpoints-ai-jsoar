from scipy.stats import pearsonr
from collections import defaultdict

def selectRelevantParameters(labeledCollection):
    if not labeledCollection:
        return []
    allLabels = set(label for sample, label in labeledCollection)
    nLabels = len(allLabels)
    sample0, _ = labeledCollection[0]
    parameters = sample0.keys()
    samplesForLabel = defaultdict(list)
    for sample, label in labeledCollection:
        samplesForLabel[label].append(sample)
    relevantParameters = set()
    for labelA in xrange(nLabels):
        for labelB in xrange(labelA + 1, nLabels):
            for parameter in parameters:
                paramVals = []
                labels = []
                for label in (labelA, labelB):
                    samples = samplesForLabel[label]
                    paramVals.extend(sample[parameter] for sample in samples)
                    labels.extend([label] * len(samples))
                correlation, pvalue = pearsonr(paramVals, labels)
                if pvalue < 0.15:
                    relevantParameters.add(parameter)
    return list(relevantParameters)
