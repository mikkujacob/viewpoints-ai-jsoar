from classifiers.ScikitClassifier import ScikitClassifier
import numpy as np
from sklearn.mixture.gmm import GMM
from sklearn.cross_validation import cross_val_score, LeaveOneOut,\
    LeaveOneLabelOut
from math import log

class ScikitMixtureOfGaussiansClassifier(ScikitClassifier):
    def __init__(self, relevantParameters, ks, nclasses = 2):
        ScikitClassifier.__init__(self, nclasses)
        self._classifier = self
        self._k = ks
        self._mixtures = [GMM(n_components = ks[i], covariance_type = 'diag') for i in xrange(nclasses)]
        self._logPriors = np.zeros((nclasses, 1))        
        self._relevantParameters = relevantParameters
        
    def clone(self):
        return ScikitMixtureOfGaussiansClassifier(self._relevantParameters, self._k, self.nlabels())
        
    def fit(self, X, y):
        for label in xrange(len(self._mixtures)):
            targetMask = (y == label)
            classXs = X[targetMask]
            self._mixtures[label].fit(classXs)
            self._logPriors[label, 0] = log(float(sum(targetMask)) / len(y))
    
    def predict(self, X):
        h, _ = X.shape
        scores = np.zeros((len(self._mixtures), h))
        for label in xrange(len(self._mixtures)):
            scores[label] = self._mixtures[label].score(X)
        scores += self._logPriors
        return np.argmax(scores, axis = 0)
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "k" : self._k, "nclasses" : self.nlabels()}