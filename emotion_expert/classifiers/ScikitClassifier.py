import numpy as np

class ScikitClassifier:
    def __init__(self, nlabels = None):
        self._nlabels = nlabels
        
    def nlabels(self):
        return self._nlabels

    def _formatLibrary(self, library):
        featureTable = np.zeros((len(library), len(self._relevantParameters)), dtype = np.float64)
        target = np.zeros(len(library), dtype = np.bool if 2 == self.nlabels() else np.int)
        for i, (parameters, label) in enumerate(library):
            featureTable[i] = [parameters[relevant] for relevant in self._relevantParameters]
            target[i] = label
        return (featureTable, target)
    
    def trainOnLibrary(self, library):
        self._classifier.fit(*self._formatLibrary(library))
        
    def classify(self, sample):
        featureVector = np.array([[sample[relevant] for relevant in self._relevantParameters]])
        output = self._classifier.predict(featureVector)
        if 2 == self._nlabels:
            return "neg" if 0 == output[0] else "pos"
        else:
            return output[0]
