import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import cross_val_score, LeaveOneOut
from classifiers.ScikitClassifier import ScikitClassifier

class ScikitLogisticRegressionClassifier(ScikitClassifier): #online version of multiclass logistic regression
    def __init__(self, relevantParameters, c, nclasses = 2):
        ScikitClassifier.__init__(self, nclasses)
        self._logit = LogisticRegression(penalty = 'l1', fit_intercept = False, C = 0.5)
        self._relevantParameters = relevantParameters
        self._c = c
        
    def clone(self):
        return ScikitLogisticRegressionClassifier(self._relevantParameters, self._c, self.nlabels())
    
    def fit(self, X, y):
        self._logit.fit(X, y)
        
    def predict(self, X):
        return self._logit.predict(X)
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "c" : self._c, "nclasses" : self.nlabels()}