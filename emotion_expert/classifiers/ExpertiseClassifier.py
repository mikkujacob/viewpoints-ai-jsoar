from classifiers.ScikitClassifier import ScikitClassifier
import numpy as np

class ExpertiseClassifier(ScikitClassifier):
    def __init__(self, relevantParameters, classifiers, nclasses = 2, weights = None):
        ScikitClassifier.__init__(self, nclasses)
        self._relevantParameters = relevantParameters
        self._classifiers = classifiers
        self._weights = weights
        
    def clone(self):
        clones = [classifier.clone() for classifier in self._classifiers]
        return ExpertiseClassifier(self._relevantParameters, clones, self._weights, self.nlabels())
    
    def weightOnLibrary(self, library):
        weights = np.zeros((len(self._classifiers), 1))
        for i, classifier in enumerate(self._classifiers):
            cvresult = classifier.cvOnLibrary(library)
            weights[i, 0] = cvresult['fMeasure']
        weights /= np.sum(weights)
        self._weights = weights
    
    def fit(self, X, y):
        for classifier in self._classifiers:
            classifier.fit(X, y)
            
    def predict(self, X):
        opinionMatrix = np.zeros((len(self._classifiers), len(X)), dtype = np.bool)
        for i, classifier in enumerate(self._classifiers):
            opinionMatrix[i] = classifier.predict(X)
        votes = np.sum(opinionMatrix * self._weights, axis = 0)
        y = votes > 0.5
        return y
    