from classifiers.ScikitClassifier import ScikitClassifier
from sklearn.ensemble.forest import RandomForestClassifier

class ScikitRandomForest(ScikitClassifier):
    def __init__(self, relevantParameters, k, f = 'auto', nclasses = 2):
        ScikitClassifier.__init__(self, nclasses)
        self._relevantParameters = relevantParameters
        self._k = k
        self._f = f
        self._classifier = RandomForestClassifier(n_estimators = k, criterion = 'entropy', max_features = f)
        
    def clone(self):
        return ScikitRandomForest(self._relevantParameters, self._k, self._f, self.nlabels())
    
    def fit(self, X, y):
        self._classifier.fit(X, y)
        
    def predict(self, X):
        return self._classifier.predict(X)  
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "k" : self._k, "f" : self._f, "nclasses" : self.nlabels()}
