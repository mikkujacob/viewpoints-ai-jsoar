from dataManagement.getTrainingData import getTrainingDataMLabel
from support.relevance import selectRelevantParameters
from classifiers.ScikitRandomForest import ScikitRandomForest
from pickle import dump
from classifiers.ScikitMixtureOfGaussiansClassifier import ScikitMixtureOfGaussiansClassifier

trainingCollection = getTrainingDataMLabel(within = "data", category = "energy", nlabels = 3)
relevantParameters = selectRelevantParameters(trainingCollection)
classifier = ScikitRandomForest(relevantParameters, 100, nclasses = 3)
classifier.trainOnLibrary(trainingCollection)
with open('pickle/energy.pickle', 'w') as picklefile:
    dump(classifier, picklefile)

trainingCollection = getTrainingDataMLabel(within = "data", category = "smoothness", nlabels = 3)
relevantParameters = selectRelevantParameters(trainingCollection)
classifier = ScikitMixtureOfGaussiansClassifier(relevantParameters, [7, 5, 7], nclasses = 3)
classifier.trainOnLibrary(trainingCollection)
with open('pickle/smoothness.pickle', 'w') as picklefile:
    dump(classifier, picklefile)