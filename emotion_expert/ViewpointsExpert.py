from pickle import load
import os
import sys
from dataManagement.getTrainingData import getGestureParametersFromLines

rootdir = os.path.dirname(os.path.realpath(__file__))
with open('%s/pickle/energy.pickle' % rootdir) as energyfile:
    energyClassifier = load(energyfile)
with open('%s/pickle/smoothness.pickle' % rootdir) as smoothnessfile:
    smoothnessClassifier = load(smoothnessfile)
while True:
    parametersLines = []
    while True:
        line = raw_input("")
        if "" == line:
            break
        else:
            parametersLines.append(line)
    if not parametersLines:
        continue
    parameters = getGestureParametersFromLines(parametersLines)
    energyResponse = energyClassifier.classify(parameters)
    smoothnessResponse = smoothnessClassifier.classify(parameters)
    print("energy: %s, smoothness: %s" % (energyResponse, smoothnessResponse))
    sys.stdout.flush()