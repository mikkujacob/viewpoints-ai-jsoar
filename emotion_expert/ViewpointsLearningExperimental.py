from dataManagement.getTrainingData import getTrainingDataMLabel
from support.relevance import selectRelevantParameters
from classifiers.ScikitDecisionTree import ScikitDecisionTree
from support.scorer import classifierCvOnLibrary
from classifiers.ScikitLogisticRegression import ScikitLogisticRegressionClassifier
from classifiers.ScikitSVM import ScikitSVM
from classifiers.ScikitKNN import ScikitKNN
import numpy as np
from classifiers.ScikitRandomForest import ScikitRandomForest
from support.autotune import autotuneFloatOnLibrary,\
    harmonicMeanFMeasureCriterion
from classifiers.ScikitAdaBoost import ScikitAdaBoost
from classifiers.ScikitMixtureOfGaussiansClassifier import ScikitMixtureOfGaussiansClassifier

trainingCollection = getTrainingDataMLabel(within = "data", category = "energy", nlabels = 3)
relevantParameters = selectRelevantParameters(trainingCollection)
classifier = ScikitRandomForest(relevantParameters, 100, nclasses = 3) #ScikitMixtureOfGaussiansClassifier(relevantParameters, [7, 5, 7], nclasses = 3) #ScikitKNN(relevantParameters, 2, 5, nclasses = 3) # ScikitLogisticRegressionClassifier(relevantParameters, 1, nclasses = 3) # ScikitRandomForest(relevantParameters, 100, nclasses = 3)
tuneResult = classifierCvOnLibrary(classifier, trainingCollection, loud = True)
#tuneValue, tuneResult = autotuneFloatOnLibrary(classifier, trainingCollection, [('c', (0.1, 10.0))], 0.001, tuneCriterion = harmonicMeanFMeasureCriterion)
for label in xrange(3):
    print("%s:" % label)
    labelResult = tuneResult[label]
    for key in ['precision', 'recall', 'f1']:
        print("%s: %s" % (key, labelResult[key]))
    print("")