package ObjectiveParameters;

import java.util.HashMap;

public interface Classifier {
	public void init(HashMap<String, Object> parameters);
	public void trainOnSample(String sampleFileFullname, String label);
	public String predict(HashMap<ParameterKey, Float> parameters);
}
