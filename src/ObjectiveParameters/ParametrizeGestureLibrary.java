package ObjectiveParameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Locale;

import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Shared.JointSpaceGesture;
import Viewpoints.Synchronous.SegmentNormalization;

public class ParametrizeGestureLibrary {
	public static final String GESTURE_DIR = "training-gestures/high-smooth-golden";
	public static final String PARAMETERS_DIR = "../../ViewpointsLearning/data/smoothness/2";
	
	public static void main(String[] args) {
		try {
			File gestureDir = new File(GESTURE_DIR);
			ObjectiveParameterMonitor objectiveParameterMonitor = new ObjectiveParameterMonitor(TargetKeyset.getFullKeyset());
			for (String gestureFileName : gestureDir.list()) {
				JointSpaceGesture gesture = FileUtilities.deserializeJointsGesture(GESTURE_DIR + "/" + gestureFileName);
				gesture = SegmentNormalization.normalize(gesture);
				HashMap<ParameterKey, Float> parameters = objectiveParameterMonitor.getParameters(gesture.getGestureFramesList());
				String clearName = gestureFileName.replaceFirst("[.][^.]+$", "");
				IO.out(PARAMETERS_DIR + "/" + clearName + ".param", parameters);
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
};
