package ObjectiveParameters;

import java.util.*;

import processing.core.PVector;

import Viewpoints.Shared.Body;
import Viewpoints.Shared.BodyDerivative;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.LIDX;
import Viewpoints.Shared.LIMB;
import Viewpoints.Shared.Measure;
import Viewpoints.Shared.Pair;

public class BendingSensor implements ParameterSensor
{

	@Override
	public HashMap<ParameterKey, ArrayList<Measure>> sense(	ArrayList<Body> frames, 
															ArrayList<BodyDerivative> firstDerivative,
															ArrayList<BodyDerivative> secondDerivative,
															TargetKeyset targetTypes) {
		HashMap<ParameterKey, ArrayList<Measure>> measures = new HashMap<ParameterKey, ArrayList<Measure>>();
		
		if (!targetTypes.hasTargetType(ParameterType.BENDING) && !targetTypes.hasTargetType(ParameterType.BENDING_SPEED)) return measures;
		
		for (LIMB limb : LIMB.ALL_LIMBS) {
			Pair<LIDX, LIDX> limbParts = LIMB.PART_LIMBS().get(limb);
			Pair<JIDX, JIDX> firstPart = LIDX.LIMB_ENDS().get(limbParts.first);
			Pair<JIDX, JIDX> secondPart = LIDX.LIMB_ENDS().get(limbParts.second);
			JIDX theJoint = firstPart.second;
			ArrayList<Measure> angles = new ArrayList<Measure>();
			ArrayList<Measure> angspeeds = new ArrayList<Measure>();
			for (Body body : frames) {
				PVector firstPartVec = PVector.sub(body.get(firstPart.second), body.get(firstPart.first));
				firstPartVec.normalize();
				PVector secondPartVec = PVector.sub(body.get(secondPart.second), body.get(secondPart.first));
				secondPartVec.normalize();
				float dot = firstPartVec.dot(secondPartVec);
				if (dot > 1.0f)
					dot = 1.0f;
				else if (dot < -1.0f)
					dot = -1.0f;
				float angle = (float)Math.acos(dot);
				if (Double.isNaN(angle)) {
					System.out.println(firstPartVec.dot(secondPartVec));
				}
				angles.add(new Measure(body.getTimestamp(), angle));
			}
			for (int i = 0; i < angles.size() - 1; i++) {
				Measure amNext = angles.get(i+1);
				Measure amCurr = angles.get(i);
				float dAngle = amNext.getValue() - amCurr.getValue();
				float dT = (amNext.getTimestamp() - amCurr.getTimestamp());
				float midT = (amCurr.getTimestamp() + amNext.getTimestamp())/2;
				if (Double.isNaN(dAngle/dT)) {
					angspeeds.add(new Measure(midT, 0));
				} else {
					angspeeds.add(new Measure(midT, dAngle/dT));
				}
			}
			if (targetTypes.hasTargetType(ParameterType.BENDING)) measures.put(new ParameterKey(ParameterType.BENDING, theJoint), angles);
			if (targetTypes.hasTargetType(ParameterType.BENDING_SPEED)) measures.put(new ParameterKey(ParameterType.BENDING_SPEED, theJoint), angspeeds);
		}
		
		return measures;
	}
	
}
