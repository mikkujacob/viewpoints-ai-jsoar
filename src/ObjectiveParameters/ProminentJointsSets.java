package ObjectiveParameters;

import Viewpoints.Shared.JIDX;

public class ProminentJointsSets {
	public static JIDX[] LIMB_JIDX = { 	JIDX.HEAD, JIDX.NECK,
										JIDX.LEFT_SHOULDER, JIDX.LEFT_ELBOW, JIDX.LEFT_HAND,
										JIDX.RIGHT_SHOULDER, JIDX.RIGHT_ELBOW, JIDX.RIGHT_HAND,
										JIDX.LEFT_HIP, JIDX.LEFT_KNEE, JIDX.LEFT_FOOT,
										JIDX.RIGHT_HIP, JIDX.RIGHT_KNEE, JIDX.RIGHT_FOOT};
}
