package ObjectiveParameters;

public enum DataDerivationType {
	NONE,
	BASIC,
	ABS
}
