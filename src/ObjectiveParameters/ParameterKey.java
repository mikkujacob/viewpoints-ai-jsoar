package ObjectiveParameters;

import Viewpoints.Shared.JIDX;

public class ParameterKey implements Cloneable {
	private ParameterType paramType;
	private DataDerivationType derivationType;
	private AggregationType aggType;
	private SymmetryAggregationType symAggType;
	private JIDX jidx;
	
	public ParameterKey(ParameterType paramType, JIDX jidx) {
		super();
		this.paramType = paramType;
		this.derivationType = DataDerivationType.NONE;
		this.aggType = AggregationType.NONE;
		this.symAggType = SymmetryAggregationType.NONE;
		this.jidx = jidx;
	}
	
	public ParameterKey(ParameterType paramType, JIDX jidx, DataDerivationType derivationType, AggregationType aggType, SymmetryAggregationType symAggType) {
		super();
		this.paramType = paramType;
		this.derivationType = derivationType;
		this.aggType = aggType;
		this.symAggType = symAggType;
		this.jidx = jidx;
	}
	
	public ParameterKey(String serialization) {
		String subs[] = serialization.split("-");
		this.paramType = ParameterType.valueOf(subs[0]);
		this.jidx = JIDX.valueOf(subs[1]);
		this.derivationType = DataDerivationType.valueOf(subs[2]);
		this.aggType = AggregationType.valueOf(subs[3]);
		this.symAggType = SymmetryAggregationType.valueOf(subs[4]);
	}
	
	public ParameterKey clone() {
		return new ParameterKey(paramType, jidx, derivationType, aggType, symAggType);
	}

	public ParameterKey deriveModifiedData(DataDerivationType derivationType) {
		return new ParameterKey(paramType, jidx, derivationType, aggType, symAggType);
	}
	
	public ParameterKey deriveAggregation(AggregationType aggType) {
		return new ParameterKey(paramType, jidx, derivationType, aggType, symAggType);
	}
	
	public ParameterKey deriveSymmetryAggregation(SymmetryAggregationType symAggType) {
		return new ParameterKey(paramType, jidx, derivationType, aggType, symAggType);
	}
	
	public boolean isLeft() {
		return "LEFT".equals(jidx.toString().substring(0, 4));
	}
	
	public ParameterKey symKey() {
		String jidxStr = jidx.toString();
		String symString = null;
		if ("LEFT".equals(jidxStr.substring(0, 4))) {
			symString = "RIGHT" + jidxStr.substring(4);
		} else if ("RIGH".equals(jidxStr.substring(0, 4))) {
			symString = "LEFT" + jidxStr.substring(5);
		} else {
			return this;
		}
		return new ParameterKey(paramType, JIDX.valueOf(symString), derivationType, aggType, symAggType);
	}
	
	public ParameterType getParamType() {
		return paramType;
	}
	
	public void setParamType(ParameterType paramType) {
		this.paramType = paramType;
	}
	
	public DataDerivationType getDerivationType() {
		return derivationType;
	}
	
	public AggregationType getAggType() {
		return aggType;
	}
	
	public SymmetryAggregationType getSymAggType() {
		return symAggType;
	}

	public JIDX getJidx() {
		return jidx;
	}
	
	public void setJidx(JIDX jidx) {
		this.jidx = jidx;
	}
	
	public String toString() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(paramType.toString());
		sbuf.append("-");
		sbuf.append(jidx.toString());
		sbuf.append("-");
		sbuf.append(derivationType.toString());
		sbuf.append("-");
		sbuf.append(aggType.toString());
		sbuf.append("-");
		sbuf.append(symAggType.toString());
		return sbuf.toString();
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj)
			return false;
		if (!(obj instanceof ParameterKey))
			return false;
		return toString().equals(obj.toString());
	}
	
	public boolean looselyFit(ParameterKey other) {
		return (ParameterType.NONE == this.paramType || ParameterType.NONE == other.paramType || this.paramType == other.paramType) &&
			   (AggregationType.NONE == this.aggType || AggregationType.NONE == other.aggType || this.aggType == other.aggType) &&
			   (SymmetryAggregationType.NONE == this.symAggType || SymmetryAggregationType.NONE == other.symAggType || this.symAggType == other.symAggType) &&
			   (DataDerivationType.NONE == this.derivationType || DataDerivationType.NONE == other.derivationType || this.derivationType == other.derivationType);
	}
}
