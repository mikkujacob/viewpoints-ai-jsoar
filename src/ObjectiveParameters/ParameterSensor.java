package ObjectiveParameters;

import java.util.*;

import Viewpoints.Shared.Body;
import Viewpoints.Shared.BodyDerivative;
import Viewpoints.Shared.Measure;

public interface ParameterSensor {
	public HashMap<ParameterKey, ArrayList<Measure>> sense(	ArrayList<Body> frames,
															ArrayList<BodyDerivative> firstDerivative,
															ArrayList<BodyDerivative> secondDerivative,
															TargetKeyset targetTypes);
}
