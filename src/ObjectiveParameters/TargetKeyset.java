package ObjectiveParameters;

import java.util.*;

public class TargetKeyset {
	private HashMap<ParameterType, ArrayList<ParameterKey>> targets = null;
	
	public void addTarget(ParameterKey target) {
		if (null == targets) {
			targets = new HashMap<ParameterType, ArrayList<ParameterKey>>();
		}
			
		if (!targets.containsKey(target.getParamType())) {
			targets.put(target.getParamType(), new ArrayList<ParameterKey>());
		}
		targets.get(target.getParamType()).add(target);
	}
	
	public boolean hasTargetType(ParameterType type) 
	{
		return null == targets || targets.containsKey(type);
	}
	
	public boolean hasTargetFor(ParameterKey key) {
		return checkTargets(key);
	}
	
	private boolean checkTargets(ParameterKey key) 
	{
		if (null == targets) return true;
		if (!targets.containsKey(key.getParamType())) return false;
		ArrayList<ParameterKey> targetsForType = targets.get(key.getParamType());
		for (ParameterKey targetKey : targetsForType) {
			if (targetKey.looselyFit(key)) return true;
		}
		return false;
	}
	
	public static TargetKeyset getFullKeyset() {
		return new TargetKeyset();
	}
	
	public static TargetKeyset getBasicSimilarityKeyset() {
		TargetKeyset targetKeyset = new TargetKeyset();
		targetKeyset.addTarget(new ParameterKey("SPEED-TORSO-NONE-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("SPEED-TORSO-NONE-MAX-LEAD"));
		targetKeyset.addTarget(new ParameterKey("TANGENT_ACCELERATION-TORSO-NONE-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("TANGENT_ACCELERATION-TORSO-NONE-MAX-LEAD"));
		targetKeyset.addTarget(new ParameterKey("NORMAL_ACCELERATION-TORSO-NONE-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("NORMAL_ACCELERATION-TORSO-NONE-MAX-LEAD"));
		targetKeyset.addTarget(new ParameterKey("FORWARD_SPEED-TORSO-ABS-MAX-LEAD"));
		targetKeyset.addTarget(new ParameterKey("FORWARD_SPEED-TORSO-ABS-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("UPWARD_SPEED-TORSO-ABS-MAX-LEAD"));
		targetKeyset.addTarget(new ParameterKey("UPWARD_SPEED-TORSO-ABS-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("SIDEWARD_SPEED-TORSO-ABS-MAX-LEAD"));
		targetKeyset.addTarget(new ParameterKey("SIDEWARD_SPEED-TORSO-ABS-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("FORWARDNESS-TORSO-ABS-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("FORWARDNESS-TORSO-BASIC-RANGE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("UPWARDNESS-TORSO-ABS-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("UPWARDNESS-TORSO-BASIC-RANGE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("SIDEWARDNESS-TORSO-ABS-AVE-LEAD"));
		targetKeyset.addTarget(new ParameterKey("SIDEWARDNESS-TORSO-BASIC-RANGE-LEAD"));
		return targetKeyset;
	}
}
