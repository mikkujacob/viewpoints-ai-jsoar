package Viewpoints.KinectParsing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import ObjectiveParameters.ObjectiveParameterMonitor;
import ObjectiveParameters.IO;
import ObjectiveParameters.ParameterKey;
import ObjectiveParameters.TargetKeyset;
import Viewpoints.Gesture.FramePredicates;
import Viewpoints.Gesture.Gesture;
import Viewpoints.Gesture.TempoPredicate;
import Viewpoints.Gesture.LeftLegStillnessPredicate;
import Viewpoints.Gesture.RightLegStillnessPredicate;
import Viewpoints.Gesture.SmoothnessPredicate;
import Viewpoints.Gesture.EnergyPredicate;
import Viewpoints.Gesture.TransformPredicates.TransformPredicateInterface;
import Viewpoints.Gesture.ViewpointPredicate;
import Viewpoints.Gesture.Predicates.*;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.JointSpaceGesture;
import Viewpoints.Gesture.*;
import processing.core.PVector;

public class GestureController {
	final static int FRAME_SAMPLING_RATE = 10;	// use to only analyze data from every N frames
	final static int TEMPO_AVERAGING_WINDOW = 3;
	final static int SMOOTHNESS_AVERAGING_WINDOW = 3;
	final static int ENERGY_AVERAGING_WINDOW = 6;

	TempoPredicate tempoPred = null;
	/*LeftLegStillnessPredicate leftLegStillnessPred;
	RightLegStillnessPredicate rightLegStillnessPred;
	LeftHandStillnessPredicate leftHandStillnessPred;
	RightHandStillnessPredicate rightHandStillnessPred;
	LeftLegTransversePredicate leftLegTransversePred;
	RightLegTransversePredicate rightLegTransversePred;
	LeftHandTransversePredicate leftHandTransversePred;
	RightHandTransversePredicate rightHandTransversePred;
	LeftLegLongitudinalPredicate leftLegLongitudinalPred;
	RightLegLongitudinalPredicate rightLegLongitudinalPred;
	LeftHandLongitudinalPredicate leftHandLongitudinalPred;
	RightHandLongitudinalPredicate rightHandLongitudinalPred;
	LeftLegVerticalPredicate leftLegVerticalPred;
	RightLegVerticalPredicate rightLegVerticalPred;
	LeftHandVerticalPredicate leftHandVerticalPred;
	RightHandVerticalPredicate rightHandVerticalPred;
	SmoothnessPredicate smoothPred;
	EnergyPredicate energyPred;*/
	
	FramePredicates framePreds;

	//float runningTempoVal = 0f; // used to track a running value of tempo across frames
	//TEMPO runningTempo = TEMPO.NONE;
	//float runningLeftLegStillnessVal = 0f; // used to track a running value of LeftLegStillness across frames
	//LEFT_LEG_STILL runningLeftLegStillness = LEFT_LEG_STILL.FALSE;
	//float runningRightLegStillnessVal = 0f; // used to track a running value of R. LegStillness across frames
	//RIGHT_LEG_STILL runningRightLegStillness = RIGHT_LEG_STILL.FALSE;
	//float runningLeftHandStillnessVal = 0f; // used to track a running value of LeftHandStillness across frames
	//LEFT_HAND_STILL runningLeftHandStillness = LEFT_HAND_STILL.FALSE;
	//float runningRightHandStillnessVal = 0f; // used to track a running value of Right Hand Stillness across frames
	//RIGHT_HAND_STILL runningRightHandStillness = RIGHT_HAND_STILL.FALSE;
	//float runningEnergyVal = 0f; // used to track a running value of tempo across frames
	//ENERGY runningEnergy = ENERGY.NONE;
	//float runningSmoothVal = 0f; // used to track a running value of tempo across frames
	//SMOOTHNESS runningSmooth = SMOOTHNESS.NONE;

	/*float runningLeftHandTransverseVal = 0f; // used to track a running value of LeftHandTransverse across frames
	LEFT_HAND_TRANSVERSE runningLeftHandTransverse = LEFT_HAND_TRANSVERSE.FALSE;
	float runningRightHandTransverseVal = 0f; // used to track a running value of Right Hand Transverse across frames
	RIGHT_HAND_TRANSVERSE runningRightHandTransverse = RIGHT_HAND_TRANSVERSE.FALSE;
	float runningLeftLegLongitudinalVal = 0f; // used to track a running value of LeftLegLongitudinal across frames
	LEFT_LEG_LONGITUDINAL runningLeftLegLongitudinal = LEFT_LEG_LONGITUDINAL.FALSE;
	float runningRightLegLongitudinalVal = 0f; // used to track a running value of R. LegLongitudinal across frames
	RIGHT_LEG_LONGITUDINAL runningRightLegLongitudinal = RIGHT_LEG_LONGITUDINAL.FALSE;
	float runningLeftHandLongitudinalVal = 0f; // used to track a running value of LeftHandLongitudinal across frames
	LEFT_HAND_LONGITUDINAL runningLeftHandLongitudinal = LEFT_HAND_LONGITUDINAL.FALSE;
	float runningRightHandLongitudinalVal = 0f; // used to track a running value of Right Hand Longitudinal across frames
	RIGHT_HAND_LONGITUDINAL runningRightHandLongitudinal = RIGHT_HAND_LONGITUDINAL.FALSE;
	float runningLeftLegVerticalVal = 0f; // used to track a running value of LeftLegVertical across frames
	LEFT_LEG_VERTICAL runningLeftLegVertical = LEFT_LEG_VERTICAL.FALSE;
	float runningRightLegVerticalVal = 0f; // used to track a running value of R. LegVertical across frames
	RIGHT_LEG_VERTICAL runningRightLegVertical = RIGHT_LEG_VERTICAL.FALSE;
	float runningLeftHandVerticalVal = 0f; // used to track a running value of LeftHandVertical across frames
	LEFT_HAND_VERTICAL runningLeftHandVertical = LEFT_HAND_VERTICAL.FALSE;
	float runningRightHandVerticalVal = 0f; // used to track a running value of Right Hand Vertical across frames
	RIGHT_HAND_VERTICAL runningRightHandVertical = RIGHT_HAND_VERTICAL.FALSE;*/


	// track history of framepredicates, gestures, and each viewpoint predicate type
	ArrayList<FramePredicates> predHist = new ArrayList<FramePredicates>();
	//ArrayList<Gesture> gestureHist = new ArrayList<Gesture>();
	ArrayList<TempoPredicate> tempoPredHist = new ArrayList<TempoPredicate>();
	ArrayList<LeftLegStillnessPredicate> leftLegStillnessPredHist = new ArrayList<LeftLegStillnessPredicate>();
	ArrayList<RightLegStillnessPredicate> rightLegStillnessPredHist = new ArrayList<RightLegStillnessPredicate>();
	ArrayList<LeftHandStillnessPredicate> leftHandStillnessPredHist = new ArrayList<LeftHandStillnessPredicate>();
	ArrayList<RightHandStillnessPredicate> rightHandStillnessPredHist = new ArrayList<RightHandStillnessPredicate>();
	ArrayList<LeftLegTransversePredicate> leftLegTransversePredHist = new ArrayList<LeftLegTransversePredicate>();
	ArrayList<RightLegTransversePredicate> rightLegTransversePredHist = new ArrayList<RightLegTransversePredicate>();
	ArrayList<LeftHandTransversePredicate> leftHandTransversePredHist = new ArrayList<LeftHandTransversePredicate>();
	ArrayList<RightHandTransversePredicate> rightHandTransversePredHist = new ArrayList<RightHandTransversePredicate>();
	ArrayList<LeftLegLongitudinalPredicate> leftLegLongitudinalPredHist = new ArrayList<LeftLegLongitudinalPredicate>();
	ArrayList<RightLegLongitudinalPredicate> rightLegLongitudinalPredHist = new ArrayList<RightLegLongitudinalPredicate>();
	ArrayList<LeftHandLongitudinalPredicate> leftHandLongitudinalPredHist = new ArrayList<LeftHandLongitudinalPredicate>();
	ArrayList<RightHandLongitudinalPredicate> rightHandLongitudinalPredHist = new ArrayList<RightHandLongitudinalPredicate>();
	ArrayList<LeftLegVerticalPredicate> leftLegVerticalPredHist = new ArrayList<LeftLegVerticalPredicate>();
	ArrayList<RightLegVerticalPredicate> rightLegVerticalPredHist = new ArrayList<RightLegVerticalPredicate>();
	ArrayList<LeftHandVerticalPredicate> leftHandVerticalPredHist = new ArrayList<LeftHandVerticalPredicate>();
	ArrayList<RightHandVerticalPredicate> rightHandVerticalPredHist = new ArrayList<RightHandVerticalPredicate>();
	
	ArrayList<SmoothnessPredicate> momentarySmoothPredHist = new ArrayList<SmoothnessPredicate>();
	ArrayList<SmoothnessPredicate> smoothPredHist = new ArrayList<SmoothnessPredicate>();
	
	ArrayList<EnergyPredicate> energyPredHist = new ArrayList<EnergyPredicate>();
	
	ArrayList<Body> jointHist = new ArrayList<Body>();
	final static int MAX_JOINT_HIST_SIZE = 3; // number of frames to retain of skeleton history
	ArrayList<Body> gestureRecodring = new ArrayList<Body>();

	Gesture currentGesture = null;
	JointSpaceGesture jsGesture = null;
	Boolean hasGesture = false;

	int stillDuration = 0;
	int gestureDuration = 0;
	int runningFrames = 0;
	
	Body curJoints = new Body();
	Body prevJoints = new Body();
	Body prev2Joints = new Body();

	HashMap<JIDX, PVector> curRelJoints = new HashMap<JIDX, PVector>();
	HashMap<JIDX, PVector> prevRelJoints = new HashMap<JIDX, PVector>();
	HashMap<JIDX, PVector> prev2RelJoints = new HashMap<JIDX, PVector>();

	HashMap<JIDX, Float> jointMovements;

	float baseHeight = 0.0f;  // height of the skeleton in "neutral" position

	private boolean leftXMvmt, leftYMvmt, leftZMvmt, rightXMvmt, rightYMvmt, rightZMvmt;  

	/*
    skeleton information
	 */
	final int SKEL_SIZE = 15;

	final int HEAD_IDX = 0;
	final int RHAND_IDX = 1;
	final int RWRIST_IDX = 2;
	final int RELBOW_IDX = 3;
	final int RSHOULDER_IDX = 4;
	final int RHIP_IDX = 5;
	final int RKNEE_IDX = 12;
	final int RFOOT_IDX = 14;

	final int LHAND_IDX = 6;
	final int LWRIST_IDX = 7;
	final int LELBOW_IDX = 8;
	final int LSHOULDER_IDX = 9;
	final int LHIP_IDX = 10;
	final int LKNEE_IDX = 11;
	final int LFOOT_IDX = 13;

	public final String MAC_ANACONDA_PATH = System.getProperty("user.home") + "//anaconda" + File.separator + "bin" + File.separator;
	public final String EMOTION_EXPERT_PATH = "/emotion_expert/EmotionExpert.py";
	public String emotionExpertCommand;
	private Process emotionExpert = null;
	private PrintStream emotionExpertInput = null;
	private Scanner emotionExpertOutput = null;
	private ObjectiveParameterMonitor objectiveParametersMonitor = new ObjectiveParameterMonitor(TargetKeyset.getFullKeyset());

	/**
	 * Initialize GestureController using set of joint information
	 * @param joints
	 */
	@Deprecated
	public GestureController(Body joints){
		curJoints = (Body)joints.clone();
	}
	
	/**
	 * Default constructor for GestureController
	 */
	public GestureController() {
		try {
			String PROJECT_HOME = System.getProperty("user.dir");
			
			if(PROJECT_HOME.indexOf(File.separator + "bin") != -1)
			{
				PROJECT_HOME += File.separator + "..";
			}
			
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") == -1)  
			{
				emotionExpertCommand = MAC_ANACONDA_PATH + "python " + PROJECT_HOME + EMOTION_EXPERT_PATH;
			}
			else
			{
				emotionExpertCommand = "python " + PROJECT_HOME + EMOTION_EXPERT_PATH;
			}
			
			System.out.println(emotionExpertCommand);
			
			emotionExpert = Runtime.getRuntime().exec(emotionExpertCommand);
			
			emotionExpertInput = new PrintStream(emotionExpert.getOutputStream());
			emotionExpertOutput = new Scanner(emotionExpert.getInputStream());
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	/***
	 * Writes the data from the InputStream to the OutputStream
	 * @param inputStream - InputStream to redirect
	 * @param out - OutputStream to redirect to
	 */
	void inputStreamToOutputStream(final InputStream inputStream, final OutputStream out, final String streamName)
	{
	    Thread t = new Thread(new Runnable()
	    {
	        public void run()
	        {
	        	Boolean isFirstLine = true;
	            try
	            {
	                int d;    
	                while ((d = inputStream.read()) != -1)
	                {
	                	if(isFirstLine)
	                	{
	                		System.out.print(streamName + " > ");
	                		isFirstLine = !isFirstLine;
	                	}
	                    out.write(d);
	                	if(d == '\n')
	                	{
	                		System.out.print(streamName + " > ");
	                	}
	                }
	            }
	            catch (IOException ex)
	            {
	                //TODO make a callback on exception.
	            }
	        }
	    });
	    t.setDaemon(true);
	    t.start();
	}

	public void update(Body joints) {
		gestureRecodring.add(joints);
		++runningFrames;
		
		if (0 != runningFrames % FRAME_SAMPLING_RATE)
			return;
		
		prev2Joints = prevJoints;
		prevJoints = curJoints;
		setJoints(joints);
		curJoints.setTimestamp(runningFrames/30.0f);
		jointHist.add(curJoints);
		
		if (jointHist.size() > MAX_JOINT_HIST_SIZE) {
			jointHist.remove(0);
		}
		
		resetGestures();
		
		framePreds = new FramePredicates();
		
		if (jointHist.size() > 2) {
			simpleGestureChecker();
			viewpointPredicateChecker();
			predHist.add(framePreds);
			detectGesture(1, 3);
		}
	}


	//Sets all to false
	void resetGestures(){
		leftXMvmt=false;
		leftYMvmt=false;
		leftZMvmt=false;
		rightXMvmt=false;
		rightYMvmt=false;
		rightZMvmt=false;
	}
	
	/**
	 * Based on recent joint history:
	 * 	(1) compute the various Viewpoints Predicates,
	 * 	(2) indicate their detection
	 * 	(3) compute their running values
	 */
	void viewpointPredicateChecker() {
		tempoPred = new TempoPredicate(jointHist);
		tempoPredHist.add(tempoPred);
		framePreds.setFrameTempo(tempoRunningAverage(tempoPredHist));

		LeftLegStillnessPredicate leftLegStillnessPred = new LeftLegStillnessPredicate(jointHist);
		leftLegStillnessPredHist.add(leftLegStillnessPred);
		RightLegStillnessPredicate rightLegStillnessPred = new RightLegStillnessPredicate(jointHist);
		rightLegStillnessPredHist.add(rightLegStillnessPred);
		LeftHandStillnessPredicate leftHandStillnessPred = new LeftHandStillnessPredicate(jointHist);
		leftHandStillnessPredHist.add(leftHandStillnessPred);
		RightHandStillnessPredicate rightHandStillnessPred = new RightHandStillnessPredicate(jointHist);
		rightHandStillnessPredHist.add(rightHandStillnessPred);
		LeftLegTransversePredicate leftLegTransversePred = new LeftLegTransversePredicate(jointHist);
		leftLegTransversePredHist.add(leftLegTransversePred);
		RightLegTransversePredicate rightLegTransversePred = new RightLegTransversePredicate(jointHist);
		rightLegTransversePredHist.add(rightLegTransversePred);
		LeftHandTransversePredicate leftHandTransversePred = new LeftHandTransversePredicate(jointHist);
		leftHandTransversePredHist.add(leftHandTransversePred);
		RightHandTransversePredicate rightHandTransversePred = new RightHandTransversePredicate(jointHist);
		rightHandTransversePredHist.add(rightHandTransversePred);
		LeftLegLongitudinalPredicate leftLegLongitudinalPred = new LeftLegLongitudinalPredicate(jointHist);
		leftLegLongitudinalPredHist.add(leftLegLongitudinalPred);
		RightLegLongitudinalPredicate rightLegLongitudinalPred = new RightLegLongitudinalPredicate(jointHist);
		rightLegLongitudinalPredHist.add(rightLegLongitudinalPred);
		LeftHandLongitudinalPredicate leftHandLongitudinalPred = new LeftHandLongitudinalPredicate(jointHist);
		leftHandLongitudinalPredHist.add(leftHandLongitudinalPred);
		RightHandLongitudinalPredicate rightHandLongitudinalPred = new RightHandLongitudinalPredicate(jointHist);
		rightHandLongitudinalPredHist.add(rightHandLongitudinalPred);
		LeftLegVerticalPredicate leftLegVerticalPred = new LeftLegVerticalPredicate(jointHist);
		leftLegVerticalPredHist.add(leftLegVerticalPred);
		RightLegVerticalPredicate rightLegVerticalPred = new RightLegVerticalPredicate(jointHist);
		rightLegVerticalPredHist.add(rightLegVerticalPred);
		LeftHandVerticalPredicate leftHandVerticalPred = new LeftHandVerticalPredicate(jointHist);
		leftHandVerticalPredHist.add(leftHandVerticalPred);
		RightHandVerticalPredicate rightHandVerticalPred = new RightHandVerticalPredicate(jointHist);
		rightHandVerticalPredHist.add(rightHandVerticalPred);
		
		EnergyPredicate energyPred = new EnergyPredicate(jointHist);
		energyPredHist.add(energyPred);
		EnergyPredicate runningEnergy = new EnergyPredicate(
				ViewpointPredicate.predicateRunningAvg(energyPredHist, ENERGY_AVERAGING_WINDOW));
		framePreds.setFrameEnergy((ENERGY)runningEnergy.getPredValue());

		SmoothnessPredicate momentarySmoothPred = new SmoothnessPredicate(jointHist);
		momentarySmoothPredHist.add(momentarySmoothPred);
		SmoothnessPredicate runningSmoothness = new SmoothnessPredicate(
				ViewpointPredicate.predicateRunningMax(momentarySmoothPredHist, SMOOTHNESS_AVERAGING_WINDOW));
		smoothPredHist.add(runningSmoothness);
		framePreds.setFrameSmoothness((SMOOTHNESS)runningSmoothness.getPredValue());
	}
	 
	TEMPO tempoRunningAverage(ArrayList<TempoPredicate> tempos) {
		TempoPredicate runningTempo = new TempoPredicate(
				ViewpointPredicate.predicateRunningAvg(tempos, TEMPO_AVERAGING_WINDOW));
		return (TEMPO)runningTempo.getPredValue();
	}

	/**
	 * Detects whether recent set of frames indicates a gesture by checking for sustained movement followed by sustained stillness.
	 * Updates predicate and gesture histories accordingly and sets currentGesture for output
	 * 
	 * @param minStillDuration - minimum number of recorded frames to not move to be the end of a gesture
	 * @param minMovingDuraiton - minimum number of recorded frames to move to be a gesture
	 */
	void detectGesture(int minStillDuration, int minMovingDuraiton) {
		if (TEMPO.ALMOST_STILL != (TEMPO)tempoPred.getPredValue()) {
			++gestureDuration;
		} else {
			++stillDuration;
			
			if (stillDuration >= minStillDuration) {
				if (gestureDuration >= minMovingDuraiton) {
					detectedGestureCleanup();
				} else {
					gcGestureDetectionReset();
				}
			} else if (0 == gestureDuration) {
				gcGestureDetectionReset();
			}
		}
	}
	
	/*void gcUpdatePredicateHistories() {
		predHist.add(framePreds);
		tempoPredHist.add(tempoPred);
		leftLegStillnessPredHist.add(leftLegStillnessPred);
		rightLegStillnessPredHist.add(rightLegStillnessPred);
		leftHandStillnessPredHist.add(leftHandStillnessPred);
		rightHandStillnessPredHist.add(rightHandStillnessPred);
		energyPredHist.add(energyPred);
		smoothPredHist.add(smoothPred);
	}*/
	
	void detectedGestureCleanup()
	{
		System.out.println("Gesture controller got gesture!");
		trimGestureTail();
		
		currentGesture = new Gesture(new ArrayList<TransformPredicateInterface>(), 
									predHist, 
									tempoPredHist, 
									leftLegStillnessPredHist, 
									rightLegStillnessPredHist, 
									leftHandStillnessPredHist, 
									rightHandStillnessPredHist, 
									energyPredHist,
									smoothPredHist, 
									leftLegTransversePredHist,
									rightLegTransversePredHist,
									leftHandTransversePredHist,
									rightHandTransversePredHist,
									leftLegLongitudinalPredHist,
									rightLegLongitudinalPredHist,
									leftHandLongitudinalPredHist,
									rightHandLongitudinalPredHist,
									leftLegVerticalPredHist,
									rightLegVerticalPredHist,
									leftHandVerticalPredHist,
									rightHandVerticalPredHist);
		
		jsGesture = new JointSpaceGesture(gestureRecodring);
		///gestureHist.add(currentGesture);
		hasGesture = true;
		
		/*HashMap<ParameterKey, Float> parameters = objectiveParametersMonitor.getParameters(jsGesture.getGestureFramesList());
		if (null != parameters) {
			IO.outToStream(emotionExpertInput, parameters);
			try {
				emotionExpert.getOutputStream().flush();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			String response = emotionExpertOutput.nextLine();
			System.out.println(response);
			currentGesture.setEmotionFromString(response);
		} */
		//int exval = emotionExpert.exitValue();
		
		gcGestureDetectionReset();
	}
	
	void trimGestureTail() {
		for (int i = predHist.size() - 1; i >= 0; i--) {
			TempoPredicate tempo = tempoPredHist.get(i);
			
			if (TEMPO.ALMOST_STILL == (TEMPO)tempo.getPredValue()) {
				popPredicateHistories();
				
				int beginningOfSample = FRAME_SAMPLING_RATE * i;
				
				for (int frameI = gestureRecodring.size() - 1; frameI >= beginningOfSample; frameI--) {
					gestureRecodring.remove(i);
				}
			} else {
				return;
			}
		}
	}
	
	void popPredicateHistories() {
		int i = predHist.size() - 1;
		predHist.remove(i);
		tempoPredHist.remove(i);
		leftLegStillnessPredHist.remove(i);
		rightLegStillnessPredHist.remove(i);
		leftHandStillnessPredHist.remove(i);
		rightHandStillnessPredHist.remove(i);
		leftLegTransversePredHist.remove(i);
		rightLegTransversePredHist.remove(i);
		leftHandTransversePredHist.remove(i);
		rightHandTransversePredHist.remove(i);
		leftLegLongitudinalPredHist.remove(i);
		rightLegLongitudinalPredHist.remove(i);
		leftHandLongitudinalPredHist.remove(i);
		rightHandLongitudinalPredHist.remove(i);
		leftLegVerticalPredHist.remove(i);
		rightLegVerticalPredHist.remove(i);
		leftHandVerticalPredHist.remove(i);
		rightHandVerticalPredHist.remove(i);
		energyPredHist.remove(i);
		momentarySmoothPredHist.remove(i);
		smoothPredHist.remove(i);
	}
	
	//Cleanup Without Signalling a gesture has been detected
	void gcGestureDetectionReset()
	{
		predHist.clear();
		tempoPredHist.clear();
		leftLegStillnessPredHist.clear();
		rightLegStillnessPredHist.clear();
		leftHandStillnessPredHist.clear();
		rightHandStillnessPredHist.clear();
		leftLegTransversePredHist.clear();
		rightLegTransversePredHist.clear();
		leftHandTransversePredHist.clear();
		rightHandTransversePredHist.clear();
		leftLegLongitudinalPredHist.clear();
		rightLegLongitudinalPredHist.clear();
		leftHandLongitudinalPredHist.clear();
		rightHandLongitudinalPredHist.clear();
		leftLegVerticalPredHist.clear();
		rightLegVerticalPredHist.clear();
		leftHandVerticalPredHist.clear();
		rightHandVerticalPredHist.clear();
		energyPredHist.clear();
		momentarySmoothPredHist.clear();
		smoothPredHist.clear();
		
		stillDuration = 0;
		gestureDuration = 0;
		gestureRecodring.clear();
	}

	/**
    Updates framePreds to account for current gestures.
	 */
	void simpleGestureChecker(){
		String forPrinting = "DEBUG saw:\n";

		PVector head = curJoints.get(JIDX.HEAD);

		PVector rShoulder = curJoints.get(JIDX.RIGHT_SHOULDER);
		PVector rElbow = curJoints.get(JIDX.RIGHT_ELBOW);
		PVector rHand = curJoints.get(JIDX.RIGHT_HAND);

		PVector lShoulder = curJoints.get(JIDX.LEFT_SHOULDER);
		PVector lElbow = curJoints.get(JIDX.LEFT_ELBOW);
		PVector lHand = curJoints.get(JIDX.LEFT_HAND);

		PVector rHip = curJoints.get(JIDX.RIGHT_HIP);
		PVector rKnee = curJoints.get(JIDX.RIGHT_KNEE);
		PVector rFoot = curJoints.get(JIDX.RIGHT_FOOT);

		PVector lHip = curJoints.get(JIDX.LEFT_HIP);
		PVector lKnee = curJoints.get(JIDX.LEFT_KNEE);
		PVector lFoot = curJoints.get(JIDX.LEFT_FOOT);



		PVector rUpperArm = PVector.sub(rShoulder, rElbow);
		PVector rLowerArm = PVector.sub(rHand, rElbow);

		PVector lUpperArm = PVector.sub(lShoulder, lElbow);
		PVector lLowerArm = PVector.sub(lHand, lElbow);

		float rAngle = PVector.angleBetween(rUpperArm, rLowerArm);
		float lAngle = PVector.angleBetween(lUpperArm, lLowerArm);

		rAngle = (float) Math.toDegrees(rAngle);
		lAngle = (float) Math.toDegrees(lAngle);

		if(lAngle>150){
			framePreds.setFrameLeftArmCurve(LEFTARM_CURVE.STRAIGHT);
		}
		else if(lAngle<115){
			framePreds.setFrameLeftArmCurve(LEFTARM_CURVE.BENT);
		}

		if(rAngle>150){
			framePreds.setFrameRightArmCurve(RIGHTARM_CURVE.STRAIGHT);
		}
		else if(rAngle<115){
			framePreds.setFrameRightArmCurve(RIGHTARM_CURVE.BENT);
		}

		//If right hand is more left than right shoulder
		if(rShoulder.x>rHand.x && rHand.x>lShoulder.x){
			framePreds.setFrameRightHandPos(RIGHTHAND_POS.IN);
		} else if(rShoulder.x<rElbow.x && rElbow.x<rHand.x){
			framePreds.setFrameRightHandPos(RIGHTHAND_POS.OUT);
		} else {
			framePreds.setFrameRightHandPos(RIGHTHAND_POS.NONE);
		}

		//If left hand is more right than left shoulder
		if(lShoulder.x<lHand.x && lHand.x<rShoulder.x){
			framePreds.setFrameLeftHandPos(LEFTHAND_POS.IN);
		} else if(lShoulder.x>lElbow.x && lElbow.x>lHand.x){
			framePreds.setFrameLeftHandPos(LEFTHAND_POS.OUT);
		} else {
			framePreds.setFrameLeftHandPos(LEFTHAND_POS.NONE);
		}

		float xRDist = Math.abs(head.x-rHand.x);
		float yRDist = Math.abs(head.y-rHand.y);

		float xLDist = Math.abs(head.x-lHand.x);
		float yLDist = Math.abs(head.y-lHand.y);
		
		if(lShoulder.y<lHand.y){
			framePreds.setFrameLeftHandHeight(LEFTHAND_HEIGHT.UP);
		} else if(lHand.y<lElbow.y && lElbow.y<lShoulder.y){
			framePreds.setFrameLeftHandHeight(LEFTHAND_HEIGHT.DOWN);
		} else if(xLDist<100 && yLDist<200){
			framePreds.setFrameLeftHandHeight(LEFTHAND_HEIGHT.AT_MOUTH);
		} else {
			framePreds.setFrameLeftHandHeight(LEFTHAND_HEIGHT.NONE);
		}

		if(rShoulder.y<rHand.y){
			framePreds.setFrameRightHandHeight(RIGHTHAND_HEIGHT.UP);
		} else if(rHand.y<rElbow.y && rElbow.y<rShoulder.y){
			framePreds.setFrameRightHandHeight(RIGHTHAND_HEIGHT.DOWN);
		} else if(xRDist<100 && yRDist<200){
			framePreds.setFrameRightHandHeight(RIGHTHAND_HEIGHT.AT_MOUTH);
		} else {
			framePreds.setFrameRightHandHeight(RIGHTHAND_HEIGHT.NONE);
		}
		
		float xHandDist = Math.abs(rHand.x-lHand.x);
		float yHandDist = Math.abs(rHand.y-lHand.y);

		if(xHandDist< 140 && yHandDist <140){
			framePreds.setFrameHandsTogether(HANDS_TOGETHER.TOGETHER);
		}

		PVector rHandDiff = PVector.sub(rHand, prevJoints.get(JIDX.RIGHT_HAND));
		PVector lHandDiff = PVector.sub(lHand, prevJoints.get(JIDX.LEFT_HAND));


		PVector rDiffNorm = new PVector (rHandDiff.x,rHandDiff.y,rHandDiff.z);
		PVector lDiffNorm = new PVector (lHandDiff.x,lHandDiff.y,lHandDiff.z);

		rDiffNorm.normalize();
		lDiffNorm.normalize();

		//Mostly in x direction
		if(Math.abs(lDiffNorm.x)>0.8){
			leftXMvmt=true;
		}
		//Mostly in y direction
		else if(Math.abs(lDiffNorm.y)>0.8){
			leftYMvmt=true;
		}
		//Mostly in z direction
		else if(Math.abs(lDiffNorm.z)>0.8){
			leftZMvmt=true;
		}

		//Mostly in x direction
		if(Math.abs(rDiffNorm.x)>0.8){
			rightXMvmt=true;
		}
		else if(Math.abs(rDiffNorm.y)>0.8){
			rightYMvmt=true;
		}
		else if(Math.abs(rDiffNorm.z)>0.8){
			rightZMvmt=true;
		}

		// predicates to check orientation
		// orientation is from audience perspective
		float shoulderAngle = (float) Math.atan2(rShoulder.z-lShoulder.z,rShoulder.x-lShoulder.x);

		if (shoulderAngle < -0.50f) {
			framePreds.setFrameFacing(FACING.FAR_LEFT);
		} else if (shoulderAngle < -0.20f) {
			framePreds.setFrameFacing(FACING.SLIGHT_LEFT);
		} else if (shoulderAngle > 0.50f) {
			framePreds.setFrameFacing(FACING.FAR_RIGHT);
		} else if (shoulderAngle > 0.20f) {
			framePreds.setFrameFacing(FACING.SLIGHT_RIGHT);
		} else {
			framePreds.setFrameFacing(FACING.CENTER);
		}

		/*
     predicates for detecting height
		 */
		// neck
		PVector lNeck = PVector.sub(head, lShoulder);
		PVector rNeck = PVector.sub(head, rShoulder);

		// torso
		PVector lTorso = PVector.sub(lHip, lShoulder);
		PVector rTorso = PVector.sub(rHip, rShoulder);

		// legs
		PVector rUpperLeg = PVector.sub(rHip, rKnee);
		PVector rLowerLeg = PVector.sub(rKnee, rFoot);

		PVector lUpperLeg = PVector.sub(lHip, lKnee);
		PVector lLowerLeg = PVector.sub(lKnee, lFoot);

		float rAngleLeg = PVector.angleBetween(rUpperLeg, rLowerLeg);
		float lAngleLeg = PVector.angleBetween(lUpperLeg, lLowerLeg);

		rAngleLeg = (float) Math.toDegrees(rAngleLeg);
		lAngleLeg = (float) Math.toDegrees(lAngleLeg);

		float lTorsoAngle = PVector.angleBetween(lTorso, lUpperLeg);
		float rTorsoAngle = PVector.angleBetween(rTorso, rUpperLeg);

		lTorsoAngle = (float) Math.toDegrees(lTorsoAngle);
		rTorsoAngle = (float) Math.toDegrees(rTorsoAngle);
		float torsoAngle = (lTorsoAngle + rTorsoAngle) / 2;

		float lNeckAngle = PVector.angleBetween(lNeck, lTorso);
		float rNeckAngle = PVector.angleBetween(rNeck, rTorso);

		float lAngleNeck = (float) Math.toDegrees(lNeckAngle);
		float rAngleNeck = (float) Math.toDegrees(rNeckAngle);
		float neckAngle = (lNeckAngle + rNeckAngle) / 2;


		if(lAngleLeg>60){
			framePreds.setFrameLeftLegCurve(LEFTLEG_CURVE.BENT);
		} else if(lAngleLeg<=25){
			framePreds.setFrameLeftLegCurve(LEFTLEG_CURVE.STRAIGHT);
		} else {
			framePreds.setFrameLeftLegCurve(LEFTLEG_CURVE.NONE);
		}
		
		if(rAngleLeg>60){
			framePreds.setFrameRightLegCurve(RIGHTLEG_CURVE.BENT);
		} else if(rAngleLeg<=25){
			framePreds.setFrameRightLegCurve(RIGHTLEG_CURVE.STRAIGHT);
		}

		// height as distance b/t head and furthest knee
		// note: buggy if one leg goes through floor
		float curHeight = Math.max(PVector.dist(head, lKnee), PVector.dist(head, rKnee));

		// set baseline height value based on standing straight
		if (torsoAngle>155 && lAngleLeg<=25 && rAngleLeg<=25) {
			baseHeight = Math.max(curHeight,baseHeight);
		}

		if (curHeight / baseHeight < 0.83) {
			framePreds.setFrameHeight(HEIGHT.SHORT);
		} else if (curHeight / baseHeight < 0.93) {
			framePreds.setFrameHeight(HEIGHT.MEDIUM);
		} else {
			framePreds.setFrameHeight(HEIGHT.TALL);
		}



		float base1 = PVector.sub(rHand, lHand).mag();
		float base2 = PVector.sub(rKnee, lKnee).mag();
		float side1 = PVector.sub(rHand, rKnee).mag();
		float side2 = PVector.sub(lHand, lKnee).mag();


		forPrinting += "hand dist: " + Math.round(base1) + "\n";
		forPrinting += "knee dist: " + Math.round(base2) + "\n";
		forPrinting += "r dist: " + Math.round(side1) + "\n";
		forPrinting += "l dist: " + Math.round(side2) + "\n";

		// http://en.wikipedia.org/wiki/Trapezoid#Midsegment_and_height
		float trapHeight = (float) (Math.sqrt((-base1+base2+side1+side2) * 
				(base1-base2+side1+side2) * 
				(base1-base2+side1-side2) * 
				(base1-base2-side1+side2))
				/ (2*Math.abs(base2 - base1)));
		float trapArea = ((base1+base2)/2) * trapHeight;

		forPrinting += "trapezoid area: " + Math.round(trapArea) + "\n";

		if (trapArea < 150000) {
			framePreds.setFrameSize(SIZE.SMALL);
		} else if (trapArea < 320000) {
			framePreds.setFrameSize(SIZE.MEDIUM);
		} else {
			framePreds.setFrameSize(SIZE.LARGE);
		}

		// quadrant predicates
		PVector humanCent = new PVector(head.z, head.x);
		//    forPrinting += "human:(" + round(head.x) + "," + round(head.z) + ")\n";

		if (head.x < 0 && head.z < 2600) {
			framePreds.setFrameQuadrant(QUADRANT.BOTTOM_LEFT);
		} else if (head.x < 0 && head.z >= 2600) {
			framePreds.setFrameQuadrant(QUADRANT.TOP_LEFT);
		} else if (head.x >= 0 && head.z < 2600) {
			framePreds.setFrameQuadrant(QUADRANT.BOTTOM_RIGHT);
		} else if (head.x >= 0 && head.z >= 2600) {
			framePreds.setFrameQuadrant(QUADRANT.TOP_RIGHT);
		}

		// predicates for human distance from center
		//    println("distance center: " + humanCent.mag());
		//    forPrinting += "distCent=" + humanCent.mag() + "; ";
		if (humanCent.mag() < 2000) {
			framePreds.setFrameDistCenter(DIST_CENTER.NEAR);
		} else if (humanCent.mag() < 2500) {
			framePreds.setFrameDistCenter(DIST_CENTER.MEDIUM);
		} else {
			framePreds.setFrameDistCenter(DIST_CENTER.FAR);
		}

		if ((LEFTARM_CURVE)framePreds.getFrameLeftArmCurve() == LEFTARM_CURVE.STRAIGHT &&
				(RIGHTARM_CURVE)framePreds.getFrameRightArmCurve() == RIGHTARM_CURVE.STRAIGHT) {
			framePreds.setFrameBothArmCurve(BOTHARM_CURVE.STRAIGHT);
		} else if ((LEFTARM_CURVE)framePreds.getFrameLeftArmCurve() == LEFTARM_CURVE.BENT &&
				(RIGHTARM_CURVE)framePreds.getFrameRightArmCurve() == RIGHTARM_CURVE.BENT) {
			framePreds.setFrameBothArmCurve(BOTHARM_CURVE.BENT);
		} else {
			framePreds.setFrameBothArmCurve(BOTHARM_CURVE.NONE);
		}

		boolean matchingArmCurve = framePreds.getFrameLeftArmCurve().name().equals(framePreds.getFrameRightArmCurve().name());
		boolean matchingHandPos = framePreds.getFrameLeftHandPos().name().equals(framePreds.getFrameRightHandPos().name());
		boolean matchingHandHeight = framePreds.getFrameLeftHandHeight().name().equals(framePreds.getFrameRightHandHeight().name());

		boolean matchingLeftCurve = framePreds.getFrameLeftLegCurve().name().equals(framePreds.getFrameRightLegCurve().name());
		
		if (matchingArmCurve && matchingHandPos && matchingHandHeight) {
			framePreds.setFrameBodySymmetric(BODY_SYMMETRIC.SYMMETRIC);
		} else {
			framePreds.setFrameBodySymmetric(BODY_SYMMETRIC.ASYMMETRIC);
		}

		PVector leftHandToElbow = PVector.sub(lHand,rElbow);
		PVector rightHandToElbow = PVector.sub(rHand,lElbow);

		boolean leftNearRightElbow = leftHandToElbow.mag()<300.0f;
		boolean rightNearLeftElbow = rightHandToElbow.mag()<300.0f;

		if (leftNearRightElbow && rightNearLeftElbow) {
			framePreds.setFrameArmsCrossed(ARMS_CROSSED.CROSSED);
		} else {
			framePreds.setFrameArmsCrossed(ARMS_CROSSED.NOT_CROSSED);
		}
		
		
		boolean handsTogether = PVector.sub(rHand,lHand).mag()<120.0f;
		if (handsTogether) {
			framePreds.setFrameHandsTogether(HANDS_TOGETHER.TOGETHER);
		} else {
			framePreds.setFrameHandsTogether(HANDS_TOGETHER.APART);
		}
		
		framePreds.setFrameBothHandsByChest(BOTH_HANDS_BY_CHEST.NONE); // TODO: NYI - never coded up in old version
	}

	void setJoints(Body joints) {
		curJoints = (Body) joints.clone();
	}

	void setRelJoints() {
		JIDX[] leftSide = { JIDX.LEFT_SHOULDER, JIDX.LEFT_ELBOW, 
							JIDX.LEFT_HAND, JIDX.LEFT_FINGERTIP, JIDX.LEFT_KNEE, 
							JIDX.LEFT_ANKLE, JIDX.LEFT_FOOT, JIDX.LEFT_HIP};
		
		JIDX[] rightSide = {JIDX.RIGHT_SHOULDER, JIDX.RIGHT_ELBOW, 
							JIDX.RIGHT_HAND, JIDX.RIGHT_FINGERTIP, JIDX.RIGHT_KNEE, 
							JIDX.RIGHT_ANKLE, JIDX.RIGHT_FOOT, JIDX.RIGHT_HIP};
		
		// create relative skeleton
		if (prev2Joints.get(JIDX.HEAD) != null) {
			curRelJoints.put(JIDX.HEAD,PVector.sub(curJoints.get(JIDX.HEAD), curJoints.get(JIDX.TORSO)));
			
			for (JIDX jidx : leftSide) {
				curRelJoints.put(jidx, PVector.sub(curJoints.get(jidx), curJoints.get(JIDX.LEFT_HIP)));
			}
			
			for (JIDX jidx : rightSide) {
				curRelJoints.put(jidx, PVector.sub(curJoints.get(jidx), curJoints.get(JIDX.RIGHT_HIP)));
			}
		}
	}
	
	/*void updateJointHistory() {
		if (runningFrames % frameSamplingRate == 0) {
			jointHist.add((Body) curJoints.clone());

			// shrink history if too many positions have been added, treating as LILO queue
			while (jointHist.size() > maxJointHistSize) {
				jointHist.remove(0);
			}
		}
	}*/
	

	/**
	 * Update histories for each of the tracked Viewpoints predicates and total set of per-frame predicates
	 */
	/*void updatePredicateHistory() {
		// set previous frame predicates to use running averages to smooth
		framePreds.setFrameTempo(runningTempo);
		//framePreds.setFrameLeftLegStillness(runningLeftLegStillness);
		framePreds.setFrameEnergy(runningEnergy);
		framePreds.setFrameSmoothness(runningSmooth);
		predHist.add(framePreds);

		tempoPredHist.add(tempoPred);
		leftLegStillnessPredHist.add(leftLegStillnessPred);
		rightLegStillnessPredHist.add(rightLegStillnessPred);
		leftHandStillnessPredHist.add(leftHandStillnessPred);
		rightHandStillnessPredHist.add(rightHandStillnessPred);
		leftLegTransversePredHist.add(leftLegTransversePred);
		rightLegTransversePredHist.add(rightLegTransversePred);
		leftHandTransversePredHist.add(leftHandTransversePred);
		rightHandTransversePredHist.add(rightHandTransversePred);
		leftLegLongitudinalPredHist.add(leftLegLongitudinalPred);
		rightLegLongitudinalPredHist.add(rightLegLongitudinalPred);
		leftHandLongitudinalPredHist.add(leftHandLongitudinalPred);
		rightHandLongitudinalPredHist.add(rightHandLongitudinalPred);
		leftLegVerticalPredHist.add(leftLegVerticalPred);
		rightLegVerticalPredHist.add(rightLegVerticalPred);
		leftHandVerticalPredHist.add(leftHandVerticalPred);
		rightHandVerticalPredHist.add(rightHandVerticalPred);
		energyPredHist.add(energyPred);
		smoothPredHist.add(smoothPred);

		if (runningFrames % 300 == 0) {
			runningFrames = 0;
		}
	}*/

	public Gesture yieldCurrentGesture() {
		hasGesture = false;
		return currentGesture;
	}
	
	public JointSpaceGesture yieldJSGesture() {
		hasGesture = false;
		return jsGesture;
	}

	public Boolean hasGesture() {
		return hasGesture;
	}
}
