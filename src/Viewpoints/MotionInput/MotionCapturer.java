package Viewpoints.MotionInput;

import java.util.*;

import processing.core.PApplet;
import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Shared.*;
import Viewpoints.Visualisation.ViewpointsGraphics;

public class MotionCapturer extends ViewpointsGraphics {
	private Clock clock;
	private KinectMotionInput kinectInput = null;
	ArrayList<Body> captured = new ArrayList<Body>();
	
	public void setup() {
		size(displayWidth, displayHeight);
		clock = new Clock();
		kinectInput = new KinectMotionInput(new SimpleOpenNI(this));
		if (!kinectInput.init()) {
			background(255, 0, 0);
			noLoop();
			kinectInput = null;
		}
		stroke(255);
		strokeWeight(1);
	}
	
	public void draw() {
		if (null != kinectInput) {
			clock.check();
			background(0);
			translate(width / 2, height / 2);
			Body pose = kinectInput.next(clock);
			if (null != pose && kinectInput.isAnyoneOnScreen(this.g)) {
				//System.out.println("got pose");
				captured.add(pose);
				drawBody(pose);
			} else {
				if (!captured.isEmpty()) {
					System.out.println("out");
					JointSpaceGesture jsg = new JointSpaceGesture(captured);
					FileUtilities.serializeJointsGesture("Capture.jsg", jsg);
					captured.clear();
				}
			}
		}
	}
	
	public boolean sketchFullScreen() {
		return true;
	}
	
	public void onNewUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.MotionInput.MotionCapturer", new String[]{"--full-screen", "--display=1"});
	}
}
