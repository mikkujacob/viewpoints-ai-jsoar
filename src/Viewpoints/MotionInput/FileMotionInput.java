package Viewpoints.MotionInput;

import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.Clock;
import Viewpoints.Shared.JointSpaceGesture;
import Viewpoints.Shared.LeaderRecord;

public class FileMotionInput implements MotionInput {
	private String filename = null;
	private boolean isInit = false;
	private LeaderRecord motionRecord = null;
	
	public FileMotionInput(String filename) {
		this.filename = filename;
	}
	
	public boolean init() {
		JointSpaceGesture jsg = FileUtilities.deserializeJointsGesture(filename);
		if (null != jsg) {
			motionRecord = new LeaderRecord(jsg.getGestureFramesList());
			isInit = true;
		}
		return isInit;
	}
	
	public boolean isInit() {
		return isInit;
	}
	
	public Body next(Clock clock) {
		float deltatime = clock.deltatime();
		return motionRecord.replayFrame(deltatime);
	}
}
