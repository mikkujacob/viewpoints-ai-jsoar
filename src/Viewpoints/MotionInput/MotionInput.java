package Viewpoints.MotionInput;

import processing.core.PApplet;
import Viewpoints.Shared.*;

public interface MotionInput {
	public boolean init();
	public boolean isInit();
	public Body next(Clock clock);
}
