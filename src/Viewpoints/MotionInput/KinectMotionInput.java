package Viewpoints.MotionInput;

import processing.core.*;
import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.Clock;

public class KinectMotionInput implements MotionInput {
	private SimpleOpenNI kinect = null;
	private boolean isInit = false;
	
	public KinectMotionInput(SimpleOpenNI kinect) {
		this.kinect = kinect;
	}
	
	public boolean init() {
		if (!kinect.init()) return false;
		kinect.enableDepth();
		kinect.enableUser();
		isInit = true;
		return true;
	}
	
	public boolean isInit() {
		return isInit;
	}
	
	public boolean isAnyoneOnScreen(PGraphics pgraphics) {
		PImage userimage = kinect.userImage();
		userimage.loadPixels();
		for (int i = 0; i < userimage.pixels.length; i++) {
			int color = userimage.pixels[i];
			float r = pgraphics.red(color);
			float g = pgraphics.blue(color);
			float b = pgraphics.green(color);
			if (r != g || g != b) {return true;}
		}
		return false;
	}
	
	public Body next(Clock clock) {
		kinect.update();
		int[] userList = kinect.getUsers();
		for(int i = 0; i < userList.length; i++)
		{
			if (kinect.isTrackingSkeleton(userList[i])) {
				Body userPose = new Body();
				userPose.initialize(kinect, userList[i], clock.time());
				return userPose;
			}
		}
		return null;
	}
}
