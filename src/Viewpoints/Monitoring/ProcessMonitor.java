package Viewpoints.Monitoring;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Scanner;

import Viewpoints.FileUtilities.FileUtilities;

/***
 * Class to automatically load Viewpoints AI installation and to continuously monitor
 * execution of PerceptionAction and Reasoning Modules and restart them if necessary.
 * @author mjacob6
 *
 */
public class ProcessMonitor
{
	public static Boolean isWindows = false;
	public Boolean isCheckingActiveTime = false;
	public Boolean isLoggingEnabled = true;
	
	public static String logDir = File.separator + "log";
	public static String statusDir = File.separator + "status";
	public static String logFileName;
	public String perceptionActionStatusFileName = "PerceptionActionStatus.txt";
	public String perceptionActionVAIStatusFileName = "PerceptionActionVAIStatus.txt";
	public String perceptionActionUserStatusFileName = "PerceptionActionUserStatus.txt";
	public String reasoningStatusFileName = "ReasoningStatus.txt";
	public String reasoningAgentStatusFileName = "ReasoningAgentStatus.txt";
	
	public static String CommandLineCommandCommon;
	public static String[] CommandLineCommandCommonArray;
	public String processNameReasoning = "Viewpoints.JSOAR.ViewpointsAIJSOAR";
	public String processNamePerceptionAction = "Viewpoints.Visualisation.Vinput";
	
	public static String JAVA_HOME;
	public static String PROJECT_HOME;
	
	private File perceptionActionStatusFile;
	private File perceptionActionVAIStatusFile;
	private File perceptionActionUserStatusFile;
	private File reasoningStatusFile;
	private File reasoningAgentStatusFile;
	
	private long perceptionActionCounter = 0;
	private long perceptionActionVAICounter = 0;
	private long perceptionActionUserCounter = 0;
	private long reasoningCounter = 0;
	private long reasoningAgentCounter = 0;
	
	private Boolean isPerceptionActionWorking = true;
	private Boolean isPerceptionActionVAIWorking = true;
	private Boolean isPerceptionActionUserWorking = true;
	private Boolean isReasoningWorking = true;
	private Boolean isReasoningAgentWorking = true;
	private Boolean isReasoningAgentMonitorable = true;
	private Boolean isPerceptionActionUserMonitorable = true;
	private Boolean isPerceptionActionVAIMonitorable = true;   
	
	private static Scanner inputScanner;
	private static String[] args;
	
    private final String STARTTIMESTRING = "16:59:59";
    private final String ENDTIMESTRING = "23:59:59";
    
    private Boolean isActive = true;
	
	Runtime rt;
	
	private static ProcessBuilder processBuilder;
	private static HashMap<String, Process> ProcessMap = new HashMap<String, Process>();
	
	
	/***
	 * Public default constructor for ProcessMonitor class
	 */
	public ProcessMonitor()
	{
		
	}
	
	public static void initMain()
	{
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)  
		{
			isWindows = true;
		}
		
		JAVA_HOME = System.getProperty("java.home");
		if(JAVA_HOME.equalsIgnoreCase(null))
		{
			JAVA_HOME = System.getenv("JAVA_HOME");
		}
		
		PROJECT_HOME = System.getProperty("user.dir");
		if(PROJECT_HOME.equalsIgnoreCase(null))
		{
			try
			{
				PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
			}
			catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(isWindows)
		{
			//TODO TEST
//			CommandLineCommandCommon = "\"" + JAVA_HOME + File.separator + "bin" + File.separator + "java\" -Dfile.encoding=Cp1252 -classpath " + PROJECT_HOME + "" + File.separator + "bin;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-core-0.12.0.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-debugger-0.12.0.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-api-1.6.1.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-jdk14-1.6.1.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-base" + File.separator + "core" + File.separator + "core.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "controlP5.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "oscP5-0.9.8" + File.separator + "oscP5.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "polymonkey.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "SimpleOpenNI" + File.separator + "library" + File.separator + "SimpleOpenNI.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "sqlite-jdbc-3.7.2.jar ";
//			CommandLineCommandCommonArray = new String[] {"\"" + JAVA_HOME + File.separator + "bin" + File.separator + "java\"", "-Dfile.encoding=Cp1252", "-classpath", PROJECT_HOME + "" + File.separator + "bin;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-core-0.12.0.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-debugger-0.12.0.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-api-1.6.1.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-jdk14-1.6.1.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-base" + File.separator + "core" + File.separator + "core.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "controlP5.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "oscP5-0.9.8" + File.separator + "oscP5.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "polymonkey.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "SimpleOpenNI" + File.separator + "library" + File.separator + "SimpleOpenNI.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "sqlite-jdbc-3.7.2.jar", "'"};
			CommandLineCommandCommonArray = new String[] {"java", "-Dfile.encoding=Cp1252", "-classpath", PROJECT_HOME + "" + File.separator + "bin;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-core-0.12.0.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-debugger-0.12.0.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-api-1.6.1.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-jdk14-1.6.1.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-base" + File.separator + "core" + File.separator + "core.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "controlP5.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "oscP5-0.9.8" + File.separator + "oscP5.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "polymonkey.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "SimpleOpenNI" + File.separator + "library" + File.separator + "SimpleOpenNI.jar;" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "sqlite-jdbc-3.7.2.jar", "'"};
		}
		else
		{
//			CommandLineCommandCommon = JAVA_HOME + File.separator + "bin" + File.separator + "java -Dfile.encoding=MacRoman -classpath " + PROJECT_HOME + "" + File.separator + "bin:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-core-0.12.0.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-debugger-0.12.0.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-api-1.6.1.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-jdk14-1.6.1.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-base" + File.separator + "core" + File.separator + "core.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "controlP5.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "oscP5-0.9.8" + File.separator + "oscP5.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "polymonkey.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "SimpleOpenNI" + File.separator + "library" + File.separator + "SimpleOpenNI.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "sqlite-jdbc-3.7.2.jar ";
			CommandLineCommandCommonArray = new String[] {"java", "-Dfile.encoding=MacRoman", "-classpath", PROJECT_HOME + "" + File.separator + "bin:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-core-0.12.0.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "jsoar-debugger-0.12.0.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-api-1.6.1.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "slf4j-jdk14-1.6.1.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-base" + File.separator + "core" + File.separator + "core.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "controlP5.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "oscP5-0.9.8" + File.separator + "oscP5.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "polymonkey.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "processing-user" + File.separator + "SimpleOpenNI" + File.separator + "library" + File.separator + "SimpleOpenNI.jar:" + PROJECT_HOME + "" + File.separator + "lib" + File.separator + "jsoar-0.12.0" + File.separator + "lib" + File.separator + "sqlite-jdbc-3.7.2.jar", "'"};
		}
		statusDir = PROJECT_HOME + statusDir;
		logDir = PROJECT_HOME + logDir;
		
		logFileName = getFormattedCurrentTime().replace(':', '-') + ".log";
		
		log("Is Windows: " + isWindows);
		
		if(JAVA_HOME.equalsIgnoreCase(null))
		{
			log("ERROR! JAVA_HOME IS NULL");
		}
		
		if(PROJECT_HOME.equalsIgnoreCase(null))
		{
			log("ERROR! PROJECT_HOME IS NULL");
		}
	}
	
	/***
	 * Setup monitoring files and other initialization code.
	 */
	public void setup()
	{ 
		rt = Runtime.getRuntime();
		
		perceptionActionStatusFile = new File(statusDir, perceptionActionStatusFileName);
		perceptionActionVAIStatusFile = new File(statusDir, perceptionActionVAIStatusFileName);
		perceptionActionUserStatusFile = new File(statusDir, perceptionActionUserStatusFileName);
		reasoningStatusFile = new File(statusDir, reasoningStatusFileName);
		reasoningAgentStatusFile = new File(statusDir, reasoningAgentStatusFileName);
		
		perceptionActionCounter = 0;
		perceptionActionVAICounter = 0;
		perceptionActionUserCounter = 0;
		reasoningCounter = 0;
		reasoningAgentCounter = 0;
		
		isPerceptionActionWorking = true;
		isPerceptionActionVAIWorking = true;
		isPerceptionActionUserWorking = true;
		isReasoningWorking = true;
		isReasoningAgentWorking = true;
		isReasoningAgentMonitorable = true;
	}
	
	/***
	 * Monitor PerceptionAction and Reasoning Modules to check if they crash.
	 */
	public void monitor()
	{
		try
		{
			do
			{	
				//Get input
				if(System.in.available() > 0)
				{
					String input = inputScanner.nextLine();
					if(input.equalsIgnoreCase("quit") || input.equalsIgnoreCase("exit") || 
							input.equalsIgnoreCase("kill"))
					{
						log("Quitting ProcessMonitor...");
						return;
					}
				}
				
				//Check if system should be active right now
				while(isCheckingActiveTime && !checkActivity());
				
				//Monitor other process threads for timely execution.
				isReasoningWorking = monitorReasoning();
				isReasoningAgentWorking = monitorReasoningAgent();
				isPerceptionActionWorking = monitorPerceptionAction();
				isPerceptionActionVAIWorking = monitorPerceptionActionVAI();
				isPerceptionActionUserWorking = monitorPerceptionActionUser();
				
				if(!isReasoningWorking || !isReasoningAgentWorking)
				{
					//Reasoning Module crashed
					//Restart...
					log("Reasoning Module crashed...");
					log("Restarting Reasoning Module...");
					stopReasoning();
					reasoningCounter = 0;
					reasoningAgentCounter = 0;
					startReasoning();
				}
				
				if(!isPerceptionActionWorking || !isPerceptionActionUserWorking || !isPerceptionActionVAIWorking)
				{
					//PerceptionAction Module
					log("PerceptionAction Module crashed...");
					log("Restarting PerceptionAction Module...");
					stopPerceptionAction();
					perceptionActionCounter = 0;
					startPerceptionAction();
				}
				
				try
				{
					//Sleep while not in use.
					Thread.sleep(20000);
				}
				catch (InterruptedException e)
				{
					log("Sleep interrupted...");
				}
				
				try
				{
					//Sleep while not in use.
					Thread.sleep(10000);
				}
				catch (InterruptedException e)
				{
					log("Sleep interrupted...");
				}
			}
			while(true);
		}
		catch(Exception e)
		{
			//If self crashes / exceptions out...
			//Restart Self.
			log("ProcessMonitor crashed...");
			log("Restarting ProcessMonitor...");
			stopAllProcesses();
			main(args);
		}
	}

	/**
	 * Checks if system is currently supposed to be active.
	 * If not, sleep till it is.
	 * Else, wake up and / or continue execution
	 */
	private boolean checkActivity()
	{
		//If, before 5 PM or after 12 AM
		if(!isActiveTime(STARTTIMESTRING, ENDTIMESTRING))
		{
			//If, system is currently active
			if(isActive)
			{
				log("System hybernating...");
				log("Yawn... *rubs eyes*");
				log("Destroying all Processes...");
				stopAllProcesses();
				isActive = false;
				
				try
				{
					//Sleep till system can be reactivated
					Thread.sleep(checkRemainingTime(STARTTIMESTRING, ENDTIMESTRING));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				log("ERROR! Thread woke up or somehow system is active but isActive = false");
			}
			
			return false;
		}//If, between 5 PM and 12 AM
		else
		{
			//If, system is not yet currently active
			if(!isActive)
			{
				log("System awakening...");
				log("Yawn... *rubs eyes*");
				log("Start All Processes: " + startAllProcesses());
				isActive = true;
			}
			
			return true;
		}
	}

	/**
	 * Return time in milliseconds till System should become active
	 * @param startTimeString - String representing start time
	 * @param endTimeString - String representing end time
	 * @return time in milliseconds till System should become active
	 */
	private long checkRemainingTime(String startTimeString, String endTimeString)
	{
		try
		{
			Date startTime = new SimpleDateFormat("HH:mm:ss").parse(startTimeString);
		    Calendar startTimeCalendar = Calendar.getInstance();
		    startTimeCalendar.setTime(startTime);
	
		    Date endTime = new SimpleDateFormat("HH:mm:ss").parse(endTimeString);
		    Calendar endTimeCalendar = Calendar.getInstance();
		    endTimeCalendar.setTime(endTime);
	
		    Date currentTime = new Date();
		    Calendar currentCalendar = Calendar.getInstance();
		    currentCalendar.setTime(currentTime);
		    
		    startTimeCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), currentCalendar.get(Calendar.DATE));
		    endTimeCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), currentCalendar.get(Calendar.DATE));
	
		    Date testTime = currentCalendar.getTime();
		    if(testTime.before(startTimeCalendar.getTime()))
		    {
		    	return (startTimeCalendar.getTimeInMillis() - currentCalendar.getTimeInMillis()) + 1000;
		    }
		    else if(testTime.after(endTimeCalendar.getTime()))
		    {
		    	startTimeCalendar.add(Calendar.DATE, 1);
		    	return (startTimeCalendar.getTimeInMillis() - currentCalendar.getTimeInMillis()) + 1000;
		    }
		}
		catch (ParseException e)
		{
		    e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Returns true if current time is between start and end times,
	 * representing time range for system activity.
	 * @param startTimeString - String representing start time
	 * @param endTimeString - String representing end time
	 * @return true if system should be active or false otherwise
	 */
	public Boolean isActiveTime(String startTimeString, String endTimeString)
	{
		try
		{
			Date startTime = new SimpleDateFormat("HH:mm:ss").parse(startTimeString);
		    Calendar startTimeCalendar = Calendar.getInstance();
		    startTimeCalendar.setTime(startTime);
	
		    Date endTime = new SimpleDateFormat("HH:mm:ss").parse(endTimeString);
		    Calendar endTimeCalendar = Calendar.getInstance();
		    endTimeCalendar.setTime(endTime);
	
		    Date currentTime = new Date();
		    Calendar currentCalendar = Calendar.getInstance();
		    currentCalendar.setTime(currentTime);
		    
		    startTimeCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), currentCalendar.get(Calendar.DATE));
		    endTimeCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), currentCalendar.get(Calendar.DATE));
	
		    Date testTime = currentCalendar.getTime();
		    if (testTime.after(startTimeCalendar.getTime()) && testTime.before(endTimeCalendar.getTime()))
		    {
		        return true;
		    }
		}
		catch (ParseException e)
		{
		    e.printStackTrace();
		}
		
		return false;
	}

	/***
	 * Monitor PerceptionAction Module Main Thread to check for crashes
	 */
	public Boolean monitorPerceptionAction()
	{
		try
		{
			Scanner perceptionActionScanner = new Scanner(perceptionActionStatusFile);
			if(perceptionActionScanner.hasNextLong())
			{
				long input = perceptionActionScanner.nextLong();
				if(input > perceptionActionCounter)
				{
					perceptionActionCounter = input;
					if(perceptionActionCounter < Long.MAX_VALUE && perceptionActionCounter > Long.MAX_VALUE - 100)
					{
						perceptionActionCounter = 0;
					}
					perceptionActionScanner.close();
					return true;
				}
				else
				{
					//Thread crashed
					log("PerceptionAction Module Main Thread crashed");
					perceptionActionScanner.close();
					return false;
				}
			}
			else
			{
				//Thread crashed
				log("PerceptionAction Module Main Thread crashed");
				perceptionActionScanner.close();
				return false;
			}
		}
		catch (FileNotFoundException e)
		{
			log("Status file for PerceptionAction Module Main Thread not found...");
			return true;
		}
	}
	
	/**
	 * Monitor PerceptionAction Module User Thread to check for crashes
	 * @return
	 */
	private Boolean monitorPerceptionActionUser() {
		try
		{
			Scanner perceptionActionUserScanner = new Scanner(perceptionActionUserStatusFile);
			if(perceptionActionUserScanner.hasNextLong())
			{
				long input = perceptionActionUserScanner.nextLong();
				if(input < 0)
				{
					isPerceptionActionUserMonitorable = false;
				}
				else
				{
					isPerceptionActionUserMonitorable = true;
				}
				
				if(isPerceptionActionUserMonitorable && input > perceptionActionUserCounter)
				{
					perceptionActionUserCounter = input;
					if(perceptionActionUserCounter < Long.MAX_VALUE && perceptionActionUserCounter > Long.MAX_VALUE - 100)
					{
						perceptionActionUserCounter = 0;
					}
					perceptionActionUserScanner.close();
					return true;
				}
				else if (!isPerceptionActionUserMonitorable)
				{
					//Reasoning Agent Thread halted  
					perceptionActionUserScanner.close();
					return true;
				}
				else
				{
					//Reasoning Agent Thread crashed
					log("Perception Module User Thread crashed");
					perceptionActionUserScanner.close();
					return false;
				}
			}
			else
			{
				//Reasoning Agent Thread crashed
				log("Perception Module User Thread crashed");
				perceptionActionUserScanner.close();
				return false;
			}
		}
		catch (FileNotFoundException e)
		{
			log("Status file for Perception Module User Thread not found...");
			return true;
		}
	}

	/**
	 * Monitor PerceptionAction Module VAI Thread to check for crashes
	 * @return
	 */
	private Boolean monitorPerceptionActionVAI() {
		try
		{
			Scanner perceptionActionVAIScanner = new Scanner(perceptionActionVAIStatusFile);
			if(perceptionActionVAIScanner.hasNextLong())
			{
				long input = perceptionActionVAIScanner.nextLong();
				if(input < 0)
				{
					isPerceptionActionVAIMonitorable = false;
				}
				else
				{
					isPerceptionActionVAIMonitorable = true;
				}
				
				if(isPerceptionActionVAIMonitorable && input > perceptionActionVAICounter)
				{
					perceptionActionVAICounter = input;
					if(perceptionActionVAICounter < Long.MAX_VALUE && perceptionActionVAICounter > Long.MAX_VALUE - 100)
					{
						perceptionActionVAICounter = 0;
					}
					perceptionActionVAIScanner.close();
					return true;
				}
				else if (!isPerceptionActionVAIMonitorable)
				{
					//Reasoning Agent Thread halted  
					perceptionActionVAIScanner.close();
					return true;
				}
				else
				{
					//Reasoning Agent Thread crashed
					log("Perception Module VAI Thread crashed");
					perceptionActionVAIScanner.close();
					return false;
				}
			}
			else
			{
				//Reasoning Agent Thread crashed
				log("Perception Module VAI Thread crashed");
				perceptionActionVAIScanner.close();
				return false;
			}
		}
		catch (FileNotFoundException e)
		{
			log("Status file for Perception Module VAI Thread not found...");
			return true;
		}
	}
	
	/***
	 * Monitor Reasoning Module Main Thread to check for crashes
	 */
	public Boolean monitorReasoning()
	{
		try
		{
			Scanner reasoningScanner = new Scanner(reasoningStatusFile);
			if(reasoningScanner.hasNextLong())
			{
				long input = reasoningScanner.nextLong();
				if(input > reasoningCounter)
				{
					reasoningCounter = input;
					if(reasoningCounter < Long.MAX_VALUE && reasoningCounter > Long.MAX_VALUE - 100)
					{
						reasoningCounter = 0;
					}
					reasoningScanner.close();
					return true;
				}
				else
				{
					//Thread crashed
					log("Reasoning Module Main Thread crashed");
					reasoningScanner.close();
					return false;
				}
			}
			else
			{
				//Thread crashed
				log("Reasoning Module Main Thread crashed");
				reasoningScanner.close();
				return false;
			}
		}
		catch (FileNotFoundException e)
		{
			log("Status file for Reasoning Module Main Thread not found...");
			return true;
		}
	}
	
	/***
	 * Monitor Reasoning Module Agent Thread to check for crashes
	 */
	public Boolean monitorReasoningAgent()
	{
		try
		{
			Scanner reasoningAgentScanner = new Scanner(reasoningAgentStatusFile);
			if(reasoningAgentScanner.hasNextLong())
			{
				long input = reasoningAgentScanner.nextLong();
				if(input < 0)
				{
					isReasoningAgentMonitorable = false;
				}
				else
				{
					isReasoningAgentMonitorable = true;
				}
				
				if(isReasoningAgentMonitorable && input > reasoningAgentCounter)
				{
					reasoningAgentCounter = input;
					if(reasoningAgentCounter < Long.MAX_VALUE && reasoningAgentCounter > Long.MAX_VALUE - 100)
					{
						reasoningAgentCounter = 0;
					}
					reasoningAgentScanner.close();
					return true;
				}
				else if (!isReasoningAgentMonitorable)
				{
					//Reasoning Agent Thread halted  
					reasoningAgentScanner.close();
					return true;
				}
				else
				{
					//Reasoning Agent Thread crashed
					log("Reasoning Module Agent Thread crashed");
					reasoningAgentScanner.close();
					return false;
				}
			}
			else
			{
				//Reasoning Agent Thread crashed
				log("Reasoning Module Agent Thread crashed");
				reasoningAgentScanner.close();
				return false;
			}
		}
		catch (FileNotFoundException e)
		{
			log("Status file for Reasoning Module Agent Thread not found...");
			return true;
		}
	}
	
	public Boolean startAllProcesses()
	{
		return (startReasoning() && startPerceptionAction());
	}
	
	/***
	 * Start up the Reasoning Module
	 */
	public Boolean startReasoning()
	{
//		log(CommandLineCommandCommon + processNameReasoning);
		return startProcess(processNameReasoning);
	}
	
	/***
	 * Start up the PerceptionAction Module
	 */
	public Boolean startPerceptionAction()
	{
//		log(CommandLineCommandCommon + processNamePerceptionAction);
		return startProcess(processNamePerceptionAction);
	}
	
	/***
	 * Start a process
	 * @param processName - Process name to start
	 * @param commandLineCommand - parameters to pass while starting it
	 */
	public Boolean startProcess(String processName)
	{
		if(rt != null)
		{
			Process p;
			try
			{
				//In case a process with same name is already running, kill it first
				if(ProcessMap.get(processName) != null)
				{
					stopProcess(processName);
				}
				
				if(!isWindows)
				{
					//Start process
//					String commandLineCommand = CommandLineCommandCommon + processName;
//					p = rt.exec(commandLineCommand);
					
					CommandLineCommandCommonArray[CommandLineCommandCommonArray.length - 1] = processName;
					processBuilder = new ProcessBuilder(CommandLineCommandCommonArray);
					processBuilder.inheritIO();
					p = processBuilder.start();
				}
				else
				{
					//Start process
//					CommandLineCommandCommonArray[CommandLineCommandCommonArray.length - 1] = processName;
//					p = rt.exec(CommandLineCommandCommonArray);
					
					CommandLineCommandCommonArray[CommandLineCommandCommonArray.length - 1] = processName;
					processBuilder = new ProcessBuilder(CommandLineCommandCommonArray);
					processBuilder.inheritIO();
					p = processBuilder.start();
				}

				//Add to ProcessMap
				ProcessMap.put(processName, p);

				//Had to remove this as it was leading to memory leaks when Processes were stopped and started.
				//Using Java 1.7's ProcessBuilder.inheritIO() instead				
//				//Redirect all streams to this processes' console
//				inputStreamToOutputStream(p.getInputStream(), System.out, processName);
//				inputStreamToOutputStream(p.getErrorStream(), System.out, processName);
				
				return true;
			}
			catch (IOException e)
			{
				return false;
			}
			catch (Exception e)
			{
				return false;
			}
		}
		
		return false;
	}
	
	/***
	 * Stop the Reasoning Module
	 */
	public Boolean stopReasoning()
	{
		return stopProcess(processNameReasoning);
	}
	
	/***
	 * Stop the PerceptionAction Module
	 */
	public Boolean stopPerceptionAction()
	{
		return stopProcess(processNamePerceptionAction);
	}
	
	/***
	 * Stop / kill all running processes
	 */
	public static void stopAllProcesses()
	{
		for(String processName : ProcessMap.keySet())
		{
			stopProcess(processName);
		}
	}
	
	/***
	 * Stop / kill a running process
	 * @param processName - Process name to kill
	 */
	public static Boolean stopProcess(String processName)
	{
		Process p = ProcessMap.get(processName);
		if(p != null)
		{
			p.destroy();
			
			return true;
		}
		
		return false;
	}
	
	/***
	 * Logs message to System.out 
	 * @param text - Message to display
	 */
	public static void log(String text)
	{
		log(text, "Viewpoints.Monitoring.ProcessMonitor");
	}
	
	/***
	 * Logs message to System.out 
	 * @param text - Message to display
	 */
	public static void log(String text, String processName)
	{
		FileUtilities.writeToFile(logDir + File.separator + logFileName, "\n[" + getFormattedCurrentTime() + "] " + processName + " > " + text, true);
		System.out.println("\n" + processName + " > " + text);
	}
	
	public static String getFormattedCurrentTime()
	{
		Calendar now = Calendar.getInstance();
		String formattedCurrentTime;
		
		formattedCurrentTime = now.get(Calendar.YEAR) + ":";
		formattedCurrentTime += ((now.get(Calendar.MONTH) >= 9) ? (now.get(Calendar.MONTH) + 1) : ("0" + (now.get(Calendar.MONTH) + 1))) + ":";
		formattedCurrentTime += ((now.get(Calendar.DATE) >= 10) ? now.get(Calendar.DATE) : ("0" + now.get(Calendar.DATE))) + ":";
		formattedCurrentTime += ((now.get(Calendar.HOUR_OF_DAY) >= 10) ? now.get(Calendar.HOUR_OF_DAY) : ("0" + now.get(Calendar.HOUR_OF_DAY))) + ":";
		formattedCurrentTime += ((now.get(Calendar.MINUTE) >= 10) ? now.get(Calendar.MINUTE) : ("0" + now.get(Calendar.MINUTE))) + ":";
		formattedCurrentTime += ((now.get(Calendar.SECOND) >= 10) ? now.get(Calendar.SECOND) : ("0" + now.get(Calendar.SECOND))) + ":";
		formattedCurrentTime += ((now.get(Calendar.MILLISECOND) >= 10) ? ((now.get(Calendar.MILLISECOND) >= 100) ? now.get(Calendar.MILLISECOND) : ("0" + now.get(Calendar.MILLISECOND))) : ("00" + now.get(Calendar.MILLISECOND))) + "";
		
		return formattedCurrentTime;
	}
	
	/***
	 * Writes the data from the InputStream to the OutputStream
	 * @param inputStream - InputStream to redirect
	 * @param out - OutputStream to redirect to
	 */
	void inputStreamToOutputStream(final InputStream inputStream, final OutputStream out, final String streamName)
	{
	    Thread t = new Thread(new Runnable()
	    {
	        public void run()
	        {
	        	Scanner inputStreamScanner = new Scanner(inputStream);
	        	String text;
	            try
	            {
	            	inputStreamScanner.useDelimiter("\n");
	            	while(true)
	            	{
	            		if(inputStreamScanner.hasNext())
	            		{
	            			text = inputStreamScanner.next();
		            		log(text, streamName);
	            		}
	            	}
	            }
	            catch (Exception ex)
	            {
	            	ex.printStackTrace();
	            	log("ERROR! " + ex.toString(), streamName);
	                //TODO make a callback on exception.
	            }
	            finally
	            {
	            	inputStreamScanner.close();
	            }
	        }
	    });
	    t.setDaemon(true);
	    t.start();
	}
	
	/***
	 * Main method to start the ProcessMonitor process
	 * @param args
	 */
	public static void main(String[] args)
	{
		try
		{
			initMain();
			log("Starting ProcessMonitor...");
			inputScanner = new Scanner(System.in);
			ProcessMonitor.args = args;
			log("Type 'quit' / 'exit' / 'kill' to stop ProcessMonitor...");
			ProcessMonitor pm = new ProcessMonitor();
			pm.setup();
			log("Start All Processes: " + pm.startAllProcesses());
			
			try
			{
				//Sleep till processes start.
				Thread.sleep(30000);
			}
			catch (InterruptedException e)
			{
				log("Sleep interrupted...");
			}
			
			pm.monitor();
			log("Finishing ProcessMonitor...");
		}
		catch(Exception e)
		{
			//If self crashes / exceptions out...
			//Restart self.
			log("ProcessMonitor crashed...");
			log("Restarting ProcessMonitor...");
			stopAllProcesses();
			main(args);
		}
		finally
		{
			log("Destroying all Processes...");
			stopAllProcesses();
			log("All done. Goodbye...");
		}
	}
}
