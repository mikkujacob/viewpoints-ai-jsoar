package Viewpoints.Synchronous;

import java.awt.Color;
import java.util.*;

import ObjectiveParameters.ObjectiveParametersMetric;
import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.Fireflies.BodyField;
import Viewpoints.Fireflies.CentralField;
import Viewpoints.Fireflies.Field;
import Viewpoints.Fireflies.Firefly;
import Viewpoints.Fireflies.FireflyColorTable;
import Viewpoints.Fireflies.FireflyParameters;
import Viewpoints.MotionInput.FileMotionInput;
import Viewpoints.MotionInput.KinectMotionInput;
import Viewpoints.MotionInput.MotionInput;
import Viewpoints.Shared.*;
import processing.core.*;

public class RhythmVAI extends PApplet{
	/**
	 * 
	 */
	private static final boolean USE_KINECT = true;
	
	private Random random = new Random();
	private final static float CUTOFF = 200.0f;
	private Firefly[] fireflies = new Firefly[1000];
	private FireflyColorTable colorTable = FireflyColorTable.Standard(this);
	private static final long serialVersionUID = 1L;
	private MotionInput motionInput;
	private RhythmTaker rhythmTaker;
	private EffectsController effectsController = null;
	private ResponseLogic responseLogic;
	private Clock clock;
	
	public void setup() {
		clock = new Clock();
		size(displayWidth, displayHeight);
		if (USE_KINECT) {
			motionInput = new KinectMotionInput(new SimpleOpenNI(this));
		} else {
			motionInput = new FileMotionInput("Capture.jsg");
		}
		if (motionInput.init()) {
			rhythmTaker = new RhythmTaker(true);
			effectsController = new EffectsController(rhythmTaker, clock);
			launchFireflies();
			responseLogic = new SimilarityResponseLogic();
			responseLogic.start();
		} else {
			background(255, 0, 0);
			noLoop();
		}
		clock.start();
	}
		
	public void draw() {
		if (motionInput.isInit()) {
			clock.check();
			noStroke();
			fill(0, 0, 0, effectsController.activeParameters().FADE);
			rect(0, 0, displayWidth, displayHeight);
			Body userPose = motionInput.next(clock);
			Body vaiPose = null;
			if (null == userPose) {
				effectsController.update(null, null);
			} else {
				rhythmTaker.takePose(userPose);
				boolean rhythm = rhythmTaker.hasRhythm();
				LeaderRecord response = null;
				if (rhythm) {
					System.out.println("has rhythm");
					ArrayList<Body> segment = rhythmTaker.motionSegment();
					if (null != segment) {
						responseLogic.requestReactionTo(segment);
					}
					response = responseLogic.checkResponse();
				} else {
					System.out.println("no rhythm");
					responseLogic.cancel();
				}
				if (null != response) {
					response.setReplaySpeed(1.0f / rhythmTaker.getPeriod());
					vaiPose = response.replayFrame(clock.deltatime());
					PVector[] targetBasis = userPose.localBasis();
					float characteristicSize = userPose.characteristicSize();
					for (PVector basisVector : targetBasis) {
						basisVector.mult(characteristicSize);
					}
					vaiPose = vaiPose.transformed(PVecUtilities.STANDARD_BASIS, targetBasis, userPose.center());
				} else {
					vaiPose = (Body) userPose.clone();
				}
				vaiPose.setTimestamp(clock.time());
				effectsController.update(userPose, vaiPose);
			}
			fill(255);
			textAlign(LEFT, TOP);
			textSize(40);
			pushMatrix();
			translate(width/2, height/2);
			
			/*if (null != vaiPose) {
				drawBody(vaiPose);
			}*/
			
			for (int i = 0; i < fireflies.length; i++) {
				fireflies[i].fly(effectsController.getControlField(), effectsController.activeParameters(), clock.deltatime());
				drawFirefly(fireflies[i]);
			}
			
			if (!effectsController.isCloudMode()) {
				for (int i = 0; i < fireflies.length / 40; i++) {
					int idx = (int)(fireflies.length*random.nextFloat());
					fireflies[idx].teleport(effectsController.getControlField().teleportEndpoint(random));
				}
			}
			
			popMatrix();
		}
	}
	
	private PVector getScreenPos(PVector pos) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.x = - screenPos.x;
		screenPos.y = - screenPos.y;
		screenPos.z = 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	private void launchFireflies() {
		for (int i = 0; i < fireflies.length; i++) {
			PVector position = PVector.add(EffectsController.CLOUD_CENTER, 
					PVector.mult(PVecUtilities.randomDirection(random), effectsController.getCharSize()));
			PVector heading = PVecUtilities.randomDirection(random);
			fireflies[i] = new Firefly(position, heading, random);
		}
	}
	
	private void drawFirefly(Firefly firefly) {
		if (firefly.position.z < CUTOFF)
			return;
		int color = colorTable.getColor(effectsController.activeParameters().SPECTRE_PITCH, firefly.intencity, this.g);
		color = addHeartColor(firefly, color);
		stroke(color);
		PVector screenPos = getScreenPos(firefly.position);
		PVector lastScreenPos = getScreenPos(firefly.lastPosition);
		int sz = (int)(5.0f*screenPos.z + 2.0f*firefly.intencity);
		if (sz < 0)
			sz = 0;
		strokeWeight(sz);		
		line(lastScreenPos.x, lastScreenPos.y, screenPos.x, screenPos.y);
	}
	
	private int addHeartColor(Firefly firefly, int originalColor) {
		if (effectsController.isCloudMode() || effectsController.getHeartIgnition() < 0.1f) {
			return originalColor;
		} else {
			PVector delta = PVector.sub(effectsController.getHeartPosition(), firefly.position);
			float heartSize = (0.03f + 0.08f * effectsController.getCardio()) * effectsController.getCharSize();
			float intencityModulator = effectsController.getHeartIgnition() * (0.7f + 0.3f * effectsController.getCardio()) * Math.min(1.0f, heartSize * heartSize / delta.magSq());
			return lerpColor(originalColor, effectsController.getHeartColor(), intencityModulator);
		}
	}

	public void onNewUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public void drawBody(Body body) 
	{
		stroke(255);
		strokeWeight(1);
		
		for (Pair<JIDX, JIDX> limbEnds : LIDX.LIMB_ENDS().values()) {
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.Synchronous.RhythmVAI", new String[]{"--full-screen", "--display=1"});
	}
}
