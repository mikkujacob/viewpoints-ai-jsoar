package Viewpoints.Synchronous;

import java.util.*;

import Viewpoints.Shared.*;

public abstract class ResponseLogic extends Thread {
	private ArrayList<Body> userSegment = null;
	private ArrayList<Body> selectedInput = null;
	private LeaderRecord lastNormalizedRequest = null;
	private LeaderRecord response = null;
	private boolean cancelled = false;
	private static final float MIN_DEVIATION_DURATION = 0.5f;
	private static final float REPETITION_TOLERANCE = 0.25f;
	
	public void run() {
		while (true) {	
			synchronized(this) {
				while (null == userSegment) {
					try {wait();} catch(InterruptedException e) {}
				}
				System.out.println("take user gesture");
				selectedInput = userSegment;
				userSegment = null;
			}
			
			LeaderRecord suggestedResponse = response;
			JointSpaceGesture request = new JointSpaceGesture(selectedInput);
			JointSpaceGesture normalizedRequest = SegmentNormalization.normalize(request);
			
			if (different(normalizedRequest)) {
				System.out.println("evaluating response");
				lastNormalizedRequest = new LeaderRecord(normalizedRequest.getGestureFramesList());
				suggestedResponse = evaluateResponse(request, normalizedRequest);
			}
			
			synchronized(this) {
				selectedInput = null;
				if (!cancelled) {
					System.out.println("memorizing response");
					response = suggestedResponse;
				}
				cancelled = false;
			}
		}
	}
	
	public void requestReactionTo(ArrayList<Body> userSegment) {
		System.out.println("requesting response");
		synchronized(this) {
			System.out.println("response requested");
			this.userSegment = userSegment;
			notifyAll();
		}
	}
	
	public LeaderRecord checkResponse() {
		synchronized(this) {
			return response;
		}
	}
	
	public void cancel() {
		synchronized(this) {
			System.out.println("cancelling");
			if (null != selectedInput) {
				cancelled = true;
				response = null;
			}
		}
	}
	
	private boolean different(JointSpaceGesture newNormalizedSegment) {
		if (null == lastNormalizedRequest || null == response) {return true;}
		System.out.println("computing difference");
		ArrayList<Body> poses = newNormalizedSegment.getGestureFramesList();
		float deviationTime = 0;
		for (int i = 0; i < poses.size(); i++) {
			Body pose = poses.get(i);
			Body corresponding = lastNormalizedRequest.at(pose.getTimestamp());
			if (!pose.isSimilar(corresponding, REPETITION_TOLERANCE)) {
				if (i > 0) {
					deviationTime += (pose.getTimestamp() - poses.get(i-1).getTimestamp()) / 2;
				} else if (i < poses.size() - 1) {
					deviationTime += (poses.get(i+1).getTimestamp() - pose.getTimestamp()) / 2;
				}
			}
		}
		return deviationTime > MIN_DEVIATION_DURATION;
	}
	
	// TO BE IMPLEMENTED IN CHILD CLASSES!
	protected abstract LeaderRecord evaluateResponse(JointSpaceGesture input, JointSpaceGesture normalizedInput);
}
