package Viewpoints.Synchronous;

import java.awt.Color;

import processing.core.PVector;
import Viewpoints.Fireflies.BodyField;
import Viewpoints.Fireflies.CentralField;
import Viewpoints.Fireflies.Field;
import Viewpoints.Fireflies.FireflyParameters;
import Viewpoints.Shared.*;

public class EffectsController {
	public final static PVector CLOUD_CENTER = new PVector(0.0f, 0.0f, 1000.0f);
	
	private static final float CARDIO_RHYTHM = 3;
	private static final float TWO_CARDIO_DEV_SQR = (float) (8 * Math.PI * Math.PI / (CARDIO_RHYTHM * CARDIO_RHYTHM));
	
	private RhythmTaker rhythmTaker = null;
	private ViewpointsTracker viewpointsTracker = new ClassifierViewpointsTracker(); // new HardcodedViewpointsTracker();
	private Clock clock = null;
	private FireflyParameters fflyParams = new FireflyParameters();
	private FireflyParameters fflyCloudParams = new FireflyParameters();
	private boolean cloudMode = true;
	private float charSize = 0.0f;
	private float cardio = 0.0f;
	private float heartIgnition = 0.0f;
	private int heartColor = 0;
	private PVector heartPosition = null;
	private CentralField cloudField = new CentralField(CLOUD_CENTER);
	private BodyField vaiBodyField = null;
	
	public EffectsController(RhythmTaker rhythmTaker, Clock clock) {
		this.rhythmTaker = rhythmTaker;
		this.clock = clock;
		charSize = 700.0f;
		fflyCloudParams.adjustToScale(charSize / 6, charSize);
	}
	
	public void update(Body userPose, Body vaiPose) {
		operateHeartIgnition();
		if (null != vaiPose) { // TODO: here should actually be userPose! vaiPose is for testing.
			viewpointsTracker.takePose(vaiPose);
			operateViewpoints();
		}
		if (null == vaiPose) {
			cloudMode = true;
			viewpointsTracker.reset();
			vaiBodyField = null;
		} else {
			if (cloudMode) {
				cloudMode = false;
				PVector delta = PVector.sub(vaiPose.get(JIDX.TORSO), vaiPose.get(JIDX.RIGHT_FOOT));
				charSize = delta.mag();
				fflyParams.adjustToScale(charSize / 6.0f, charSize / 15.0f);
			}
			if (rhythmTaker.hasRhythm()) {
				cardio = cardiogram(rhythmTaker.getPhase());
				heartColor = Color.HSBtoRGB(353 / 360.0f, 0.95f, 0.2f + 0.8f * cardio);
			}
			heartPosition = PVector.lerp(vaiPose.get(JIDX.TORSO), vaiPose.get(JIDX.NECK), 0.575f);
			if (null == vaiBodyField) {
				vaiBodyField = new BodyField(vaiPose);
			} else {
				vaiBodyField.update(vaiPose);
			}
		}
	}
	
	public FireflyParameters activeParameters() {
		if (cloudMode) {
			return fflyCloudParams;
		} else {
			return fflyParams;
		}
	}
	
	private void operateViewpoints() {
		if (viewpointsTracker.isTracking()) {
			float spectrePitch = 1 - viewpointsTracker.getEnergy();
			//System.out.println(spectrePitch);
			fflyParams.setSpectrePitch(spectrePitch);
			fflyCloudParams.setSpectrePitch(spectrePitch);
			//System.out.println(viewpointsTracker.getSmoothness());
			//float electrization = 0.0f;
			float sparkleness = 0.0f;
			float smoothness = viewpointsTracker.getSmoothness();
			if (smoothness < 0.5f) {
				//electrization = 1.0f - 2 * smoothness;
				sparkleness = 1.0f - 2 * smoothness;
			}
			//fflyParams.setElectrization(electrization);
			fflyParams.setSparkleness(sparkleness);
			fflyCloudParams.setSparkleness(sparkleness);
			if (smoothness > 0.5f) {
				fflyParams.setAnxiety(5.0f - (smoothness - 0.5f) * 10);
			} else {
				fflyParams.setAnxiety(5.0f);
			}
		}
	}
	
	private void operateHeartIgnition() {
		if (rhythmTaker.hasRhythm()) {
			float underignition = 1.0f - heartIgnition;
			underignition *= Math.exp(-clock.deltatime());
			heartIgnition = 1.0f - underignition;
		} else {
			heartIgnition *= Math.exp(-clock.deltatime());
		}
	}
	
	private float cardiogram(float phase) {
		int periods = (int) (phase / (2 * Math.PI));
		float t = (float) (phase - periods * 2 * Math.PI - Math.PI);
		return (float) Math.exp(-Math.pow(t, 2)/TWO_CARDIO_DEV_SQR); //* sin(CARDIO_RHYTHM * t);
	}
	
	public float getCardio() {
		return cardio;
	}
	
	public float getCharSize() {
		return charSize;
	}
	
	public PVector getHeartPosition() {
		return heartPosition;
	}
	
	public float getHeartIgnition() {
		return heartIgnition;
	}
	
	public int getHeartColor() {
		return heartColor;
	}
	
	public boolean isCloudMode() {
		return cloudMode;
	}
	
	public Field getControlField() {
		if (cloudMode) {
			return cloudField;
		} else {
			return vaiBodyField;
		}
	}
}
