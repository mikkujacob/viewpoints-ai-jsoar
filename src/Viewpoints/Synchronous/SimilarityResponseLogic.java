package Viewpoints.Synchronous;

import ObjectiveParameters.ObjectiveParametersMetric;
import ObjectiveParameters.TargetKeyset;
import Viewpoints.Shared.*;

public class SimilarityResponseLogic extends ResponseLogic {
	private ObjectiveParametersMetric metric = new ObjectiveParametersMetric(TargetKeyset.getBasicSimilarityKeyset(), 
																			"gesture-segments", "gesture-segments",
																			"runtime-gesture-segments", "runtime-gesture-segments");
	private LeaderRecord currentResponse = null;
	
	public SimilarityResponseLogic() {
		metric.load();
	}
	
	@Override
	protected LeaderRecord evaluateResponse(JointSpaceGesture input, JointSpaceGesture normalizedInput) {
		JointSpaceGesture similar = metric.selectClosest(normalizedInput);
		if (metric.size() < 200) {
			metric.memorizeLastGesture();
		}
		currentResponse = new LeaderRecord(similar.getGestureFramesList());
		currentResponse.setIsRepetitionOn(true);
		currentResponse.setDirectRepetition(true);
		return currentResponse;
	}
}
