package Viewpoints.Synchronous;

import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.Shared.*;
import processing.core.*;

public class RhythmApplet extends PApplet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SimpleOpenNI kinect;
	RhythmTaker rhythmTaker;
	long timeZero = 0;
	float phase = 0;
	float lastTime = 0;
	
	public void setup() {
		size(displayWidth, displayHeight);
		kinect = new SimpleOpenNI(this);
		if (kinect.isInit()) {
			kinect.enableDepth();
			kinect.enableUser();
			timeZero = System.currentTimeMillis();
			rhythmTaker = new RhythmTaker(false);
		} else {
			kinect = null;
			background(255, 0, 0);
			noLoop();
		}
	}
	
	public void draw() {
		if (null != kinect) {
			//stroke(255);
			background(0);
			kinect.update();
			float time = (System.currentTimeMillis() - timeZero) / 1000.0f;
			float deltatime = time - lastTime;
			lastTime = time;
			int[] userList = kinect.getUsers();
			Body current = null;
			for(int i = 0; i < userList.length; i++)
			{
				if (kinect.isTrackingSkeleton(userList[i])) {
					current = new Body();
					current.initialize(kinect, userList[i], time);
					break;
				}
			}
			if (null != current) {
				rhythmTaker.takePose(current);
				if (rhythmTaker.hasRhythm()) {
					background(0, 32 + 128 * cardiogram(phase), 0);
					System.out.println(rhythmTaker.getPeriod());
					phase += 2 * PI * deltatime / rhythmTaker.getPeriod();
				}
				if (rhythmTaker.rhythmJustStarted()) {
					for (String location : rhythmTaker.getPeriodicityLocations()) {
						System.out.println(location);
					}
				}
				stroke(255);
				strokeWeight(1);
				pushMatrix();
				translate(width/2, height/2);
				for (Pair<JIDX, JIDX> connection : LIDX.LIMB_ENDS().values()) {
					PVector tipA = getScreenPos(current.get(connection.first));
					PVector tipB = getScreenPos(current.get(connection.second));
					line(tipA.x, tipA.y, tipB.x, tipB.y);
				}
				popMatrix();
				//String lhpos = String.format("LEFT_HAND.y = %.2f", current.get(JIDX.LEFT_HAND).z);
				//text(lhpos, 20, height/2);
			}
			fill(255);
			textAlign(LEFT, TOP);
			textSize(40);
			String samplestatus = rhythmTaker.getSampleStatus();
			//text(samplestatus, 20, 20);
		}
	}
	
	private static final float CARDIO_RHYTHM = 3;
	private static final float TWO_CARDIO_DEV_SQR = 8 * PI * PI / (CARDIO_RHYTHM * CARDIO_RHYTHM);
	
	private float cardiogram(float phase) {
		int periods = (int) (phase / (2 * PI));
		float t = phase - periods * 2 * PI - PI;
		return exp(-pow(t, 2)/TWO_CARDIO_DEV_SQR); //* sin(CARDIO_RHYTHM * t);
	}
	
	private PVector getScreenPos(PVector pos) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.x = - screenPos.x;
		screenPos.y = - screenPos.y;
		screenPos.z = 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	public void onNewUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.Synchronous.RhythmApplet", new String[]{"--full-screen", "--display=1"});
	}
}
