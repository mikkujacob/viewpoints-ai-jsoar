package Viewpoints.Synchronous;

import java.io.*;
import java.util.*;

import ObjectiveParameters.IO;
import ObjectiveParameters.ObjectiveParameterMonitor;
import ObjectiveParameters.ParameterKey;
import ObjectiveParameters.TargetKeyset;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.MathUtilities;

public class ClassifierViewpointsTracker implements ViewpointsTracker {
	private static final String MAC_ANACONDA_PATH = "//anaconda" + File.separator + "bin" + File.separator;
	private static final String VIEWPOINTS_EXPERT_PATH = "/emotion_expert/ViewpointsExpert.py";
	private static final float CHECK_PERIOD = 0.5f;
	private static final float HISTORY_LENGTH = 1.5f;
	private static final float CHARACTERISTIC_UPDATE_TIME = 0.5f;
	
	private ArrayList<Body> history = new ArrayList<Body>();
	private float lastCheckTime = -1.0f;
	private float energyValue = 0.5f;
	private float smoothnessValue = 0.5f;
	
	private ClassifierCommThread classifierCommThread = null;
	
	public ClassifierViewpointsTracker() {
		try {
			classifierCommThread = new ClassifierCommThread();
			classifierCommThread.start();
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}

	@Override
	public void takePose(Body pose) {
		history.add(pose);
		
		if (-1.0f == lastCheckTime) {
			lastCheckTime = pose.getTimestamp();
		}
		
		if (pose.getTimestamp() - lastCheckTime >= CHECK_PERIOD) {
			lastCheckTime = pose.getTimestamp();
			truncateHistory();
			classifierCommThread.requestClassificationOf(new ArrayList<Body>(history));
		}
		
		updateEnergy();
	}
	
	private void truncateHistory() {
		float farEndTime = lastCheckTime - HISTORY_LENGTH;
		int cutIndex = 0;
		for (; cutIndex < history.size(); cutIndex++) {
			if (history.get(cutIndex).getTimestamp() > farEndTime) break;
		}
		history = new ArrayList<Body>(history.subList(cutIndex, history.size()));
	}
	
	private void updateEnergy() {
		if (history.size() < 2) {
			//System.out.println("Energy classifier warning: short history!");
			return;
		}
		
		Body last = history.get(history.size() - 1);
		Body prelast = history.get(history.size() - 2);
		
		float deltatime = last.getTimestamp() - prelast.getTimestamp();
		
		energyValue = MathUtilities.exponentialDrift(energyValue, 
													 classifierCommThread.targetEnergy(), 
													 deltatime,
													 CHARACTERISTIC_UPDATE_TIME);
		
		smoothnessValue = MathUtilities.exponentialDrift(smoothnessValue, 
				 										 classifierCommThread.targetSmoothness(), 
				 										 deltatime,
				 										 CHARACTERISTIC_UPDATE_TIME);
		//System.out.println(energyValue);
	}

	@Override
	public void reset() {
		energyValue = 0.5f;
		classifierCommThread.reset();
	}

	@Override
	public boolean isTracking() {
		return true;
	}

	@Override
	public float getEnergy() {
		return energyValue;
	}

	@Override
	public float getSmoothness() {
		return smoothnessValue;
	}
	
	private static class ClassifierCommThread extends Thread {
		private ObjectiveParameterMonitor objectiveParametersMonitor = new ObjectiveParameterMonitor(TargetKeyset.getFullKeyset());
		
		private Process viewpointsExpert = null;
		private PrintStream viewpointsExpertInput = null;
		private Scanner viewpointsExpertOutput = null;
		//private Scanner viewpointsExpertErr = null;
		
		private ArrayList<Body> request = null;
		private float targetEnergy = 0.5f;
		private float targetSmoothness = 0.5f;
		
		private boolean isReset = false;
		
		public ClassifierCommThread() throws Exception {
			String viewpointsExpertCommand = null;
			
			String PROJECT_HOME = System.getProperty("user.dir");
			
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") == -1)  
			{
				viewpointsExpertCommand = MAC_ANACONDA_PATH + "python " + PROJECT_HOME + VIEWPOINTS_EXPERT_PATH;
			}
			else
			{
				viewpointsExpertCommand = "python " + PROJECT_HOME + VIEWPOINTS_EXPERT_PATH;
			}
			
			System.out.println(viewpointsExpertCommand);
			
			viewpointsExpert = Runtime.getRuntime().exec(viewpointsExpertCommand);
			
			viewpointsExpertInput = new PrintStream(viewpointsExpert.getOutputStream());
			viewpointsExpertOutput = new Scanner(viewpointsExpert.getInputStream());
			//viewpointsExpertErr = new Scanner(viewpointsExpert.getErrorStream());
		}

		public void run() {
			while (true) {
				synchronized(this) {
					while (null == request) {
						try {wait();} catch(InterruptedException e) {}
					}
				}
				
				List<Float> received = classifyViewpoints();
				
				synchronized(this) {
					request = null;
					if (!isReset && null != received) {
						//System.out.println("target: " + Float.toString(targetEnergy));
						targetEnergy = received.get(0);
						targetSmoothness = received.get(1);
					} else {
						targetEnergy = 0.5f;
						targetSmoothness = 0.5f;
					}
				}
			}
		}
		
		private List<Float> classifyViewpoints() {
			HashMap<ParameterKey, Float> parameters = objectiveParametersMonitor.getParameters(request);
			if (null != parameters) {
				//IO.outToStream(System.out, parameters);
				IO.outToStream(viewpointsExpertInput, parameters);
				try {
					viewpointsExpert.getOutputStream().flush();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				String response = viewpointsExpertOutput.nextLine();
				response = response.trim();
				//System.out.println(response);
				String[] responseParts = response.split(",");
				List<Float> responseValues = new ArrayList<Float>();
				for (String responsePart : responseParts) {
					responsePart = responsePart.trim();
					String[] subparts = responsePart.split(" ");
					responseValues.add(Float.valueOf(subparts[1]) / 2.0f);
				}
				return responseValues;
			} else {
				return null;
			}
		}
		
		public void requestClassificationOf(ArrayList<Body> history) {
			synchronized(this) {
				isReset = false;
				if (null != request) return;
				request = history;
				notifyAll();
			}
		}
		
		public float targetEnergy() {
			synchronized(this) {
				return targetEnergy;
			}
		}
		
		public float targetSmoothness() {
			synchronized(this) {
				return targetSmoothness;
			}
		}
		
		public void reset() {
			synchronized(this) {
				isReset = true;
				targetEnergy = 0.5f;
				targetSmoothness = 0.5f;
			}
		}
	}
}
