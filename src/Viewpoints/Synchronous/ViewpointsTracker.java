package Viewpoints.Synchronous;

import Viewpoints.Shared.Body;

public interface ViewpointsTracker {
	void takePose(Body pose);
	void reset();
	boolean isTracking();
	float getEnergy();
	float getSmoothness();
}
