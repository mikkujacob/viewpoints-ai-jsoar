package Viewpoints.Synchronous;

import Viewpoints.Shared.*;

import java.util.*;

import processing.core.PVector;

public class RhythmTaker {
	private static final float MIN_SEGMENT_DURATION = 0.5f;
	private static final float REQUIRED_IN_TRACK_DEVIATION_FACTOR = 2.0f;
	private static final float FORGETTING_TIME = 10.0f;
	private static final float JOINT_DEVIATION_TOLERANCE = 0.25f;
	private static final JIDX[] RESTRICTED_JSET = JIDX.ALL_JIDX;
	private static final float MIN_DEVIATION = 150.0f;
	private static final float MIN_DELTA = 20.0f;
	private static final float MAX_STILLNESS = 0.5f;
	private static final float PERIOD_PRECISION = 1.75f;
	private static final float PERIOD_CONSERVATISM = 0.8f;
	private static final int MIN_HALFPERIODS = 4;
	private float phase = 0.0f;
	private float period = Float.MAX_VALUE;
	private float lastTime = -1.0f;
	
	private boolean hadRhythm = false;
	private boolean havingRhythm = false;
	
	private static class RhythmTracker {
		private int movementSign = 0;
		private float period = Float.MAX_VALUE;
		private int halfperiods = 0;
		private float turnTime = 0;
		private float turnX = 0;
		private float beatTime = 0;
		private float stillnessStart = -1;
		private float prevX = Integer.MAX_VALUE;
		private float dbgdelta = 0;
		private float dbgdev = 0;
		private boolean loud = false;
		private boolean beatMoment = false;
		
		public void setLoud(boolean loud) {
			this.loud = loud;
		}
		
		private void resetCycle(float time, float x) {
			turnX = x;
			halfperiods = 0;
			beatTime = time;
			turnTime = time;
			period = Float.MAX_VALUE;
		}
		
		public void takePosition(float x, float time) {
			beatMoment = false;
			if (Integer.MAX_VALUE == prevX) {
				prevX = x;
				return;
			}
			float delta = x - prevX;
			prevX = x;
			dbgdev = x - turnX;
			dbgdelta = delta;
			if (0 != movementSign &&
				time - turnTime > PERIOD_PRECISION * period) {
				if (loud) System.out.println("Halfperiod overtime");
				resetCycle(turnTime, x);
			}
			if (Math.abs(delta) < MIN_DELTA) {
				if (stillnessStart == -1) {
					//System.out.println("Stillness start");
					stillnessStart = time;
				} else if (time - stillnessStart > MAX_STILLNESS) {
					//System.out.println("Stillness overtime");
					resetCycle(time, x);
					movementSign = 0;
					stillnessStart = time;
				}
				return;
			}
			stillnessStart = -1;
			int currentMovementSign = (int)Math.signum(delta);
			if (0 == movementSign) {
				//System.out.println("Movement start");
				movementSign = currentMovementSign;
				resetCycle(time, x);
				return;
			}
			if (currentMovementSign != movementSign) {
				if (Math.abs(x - turnX) < MIN_DEVIATION && halfperiods < MIN_HALFPERIODS) {
					if (loud) System.out.println("Underdeviation: " + Math.abs(x - turnX));
					resetCycle(time, x);
					return;
				}
				turnX = x;
				turnTime = time;
				halfperiods++;
				movementSign = currentMovementSign;
				if (halfperiods % 2 == 0) {
					if (2 == halfperiods) {
						//System.out.println("Period establised");
						period = time - beatTime;
					} else {
						if (time - beatTime < period / PERIOD_PRECISION && halfperiods < MIN_HALFPERIODS) {
							if (loud) System.out.println("Halfperiod undertime");
							resetCycle(time, x);
						} else {
							//System.out.println("Period confirmed");
							period = PERIOD_CONSERVATISM * period + (1 - PERIOD_CONSERVATISM) * (time - beatTime);
						}
					}
					beatMoment = true;
					beatTime = time;
				}
			}
		}
		
		public boolean isBeatMoment() {
			return beatMoment;
		}
		
		public float getPeriod() {
			return period;
		}

		public boolean isPeriodic() {
			return halfperiods >= MIN_HALFPERIODS;
		}
		
		public String getStatus() {
			return String.format("MovementSign: %d\nDeviation: %.2f\nHalfperiods: %d\nDelta: %.2f", movementSign, dbgdev, halfperiods, dbgdelta);
		}
	}
	
	private Map[] trackers = {new HashMap(), new HashMap(), new HashMap()};
	private ArrayDeque<Body> track = null;
	private boolean trackIsSegment = false;
	
	public RhythmTaker(boolean trackSegments) {
		for (JIDX jidx : RESTRICTED_JSET) { //JIDX.ALL_JIDX) {
			for (int i = 0; i < 3; i++) {
				trackers[i].put(jidx, new RhythmTracker());
			}
		}
		if (trackSegments) {track = new ArrayDeque<Body>();}
		//((RhythmTracker)(trackers[2].get(JIDX.LEFT_HAND))).setLoud(true);
	}
	
	public void takePose(Body body) {
		trackIsSegment = false;
		updateTrackers(body);
		checkRhythm();
		updatePhase(body.getTimestamp());
		updateTrack(body);
		lastTime = body.getTimestamp();
	}
	
	private void updateTrackers(Body body) {
		for (JIDX jidx : RESTRICTED_JSET) { //JIDX.ALL_JIDX) {
			float[] pos = body.get(jidx).array();
			for (int i = 0; i < 3; i++) {
				((RhythmTracker)trackers[i].get(jidx)).takePosition(pos[i], body.getTimestamp());
			}
		}
	}
	
	private void checkRhythm() {
		hadRhythm = havingRhythm;
		havingRhythm = false;
		period = Float.MAX_VALUE;
		
		for (int i = 0; i < 3; i++) {
			Map<JIDX, RhythmTracker> dimensionTrackers = (Map<JIDX, RhythmTracker>)trackers[i];
			for (RhythmTracker tracker : dimensionTrackers.values()) {
				if (tracker.isPeriodic()) {
					havingRhythm = true;
					float localPeriod = tracker.getPeriod();
					if (localPeriod < period) {
						period = localPeriod;
					}
				}
			}
		}
	}
	
	private void updatePhase(float time) {
		if (lastTime > 0 && havingRhythm) {
			float deltatime = time - lastTime;
			phase += 2 * Math.PI * deltatime / period;
		}
	}
	
	private void updateTrack(Body body) {
		if (null == track)
			return;
		
		track.add(body);
		
		while (body.getTimestamp() - track.getFirst().getTimestamp() > FORGETTING_TIME) {
			track.poll();
		}
		
		//if (checkFluctuation()) {
			//System.out.println("fluke");
		//	return;
		//}
		
		ArrayList<Float> activeBeats = new ArrayList<Float>();
		collectActiveBeats(activeBeats);
		Collections.sort(activeBeats);
		
		for (Float activePeriod: activeBeats) {
			Body historyBody = regress(activePeriod);
			if (null == historyBody) continue;
			if (!body.isSimilar(historyBody, JOINT_DEVIATION_TOLERANCE)) continue;
			if (!sufficientDeviationWithinTrack(body, historyBody)) continue;
			cutTrackToSegment(historyBody);
			break;
		}
	}
	
	private boolean checkFluctuation() {
		if (track.size() < 2)
			return false;
		Body endPose = track.pollLast();
		Body precedingPose = track.getLast();
		track.add(endPose);
		float charsize = endPose.characteristicSize();
		ArrayList<Body> todiff = new ArrayList<Body>(); todiff.add(precedingPose); todiff.add(endPose);
		ArrayList<BodyDerivative> diff = BodyDerivative.derivative(todiff);
		BodyDerivative thediff = diff.get(0);
		for (JIDX jidx : JIDX.ALL_JIDX) {
			PVector d = thediff.get(jidx);
			if (d.mag() > 8 * charsize) {
				System.out.println("fluke");
				track.clear();
				track.add(endPose);
				return true;
			}
		}
		return false;
	} 
	
	private void collectActiveBeats(ArrayList<Float> oActiveBeats) {
		for (int i = 0; i < 3; i++) {
			Map<JIDX, RhythmTracker> dimensionTrackers = (Map<JIDX, RhythmTracker>)trackers[i];
			for (RhythmTracker rhythmTracker : dimensionTrackers.values()) {
				if (rhythmTracker.isBeatMoment()) {
					if (rhythmTracker.getPeriod() > MIN_SEGMENT_DURATION) {
						oActiveBeats.add(rhythmTracker.getPeriod());
					}
				}
			}
		}
	}
	
	private Body regress(float deltaTime) {
		if (track.isEmpty()) return null;
		float targetTime = track.getLast().getTimestamp() - deltaTime;
		Iterator<Body> historyIterator = track.descendingIterator();
		Body up = null;
		Body dn = null;
		while (historyIterator.hasNext()) {
			up = dn;
			dn = historyIterator.next();
			if (dn.getTimestamp() < targetTime)
				break;
		}
		if (null == dn) return null;
		if (null == up) return dn;
		if (targetTime - dn.getTimestamp() < up.getTimestamp() - targetTime) {
			return dn;
		} else {
			return up;
		}
	}
	
	private float deviationValue(Body poseA, Body poseB) {
		PVector centerA = poseA.get(JIDX.TORSO);
		PVector centerB = poseB.get(JIDX.TORSO);
		float deviationValue = 0;
		for (JIDX jidx : JIDX.ALL_JIDX) {
			if (JIDX.TORSO == jidx) continue;
			PVector radialA = PVector.sub(poseA.get(jidx), centerA);
			PVector radialB = PVector.sub(poseB.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float deviationMag = deviation.mag();
			if (deviationMag > deviationValue) deviationValue = deviationMag;
		}
		return deviationValue;
	}
	
	private boolean sufficientDeviationWithinTrack(Body recentEnd, Body oldEnd) {
		float endDifference = deviationValue(recentEnd, oldEnd);
		boolean inSegment = false;
		for (Body pose : track) {
			if (pose == oldEnd) {
				inSegment = true;
			}
			if (inSegment) {
				float inSegmentDeviation = deviationValue(pose, oldEnd);
				if (inSegmentDeviation > REQUIRED_IN_TRACK_DEVIATION_FACTOR * endDifference) return true;
			}
		}
		return false;
	}
	
	private void cutTrackToSegment(Body segmentStart) {
		trackIsSegment = true;
		while (segmentStart != track.getFirst()) {
			track.poll();
		}
	}
	
	public boolean hasRhythm() {
		return havingRhythm;
	}
	
	public boolean rhythmJustStarted() {
		return !hadRhythm && havingRhythm;
	}
	
	public boolean rhythmJustEnded() {
		return !havingRhythm && hadRhythm;
	}
	
	public ArrayList<String> getPeriodicityLocations() {
		ArrayList<String> locations = new ArrayList<String>();
		for (int i = 0; i < 3; i++) {
			Map<JIDX, RhythmTracker> dimensionTrackers = (Map<JIDX, RhythmTracker>)trackers[i];
			for (Map.Entry<JIDX, RhythmTracker> entry : dimensionTrackers.entrySet()) {
				if (entry.getValue().isPeriodic()) {
					locations.add(entry.getKey().toString() + " " + Integer.toString(i));
				}
			}
		}
		return locations;
	}
	
	public float getPeriod() {
		return period;
	}
	
	public float getPhase() {
		return phase;
	}
	
	public String getSampleStatus() {
		RhythmTracker sampleTracker = (RhythmTracker)(trackers[2].get(JIDX.LEFT_HAND));
		return sampleTracker.getStatus();
	}
	
	public ArrayList<Body> motionSegment() {
		if(!trackIsSegment)
			return null;
		ArrayList<Body> segment = new ArrayList<Body>();
		segment.addAll(track);
		return segment;
	}
}
