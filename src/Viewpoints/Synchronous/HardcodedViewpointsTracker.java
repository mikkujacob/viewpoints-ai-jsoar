package Viewpoints.Synchronous;

import java.util.*;

import Viewpoints.Shared.*;
import Viewpoints.Gesture.*;

public class HardcodedViewpointsTracker implements ViewpointsTracker {
	private static final float DRIFT_CHARACTERISTIC_TIME = 1.0f;
	private Map<String, ViewpointPredicate> trackedPredicates = new HashMap<String, ViewpointPredicate>();
	private Map<String, Float> trackedValues = new HashMap<String, Float>();
	private ArrayList<Body> history = new ArrayList<Body>();
	
	public HardcodedViewpointsTracker() {
		trackedPredicates.put("e", new EnergyPredicate(0.0f));
		trackedPredicates.put("s", new SmoothnessPredicate(0.0f));
	}
	
	public void takePose(Body pose) {
		history.add(pose);
		int hsize = history.size();
		if (3 == hsize) {
			float deltatime = history.get(hsize - 1).getTimestamp() - history.get(hsize - 2).getTimestamp();
			if (deltatime > 2.0f) {
				System.out.println("catch!");
			}
			for (Map.Entry<String, ViewpointPredicate> trackedEntry : trackedPredicates.entrySet()) {
				String trackedCode = trackedEntry.getKey();
				ViewpointPredicate trackedPredicate = trackedEntry.getValue();
				float predValue = trackedPredicate.calcPredNumber(history);
				updatePredicateValue(trackedCode, predValue, deltatime);
			}
			history.remove(0);
		}
	}
	
	public void reset() {
		history.clear();
		trackedValues.clear();
	}
	
	private void updatePredicateValue(String predCode, float predValue, float deltatime) {
		if (!trackedValues.containsKey(predCode)) {
			trackedValues.put(predCode, predValue);
		} else {
			float historyValue = trackedValues.get(predCode);
			float d = predValue - historyValue;
			d *= Math.pow(2.0f, -deltatime / DRIFT_CHARACTERISTIC_TIME);
			float updatedValue = predValue - d;
			//System.out.println(Float.toString(predValue) + " - "+ Float.toString(historyValue) + " - " +Float.toString(updatedValue) + " - " + Float.toString(deltatime));
			trackedValues.put(predCode, updatedValue);
		}
	}
	
	public boolean isTracking() {
		return !trackedValues.isEmpty();
	}
	
	public float getEnergy() {
		return trackedValues.get("e");
	}
	
	public float getSmoothness() {
		return trackedValues.get("s");
	}
}
