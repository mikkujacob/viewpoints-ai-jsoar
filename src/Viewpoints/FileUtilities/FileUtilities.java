package Viewpoints.FileUtilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Viewpoints.Gesture.Gesture;
import Viewpoints.Shared.JointSpaceGesture;

/**
 * Utility class meant for methods related to writing gestures to files
 * @author mjacob6
 *
 */
public class FileUtilities {
	
	/**
	 * 
	 * @param path
	 * @param outputGesture
	 */
	public static void writeToText(String path, Gesture outputGesture) {
		
		FileWriter fstream = null;
		BufferedWriter out = null;
		
		try {
			fstream = new FileWriter(path, false);
			out = new BufferedWriter(fstream);
			
//			System.out.println("Writing: " + outputGesture.toString());
			
			out.write(outputGesture.toString());
			
			System.out.println("Done Writing " + path);
		} catch (Exception e) {
			System.err.println("failed to output Gesture: " + e.getMessage());
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	public static Gesture readFromText(String path){
		
		FileReader fstream = null;
		BufferedReader in = null;
		String inputGesture = "";
		try {
			fstream = new FileReader(path);
			in = new BufferedReader(fstream);
			String line = "";
			
			while((line = in.readLine()) != null)
			{
				inputGesture += line + "\n";
			}
			
			//System.out.println("Done Reading: " + inputGesture);
		} catch (Exception e) {
			System.err.println("failed to output Gesture: " + e.getMessage());
		}
		finally {
			try {
				if (null != in)
					in.close();
				
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		
		//System.out.println(Gesture.fromString(inputGesture));
		return Gesture.fromString(inputGesture);
	}
	
	/**
	 * Serialize Predicate Space Gesture
	 * @param path
	 * @param outputGesture
	 * @return
	 */
	public static Boolean serializeGesture(String path, Gesture outputGesture)
	{
		try {
			FileOutputStream fileoutstream = new FileOutputStream(path);
			ObjectOutputStream objectoutstream = new ObjectOutputStream(fileoutstream);
			objectoutstream.writeObject(outputGesture);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		System.out.println("Done Writing Gesture" + path);
		
		return true;
	}
	
	/**
	 * Deserialize Space Gesture
	 * @param path
	 * @return
	 */
	public static Gesture deserializeGesture(String path)
	{
		Gesture readGesture = new Gesture();
		
		try
		{
			File infile = new File(path);
			FileInputStream fileinstream = new FileInputStream(infile);
			
			if(fileinstream.available() == 0)
			{
				fileinstream.close();
				return null;
			}
			
			ObjectInputStream objectinstream = new ObjectInputStream(fileinstream);
			readGesture = (Gesture) objectinstream.readObject();
			objectinstream.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} 
//		System.out.println("Done Reading");
		
		return readGesture;
	}
	
	/**
	 * Clear a text file.
	 * @param path
	 */
	public static void clearText(String path){
		
		FileWriter fstream = null;
		BufferedWriter out = null;
		
		try {
			fstream = new FileWriter(path, false);
			out = new BufferedWriter(fstream);
			
//			System.out.println("Clearing");
			
			out.write("");
			
			System.out.println("Done Clearing " + path);
		} catch (Exception e) {
			System.err.println("failed to clear text: " + e.getMessage());
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Serialize Joint Space Gesture
	 * @param path
	 * @param outputGestureJoints
	 * @return
	 */
	public static Boolean serializeJointsGesture(String path, JointSpaceGesture outputGestureJoints)
	{
		try {
			FileOutputStream fileoutstream = new FileOutputStream(path);
			ObjectOutputStream objectoutstream = new ObjectOutputStream(fileoutstream);
			objectoutstream.writeObject(outputGestureJoints);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		}
		catch (IOException e)
		{
			return false;
		}

		System.out.println("Done Writing " + path);
		
		return true;
	}
	
	/**
	 * Deserialize Joint Space Gesture
	 * @param path
	 * @return
	 */
	public static JointSpaceGesture deserializeJointsGesture(String path)
	{
		JointSpaceGesture readGesture = new JointSpaceGesture();
		FileInputStream fileinstream = null;
		ObjectInputStream objectinstream = null;
		try
		{
			File infile = new File(path);
			fileinstream = new FileInputStream(infile);
			
			if(fileinstream.available() == 0) {
				fileinstream.close();
				return null;
			}


			objectinstream = new ObjectInputStream(fileinstream);
			readGesture = (JointSpaceGesture) objectinstream.readObject();
			objectinstream.close();
		}
		catch (IOException|ClassNotFoundException e) {return null;}
		finally {
			try {
				if (null != objectinstream) objectinstream.close();
				if (null != fileinstream) fileinstream.close();
			} catch (Exception e) {return null;}
		}

//		System.out.println("Done Reading");
		readGesture.autotiming();
		return readGesture;
	}
	
	/**
	 * Write a text message to a file. Overwrite existing text in file.
	 * @param path Path to file.
	 * @param text Text message to write to file.
	 */
	public static void writeToFile(String path, String text){
		writeToFile (path, text, false);
	}
	
	/**
	 * Write a text message to a file.
	 * @param path Path to file.
	 * @param text Text message to write to file.
	 * @param append Boolean to enable appending current text message to existing text in file.
	 */
	public static void writeToFile(String path, String text, boolean append){
		File pathFile = new File(path);
		FileWriter fstream = null;
		BufferedWriter out = null;
		try {
			pathFile.createNewFile();
			fstream = new FileWriter(path, append);
			
			out = new BufferedWriter(fstream);
			
			out.write(text);
			
//			System.out.println("Done Writing " + path);
		} catch (Exception e) {
			System.err.println("Failed to write text: " + e.getMessage());
			System.err.println("Path: " + path);
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}
}
