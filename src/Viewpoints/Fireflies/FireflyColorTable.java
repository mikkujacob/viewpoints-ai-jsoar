package Viewpoints.Fireflies;

import java.util.ArrayList;

import Viewpoints.Shared.Pair;
import processing.core.PApplet;
import processing.core.PGraphics;

public class FireflyColorTable {
	private ArrayList<Integer> lastScale= null;
	private ArrayList<ArrayList<Integer>> colors = new ArrayList<ArrayList<Integer>>();
	private ArrayList<Float> intensities = new ArrayList<Float>();
	private ArrayList<Float> spectre = new ArrayList<Float>();
	
	public void addIntensityLevel(float intensity) {
		intensities.add(intensity);
	}
	
	public void addSpectreLevel(float spectreLevel) {
		spectre.add(spectreLevel);
		lastScale = new ArrayList<Integer>();
		colors.add(lastScale);
	}
	
	public void addSample(int color) {
		lastScale.add(color);
	}
	
	public int getColor(float pitch, float intensity, PGraphics pGraphics) {
		Pair<Integer, Float> lookup = lookOnRange(spectre, pitch);
		int i = lookup.first;
		pitch = lookup.second;
		float intervalPosition = (pitch - spectre.get(i-1))/(spectre.get(i) - spectre.get(i-1));
		int colorLow  = getColorOnScale(colors.get(i-1), intensity, pGraphics);
		int colorHigh = getColorOnScale(colors.get(i),   intensity, pGraphics);
		return pGraphics.lerpColor(colorLow, colorHigh, intervalPosition);
	}
	
	private int getColorOnScale(ArrayList<Integer> scale, float intensity, PGraphics pGraphics) {
		Pair<Integer, Float> lookup = lookOnRange(intensities, intensity);
		int i = lookup.first;
		intensity = lookup.second;
		float intervalPosition = (intensity - intensities.get(i-1))/(intensities.get(i) - intensities.get(i-1));
		return pGraphics.lerpColor(scale.get(i-1), scale.get(i), intervalPosition);
	}
	
	private Pair<Integer, Float> lookOnRange(ArrayList<Float> range, float position) {
		int i = 0;

		while (++i < range.size() - 1) {
			if (position < range.get(i)) 
				break;
		}
		
		float top = range.get(range.size()-1);
		
		if (position > top) {
			position = top;
		}
		
		return Pair.of(i, position);
	}
	
	public static FireflyColorTable Standard(PApplet pApplet) {
		FireflyColorTable colorTable = new FireflyColorTable();
		colorTable.addIntensityLevel(0.0f);
		colorTable.addIntensityLevel(0.4f);
		colorTable.addIntensityLevel(1.0f);
		colorTable.addIntensityLevel(2.0f);
		colorTable.addSpectreLevel(0.0f);
		colorTable.addSample(pApplet.color(64, 0, 1, 255));
		colorTable.addSample(pApplet.color(153, 13, 1, 255));
		colorTable.addSample(pApplet.color(254, 53, 1, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.5f);
		colorTable.addSample(pApplet.color(227, 97, 11, 100));
		colorTable.addSample(pApplet.color(227, 97, 11, 128));
		colorTable.addSample(pApplet.color(251, 180, 40, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.75f);
		colorTable.addSample(pApplet.color(0,   15,  30,   255));
		colorTable.addSample(pApplet.color(15,  118, 140, 255));
		colorTable.addSample(pApplet.color(105, 237, 242, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(1.0f);
		colorTable.addSample(pApplet.color(0, 2, 50, 255));
		colorTable.addSample(pApplet.color(1, 60, 130, 255));
		colorTable.addSample(pApplet.color(156, 235, 255, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		return colorTable;
	}
	
	public static FireflyColorTable StandardEmotion(PApplet pApplet) {
		FireflyColorTable colorTable = new FireflyColorTable();
		colorTable.addIntensityLevel(0.0f);
		colorTable.addIntensityLevel(0.2f);
		colorTable.addIntensityLevel(0.5f);
		colorTable.addIntensityLevel(0.8f);
		colorTable.addIntensityLevel(1.1f);
		colorTable.addIntensityLevel(2.0f);
		colorTable.addSpectreLevel(0.0f);
		colorTable.addSample(pApplet.color(98, 31, 30, 255));
		colorTable.addSample(pApplet.color(181, 38, 47, 255));
		colorTable.addSample(pApplet.color(214, 41, 49, 255));
		colorTable.addSample(pApplet.color(222, 82, 89, 255));
		colorTable.addSample(pApplet.color(236, 160, 160, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.125f);
		colorTable.addSample(pApplet.color(67, 29, 65, 255));
		colorTable.addSample(pApplet.color(119, 46, 119, 255));
		colorTable.addSample(pApplet.color(142, 60, 131, 255));
		colorTable.addSample(pApplet.color(157, 81, 142, 255));
		colorTable.addSample(pApplet.color(202, 150, 183, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.25f);
		colorTable.addSample(pApplet.color(20, 22, 51, 255));
		colorTable.addSample(pApplet.color(40, 45, 96, 255));
		colorTable.addSample(pApplet.color(39, 52, 121, 255));
		colorTable.addSample(pApplet.color(68, 82, 142, 255));
		colorTable.addSample(pApplet.color(146, 148, 190, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.375f);
		colorTable.addSample(pApplet.color(23, 51, 72, 255));
		colorTable.addSample(pApplet.color(33, 91, 132, 255));
		colorTable.addSample(pApplet.color(35, 117, 165, 255));
		colorTable.addSample(pApplet.color(76, 155, 192, 255));
		colorTable.addSample(pApplet.color(156, 201, 220, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.5f);
		colorTable.addSample(pApplet.color(35, 73, 41, 255));
		colorTable.addSample(pApplet.color(34, 129, 68, 255));
		colorTable.addSample(pApplet.color(35, 162, 75, 255));
		colorTable.addSample(pApplet.color(95, 177, 88, 255));
		colorTable.addSample(pApplet.color(162, 205, 154, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.625f);
		colorTable.addSample(pApplet.color(86, 103, 51, 255));
		colorTable.addSample(pApplet.color(147, 174, 69, 255));
		colorTable.addSample(pApplet.color(186, 209, 69, 255));
		colorTable.addSample(pApplet.color(206, 218, 99, 255));
		colorTable.addSample(pApplet.color(231, 235, 165, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.75f);
		colorTable.addSample(pApplet.color(111, 90, 46, 255));
		colorTable.addSample(pApplet.color(197, 156, 62, 255));
		colorTable.addSample(pApplet.color(246, 195, 57, 255));
		colorTable.addSample(pApplet.color(250, 213, 102, 255));
		colorTable.addSample(pApplet.color(252, 235, 169, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(0.875f);
		colorTable.addSample(pApplet.color(104, 55, 35, 255));
		colorTable.addSample(pApplet.color(184, 95, 52, 255));
		colorTable.addSample(pApplet.color(227, 112, 53, 255));
		colorTable.addSample(pApplet.color(237, 158, 94, 255));
		colorTable.addSample(pApplet.color(246, 207, 169, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		colorTable.addSpectreLevel(1.0f);
		colorTable.addSample(pApplet.color(98, 31, 30, 255));
		colorTable.addSample(pApplet.color(181, 38, 47, 255));
		colorTable.addSample(pApplet.color(214, 41, 49, 255));
		colorTable.addSample(pApplet.color(222, 82, 89, 255));
		colorTable.addSample(pApplet.color(236, 160, 160, 255));
		colorTable.addSample(pApplet.color(255, 255, 255, 255));
		return colorTable;
	}
}
