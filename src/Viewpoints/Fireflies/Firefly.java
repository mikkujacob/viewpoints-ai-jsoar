package Viewpoints.Fireflies;

import java.util.Random;

import processing.core.*;

public class Firefly {
	public PVector position;
	public PVector lastPosition;
	private PVector heading;
	private float pitchSpd = 0.0f;
	private float deviationSpd = 0.0f;
	private Random random;
	public float intencity = 0.0f;

	public Firefly(PVector iniPosition, PVector iniHeading, Random random) {
		position = iniPosition;
		lastPosition = position;
		heading = iniHeading;
		this.random = random;
	}

	public void fly(Field controlField, FireflyParameters params, float deltaTime) {
		pitchSpd = deviate(pitchSpd, params.PITCH_FACTOR, params.PITCH_CUTOFF, deltaTime);
		deviationSpd = deviate(deviationSpd, params.DEVIATION_FACTOR,
				params.DEVIATION_CUTOFF, deltaTime);
		FieldSample sample = controlField.sample(position);
		updatePosition(sample, params, deltaTime);
		updateHeading(sample, params, deltaTime);
		updateIntencity(sample, params, deltaTime);
	}
	
	private void updateIntencity(FieldSample sample, FireflyParameters params, float deltaTime) {
		float disturbanceSpd = 0.0f;
		if (deltaTime > 0.0f) {
			disturbanceSpd = sample.disturbance.mag()/deltaTime;
		}
		float ignition = 0.05f * disturbanceSpd / params.TURN_RADIUS;
		
		if (ignition > 1.0f)
			ignition = 1.0f;
		
		ignition = PApplet.pow(ignition, 5);
		
		float nintencity = 0.9f*sample.power; //intencityFluctuation;
		nintencity = (1.0f - ignition)*nintencity + (1.0f + params.SPARKLENESS)*ignition;
		intencity = intencity + params.ELECTRIZATION*(nintencity - intencity);
	}

	private void updatePosition(FieldSample sample, FireflyParameters params, float deltaTime) {
		lastPosition = position;
		float speed = params.FLIGHT_SPEED * (1.0f + params.ANXIETY * (1.0f - sample.power));
		position = PVector.add(position, PVector.mult(heading, speed * deltaTime));
		position.add(PVector.mult(sample.disturbance, params.LAG_FACTOR * sample.power));
		PVector delta = PVector.sub(position, lastPosition);
	}

	private float deviate(float var, float factor, float cutoff, float deltaTime) {
		float randomFloat = random.nextFloat();
		var += 2 * factor * (randomFloat - 0.5) * deltaTime;

		if (var > cutoff)
			var = cutoff;

		if (var < -cutoff)
			var = -cutoff;

		return var;
	}

	private void updateHeading(FieldSample sample, FireflyParameters params, float deltaTime) {
		PVector dir = sample.dir;
		float turnImpetus = 1.0f - dir.dot(heading);
		float turn = ((1.0f + params.ANXIETY * (1.0f - sample.power)) * params.ANGSPEED_FACTOR * turnImpetus + sample.power * deviationSpd) * deltaTime;
		PVector axis = heading.cross(dir);
		PVector ort = axis.cross(heading);
		float pitch = sample.power * pitchSpd * deltaTime;
		heading.mult(1.0f - turn);
		ort.mult(turn);
		heading.add(ort);
		heading.mult(1.0f - pitch);
		axis.mult(pitch);
		heading.add(axis);
		heading.normalize();
	}
	
	public void teleport(PVector iPosition) {
		position = iPosition;
		lastPosition = iPosition;
	}
	
	public void printPosition() {
		System.out.println(position);
	}
}
