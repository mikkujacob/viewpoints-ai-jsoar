package Viewpoints.Fireflies;

import java.util.Random;

import Viewpoints.Shared.PVecUtilities;
import processing.core.*;

public class CentralField implements Field {
	private PVector center;

	public CentralField(PVector iCenter) {
		center = iCenter;
	}
	
	public void setCenter(PVector iCenter) {
		center = iCenter;
	}

	public FieldSample sample(PVector pos) {
		PVector gradient = PVector.sub(center, pos);
		gradient.normalize();
		return new FieldSample(gradient, PVecUtilities.ZERO, 1.0f);
	}

	@Override
	public PVector teleportEndpoint(Random random) {
		return center;
	}
}
