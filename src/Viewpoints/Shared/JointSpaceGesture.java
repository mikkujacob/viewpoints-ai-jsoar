package Viewpoints.Shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;



public class JointSpaceGesture implements Serializable
{
	/**
	 * Do not change unless you want to intentionally break serializability
	 * with previous versions of this class
	 */
	private static final long serialVersionUID = -3514812047322184597L;
	public static final float ASSUMED_DELTA_TIME = 1 / 24.0f;
	
	private UUID gestureUUID;
	private Boolean isUUIDSet = false;
	
	LinkedHashMap<Integer, Body> gesture;
	
	public JointSpaceGesture()
	{
		gesture = new LinkedHashMap<Integer, Body>();
	}
	
	@SuppressWarnings("unchecked")
	public JointSpaceGesture(LinkedHashMap<Integer, Body> inputGesture)
	{
		gesture  = new LinkedHashMap<Integer, Body>
			((LinkedHashMap<Integer, Body>)inputGesture.clone());
	}
	
	@SuppressWarnings("unchecked")
	public JointSpaceGesture(JointSpaceGesture inputGesture)
	{
		gesture  = new LinkedHashMap<Integer, Body>((LinkedHashMap<Integer, Body>)inputGesture.getGesture().clone());
	}
	
	public JointSpaceGesture(ArrayList<Body> inputGestureList)
	{
		gesture = new LinkedHashMap<Integer, Body>();
		Body jointFrame = null;
		for(int index = 0; index < inputGestureList.size(); index++)
		{
			jointFrame = inputGestureList.get(index);
			gesture.put(index, jointFrame);
		}
	}

	public LinkedHashMap<Integer, Body> getGesture() {
		return gesture;
	}
	
	public void autotiming() {
		if (gesture.size() < 2)
			return;
		Body first = gesture.get(0);
		Body last = gesture.get(gesture.size() - 1);
		if (first.getTimestamp() == last.getTimestamp()) {
			for (int i = 0; i < gesture.size(); i++) {
				Body body = gesture.get(i);
				body.setTimestamp(i*ASSUMED_DELTA_TIME);
			}
		}
	}

	public ArrayList<Body> getGestureFramesList() {
		ArrayList<Body> frames = new ArrayList<Body>();
		
		for(int index = 0; index < gesture.size(); index++)
		{
			frames.add(gesture.get(index));
		}
		
		return frames;
	}

	public void setGesture(LinkedHashMap<Integer, Body> gesture) {
		this.gesture = gesture;
	}

	public UUID getGestureUUID() {
		return gestureUUID;
	}

	public void setGestureUUID(UUID gestureUUID) {
		this.gestureUUID = gestureUUID;
		isUUIDSet = true;
	}
	
	@Override
	public String toString()
	{
		String outString = "[";
		
		Body jointFrame = null;
		for(int index = 0; index < gesture.size(); index++)
		{
			jointFrame = gesture.get(index);
			outString += jointFrame.toString();
			
			if(index < gesture.size() - 1)
			{
				outString += ", ";
			}
		}
		
		return outString + "]";
	}
}
