package Viewpoints.Shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.io.Serializable;

import SimpleOpenNI.SimpleOpenNI;
import processing.core.*;

public class Body extends SkeletalData implements Serializable, Cloneable
{
	/**
	 * Serial Version UID - DO NOT CHANGE PLEASE!!!
	 */
	private static final long serialVersionUID = 2610605569934643630L;

	public void set(JIDX iJidx, PVector iPvector) {
		put(iJidx, iPvector);
	}
	
	public void initialize(SimpleOpenNI context, int userId, float timestamp) {
		setTimestamp(timestamp);
		
		PVector jointPos = new PVector();
		
		// note: jointPos.get() is copying each PVector as they are call-by-reference

		//Get Skeleton Coordinates
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_HEAD,jointPos);
		set(JIDX.HEAD,jointPos.get());
		//PApplet.print(joints.get(JIDX.HEAD)+"\n");
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
		set(JIDX.NECK,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos); //Never used this before
		//set(JIDX.LEFT_COLLAR,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_SHOULDER,jointPos);
		set(JIDX.LEFT_SHOULDER,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_ELBOW,jointPos);
		set(JIDX.LEFT_ELBOW,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HAND,jointPos);
		set(JIDX.LEFT_HAND,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FINGERTIP,jointPos);  // always (0.0, 0.0, 0.0)
		set(JIDX.LEFT_FINGERTIP,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos); //Never used this before
		//set(JIDX.RIGHT_COLLAR,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_SHOULDER,jointPos);
		set(JIDX.RIGHT_SHOULDER,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_ELBOW,jointPos);
		set(JIDX.RIGHT_ELBOW,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HAND,jointPos);
		set(JIDX.RIGHT_HAND,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FINGERTIP,jointPos);  // always (0.0, 0.0, 0.0)
		set(JIDX.RIGHT_FINGERTIP,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointPos);
		set(JIDX.TORSO,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointPos); // always (0.0, 0.0, 0.0)
		//set(JIDX.WAIST,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HIP,jointPos);
		set(JIDX.LEFT_HIP,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_KNEE,jointPos);
		set(JIDX.LEFT_KNEE,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointPos); // always (0.0, 0.0, 0.0)
		//set(JIDX.LEFT_ANKLE,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointPos);
		set(JIDX.LEFT_FOOT,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HIP,jointPos);
		set(JIDX.RIGHT_HIP,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_KNEE,jointPos);
		set(JIDX.RIGHT_KNEE,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointPos); // always (0.0, 0.0, 0.0)
		//set(JIDX.RIGHT_ANKLE,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointPos);
		set(JIDX.RIGHT_FOOT,jointPos.get());
	}
	
	public PVector[] localBasis() {
		PVector localFwddir = PVecUtilities.ortonormalization(orientation(), PVecUtilities.UPVEC);
		PVector localSidedir = PVecUtilities.UPVEC.cross(localFwddir);
		PVector[] localBasis = {localSidedir, PVecUtilities.UPVEC.get(), localFwddir};
		return localBasis;
	}
	
	public PVector center() {
		return get(JIDX.TORSO);
	}
	
	public Body transformed(PVector[] sourceBasis, 
						   	PVector[] targetBasis,
						   	PVector targetCenter) {
		Body transformedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.ALL_JIDX) {
			if (JIDX.TORSO == jidx) {
				transformedPose.set(JIDX.TORSO, targetCenter.get());
			} else {
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				float[] dissolution = PVecUtilities.dissolve(radiusVector, sourceBasis);
				PVector normalizedPosition = PVecUtilities.assemble(targetBasis, dissolution);
				transformedPose.set(jidx, PVector.add(targetCenter, normalizedPosition));
			}
		}
		transformedPose.setTimestamp(getTimestamp());
		return transformedPose;
	}
	
	public PVector orientation()
	{
		return PVecUtilities.normTo3Pt(get(JIDX.TORSO), get(JIDX.LEFT_SHOULDER), get(JIDX.RIGHT_SHOULDER));
	}
	
	public PVector upperUpvec()
	{
		PVector upvec = PVector.sub(get(JIDX.NECK), get(JIDX.TORSO));
		upvec.div(upvec.mag());
		return upvec;
	}
	
	public PVector lowerUpvec() 
	{
		return PVecUtilities.UPVEC;
	}
	
	@Override
	public Object clone()
	{
		Body cln = new Body();
		
		cln.setTimestamp(getTimestamp());
		
		for (JIDX jidx : keySet()) {
			cln.set(jidx, PVecUtilities.clone(get(jidx)));
		}
		
		return cln;
	}
	
	// interpolates a position of the body from the positions and their coefficients given in parameter
	public static Body interpolate(Body body1, Body body2, float coefficient) {
		
		// position of the body to be returned
		Body body = new Body();
		
		// to store the part of the body which position is being computed
		JIDX iJidx;
		
		// browses all parts of the body
		for (Map.Entry<JIDX, PVector> joints_iterator : body1.entrySet()) {
			
			// part of the body to interpolate
			iJidx = joints_iterator.getKey();
			
			// interpolates the position of this part of the body and stores it
			body.set(iJidx,
					PVector.lerp(joints_iterator.getValue(), body2.get(iJidx), coefficient));
			
		}
		
		// the interpolated position of the body is returned
		return body;
		
	}
	
	public boolean isSimilar(Body other, float relativeDeviationTolerance) {
		PVector centerA = get(JIDX.TORSO);
		PVector centerB = other.get(JIDX.TORSO);
		for (JIDX jidx : JIDX.ALL_JIDX) {
			if (JIDX.TORSO == jidx) continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float referenceMag = Math.max(radialA.mag(), radialB.mag());
			float deviationMag = deviation.mag();
			if (deviationMag > referenceMag * relativeDeviationTolerance) {
				return false;
			}
		}
		return true;
	}
	
	public float characteristicSize() {
		float sumsize = 0.0f;
		
		for (Pair<JIDX, JIDX> jidxpair : LIDX.LIMB_ENDS().values()) {
			sumsize += PVector.sub(get(jidxpair.second), get(jidxpair.first)).mag();
		}
		
		return sumsize / LIDX.LIMB_ENDS().size();
	}
	
	public Body scale(float factor) {
		Body scaled = new Body();
		
		for (JIDX jidx : JIDX.ALL_JIDX) {
			scaled.set(jidx, PVector.mult(get(jidx), factor));
		}
		
		scaled.setTimestamp(getTimestamp());
		return scaled;
	}
}
