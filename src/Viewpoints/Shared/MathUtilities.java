package Viewpoints.Shared;

public class MathUtilities {
	public static float exponentialDrift(float oldValue, float targetValue, float deltatime, float characteristicTime) {
		float delta = targetValue - oldValue;
		delta *= Math.pow(2.0, -deltatime / characteristicTime);
		return targetValue - delta;
	}
}
