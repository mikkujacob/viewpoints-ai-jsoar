package Viewpoints.Visualisation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Random;

import com.sun.corba.se.spi.ior.ObjectId;

import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Fireflies.BodyField;
import Viewpoints.Fireflies.CentralField;
import Viewpoints.Fireflies.Field;
import Viewpoints.Fireflies.Firefly;
import Viewpoints.Fireflies.FireflyColorTable;
import Viewpoints.Fireflies.FireflyParameters;
import Viewpoints.Gesture.FramePredicates;
import Viewpoints.Gesture.Gesture;
import Viewpoints.Gesture.Predicates.EMOTION;
import Viewpoints.Gesture.TransformPredicates;
import Viewpoints.Gesture.Predicates.ENERGY;
import Viewpoints.Gesture.Predicates.SMOOTHNESS;
import Viewpoints.Gesture.Predicates.TEMPO;
import Viewpoints.Gesture.TransformPredicates.ADD_REPEAT;
import Viewpoints.Gesture.TransformPredicates.TRANSFORM_DURATION;
import Viewpoints.Gesture.TransformPredicates.TRANSFORM_TEMPO;
import Viewpoints.Gesture.TransformPredicates.TransformPredicateInterface;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.JointSpaceGesture;
import Viewpoints.Shared.LeaderRecord;
import Viewpoints.Shared.PVecUtilities;
import polymonkey.time.Time;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public class VAIGraphics extends BufferedGraphics {
	public static final String jsoarPredicatesInPath = "file_communications/JSOARPredicatesToVisualization.txt";
	public static final String jsoarJointsInPath = "file_communications/JSOARJointsToVisualization.txt";
	public final String GREETINGS_DIR = "greetings";
	private final static PVector CLOUD_CENTER = new PVector(0.0f, 0.0f, 1000.0f);
	private final static float CUTOFF = 200.0f;
	
	Time time = null;
	private Firefly[] fireflies = null;
	private FireflyParameters fflyParams = new FireflyParameters();
	private FireflyParameters fflyCloudParams = new FireflyParameters();
	private CentralField cloudField = new CentralField(CLOUD_CENTER);
	private BodyField vaiBodyField = null;
	private FireflyColorTable colorTable = null;
	private EMOTION prevEmotion = EMOTION.NONE;
	//If VAI's color is controlled by the emotion of the gesture (or by the energy)
	private boolean isColorEmotion = true;
	//If VAI's color for EMOTION.NONE is dependent on the previous gesture and can be different colors (or only a single color)
	private boolean isMultiNeutralEmotionMode = false;
	
	private LeaderRecord awaitingRecording = null;
	private Gesture awaitingGesture = null;
	private LeaderRecord greetingRecording = null;
	private LeaderRecord goodbyeAvaitingRecording = null;
	private LeaderRecord ongoingRecording = null;
	private Gesture ongoingGesture = null;
	
	private float desiredDuration = 0.0f;
	private Random random = new Random();
	
	public String[] greetingFilesList = null;
	private boolean isCloudMode = true;
	private Body vaiBody;

	public String perceptionActionStatusFileName = "PerceptionActionVAIStatus.txt";
	
	public VAIGraphics(PApplet master, Time time) {
		preSetup(perceptionActionStatusFileName);
		VAIGraphics.vgSimpleton = this;
		this.time = time;
		PGraphics vaibuffer = master.createGraphics(master.width, master.height);
		super.init(vaibuffer);
		if(isColorEmotion) colorTable = FireflyColorTable.StandardEmotion(master);
		else colorTable = FireflyColorTable.Standard(master);
		File gesturesFolder = new File(GREETINGS_DIR);
		greetingFilesList = gesturesFolder.list();
		launchFireflies();
	}

	private void launchFireflies() {
		float charSize = 700.0f;
		fireflies = new Firefly[1500];
		fflyCloudParams.adjustToScale(charSize/6.0f, charSize);
		//translateGesture();
		
		for (int i = 0; i < fireflies.length; i++) {
			PVector position = PVector.add(CLOUD_CENTER, 
					PVector.mult(PVecUtilities.randomDirection(random), charSize));
			PVector heading = PVecUtilities.randomDirection(random);
			fireflies[i] = new Firefly(position, heading, random);
		}
	}
	
	@Override
	public void run() {
		while (true) {
			drawVAI();
			supply();
			writeHeartBeat();
		}
	}
	
	private void drawVAI() {
		buffer.beginDraw();
		buffer.noStroke();
		buffer.fill(0, 0, 0, fflyParams.FADE);
		buffer.rect(0, 0, buffer.width, buffer.height);
		buffer.translate(buffer.width/2, buffer.height/2);
		vaiBody = null;
		
		if (null == awaitingRecording) {
			JointSpaceGesture spaceGesture = FileUtilities.deserializeJointsGesture(jsoarJointsInPath);
			
			if (null != spaceGesture) {
				FileUtilities.clearText(jsoarJointsInPath);
				awaitingRecording = new LeaderRecord(spaceGesture.getGestureFramesList());
			}
		}
		
		if (null == awaitingGesture) {
//			//awaitingGesture = FileUtilities.readFromText(jsoarPredicatesInPath);
			awaitingGesture = FileUtilities.deserializeGesture(jsoarPredicatesInPath);
			
			if (null != awaitingGesture) {
				System.out.println("Got predicates!!\n");
				FileUtilities.clearText(jsoarPredicatesInPath);
			}
		}
		
		if (null != awaitingRecording && null != awaitingGesture) {
			System.out.println("replaying!");
			ongoingRecording = awaitingRecording;
			ongoingGesture = awaitingGesture;
			awaitingRecording = null;
			awaitingGesture = null;
			desiredDuration = ongoingRecording.getBasicDuration();
			if(isColorEmotion) translateGestureEmotion(ongoingGesture.getEmotion());
			translateTransformPredicates();
		}
		
		// draw background
		buffer.noStroke();
		//strokeWeight(0);

		// draw the skeleton if it's available
		if (null != ongoingRecording) {
			toNormalMode();
			greetingRecording = null;
			
			translateFramePredicates();
			vaiBody = ongoingRecording.replayFrame(time.getDeltaTime());
			
			if (ongoingRecording.getTimePlayed() > desiredDuration) {
				ongoingRecording = null;
				ongoingGesture = null;
			}
		} else if (null != goodbyeAvaitingRecording) {
			greetingRecording = goodbyeAvaitingRecording;
			goodbyeAvaitingRecording = null;
		}
			
		if (null != greetingRecording) {
			toNormalMode();
			vaiBody = greetingRecording.replayFrame(time.getDeltaTime());
			
			if (!greetingRecording.isRepetitionOn() && greetingRecording.getTimePlayed() > greetingRecording.getBasicDuration()) {
				greetingRecording = null;
				toCloudMode();
			}
		}
		
		if (null != vaiBody) {
			if (null == vaiBodyField) {
				vaiBodyField = new BodyField(vaiBody);
				PVector delta = PVector.sub(vaiBody.get(JIDX.TORSO), vaiBody.get(JIDX.RIGHT_FOOT));
				float charSize = delta.mag();
				fflyParams.adjustToScale(charSize / 6.0f, charSize / 15.0f);
			} else {
				vaiBodyField.update(vaiBody);
			}
		}
	
		float dt = Time.getDeltaTime();
		FireflyParameters controlParams = null;
		Field controlField = null;
		
		if (isCloudMode) {
			controlField = cloudField;
			controlParams = fflyCloudParams;
		} else {
			controlField = vaiBodyField;
			controlParams = fflyParams;
		}
		
		for (int i = 0; i < fireflies.length; i++) {
			fireflies[i].fly(controlField, controlParams, dt);
			drawFirefly(fireflies[i]);
		}
		
		if (!isCloudMode) {
			for (int i = 0; i < fireflies.length / 40; i++) {
				int idx = (int)(fireflies.length*random.nextFloat());
				fireflies[idx].teleport(vaiBodyField.teleportEndpoint(random));
			}
		}
			
		buffer.endDraw();
	}
	
	public void onNewUser() {
		if (null == ongoingRecording && (null == greetingRecording || !greetingRecording.isRepetitionOn())) {
			greetingRecording = loadRandomGreeting(true);
		}
	}
	
	public void onLostUser() {
		if (null == goodbyeAvaitingRecording) {
			goodbyeAvaitingRecording = loadRandomGreeting(false);
		}
	}
	
	private LeaderRecord loadRandomGreeting(boolean repetitionOn) {
		String greetingFile = "greeting/" + greetingFilesList[random.nextInt(greetingFilesList.length)];
		JointSpaceGesture greeting = FileUtilities.deserializeJointsGesture(greetingFile);
		if (null == greeting) return null;
		LeaderRecord gRecord = new LeaderRecord(greeting.getGestureFramesList());
		gRecord.setIsRepetitionOn(repetitionOn);
		return gRecord;
	}
	
	private void drawFirefly(Firefly firefly) {
		if (firefly.position.z < CUTOFF)
			return;
		int color = colorTable.getColor(fflyParams.SPECTRE_PITCH, firefly.intencity, buffer);
		buffer.stroke(color);
		PVector screenPos = getScreenPos(firefly.position);
		PVector lastScreenPos = getScreenPos(firefly.lastPosition);
		int sz = (int)(5.0f*screenPos.z + 2.0f*firefly.intencity);
		if (sz < 0)
			sz = 0;
		buffer.strokeWeight(sz);		
		buffer.line(lastScreenPos.x, lastScreenPos.y, screenPos.x, screenPos.y);
	}
	
	private PVector getScreenPos(PVector pos) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.x = - screenPos.x;
		screenPos.y = - screenPos.y;
		screenPos.z = 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	private void translateFramePredicates() {
		ArrayList<FramePredicates> predHist = ongoingGesture.All_Predicate_History;
		int gestFrameI = (int)((predHist.size() - 1) * ongoingRecording.percentPassed());	
		FramePredicates predicates = predHist.get(gestFrameI);
		translateGestureEnergy(predicates.getFrameEnergy());
		translateGestureTempo(predicates.getFrameTempo());
		translateGestureSmoothness(predicates.getFrameSmoothness());
	}
	
	private void translateGestureEmotion(EMOTION emotion) {
		if (EMOTION.ANGRY == emotion) {
			fflyParams.setSpectrePitch(0.0f);
//			System.out.println("ANGRY");
		} else if (EMOTION.SAD == emotion) {
			fflyParams.setSpectrePitch(0.25f);
//			System.out.println("SAD");
		} else if (EMOTION.JOY == emotion) {
			fflyParams.setSpectrePitch(0.5f);
//			System.out.println("JOY");
		} else if (EMOTION.FEAR == emotion) {
			fflyParams.setSpectrePitch(0.75f);
//			System.out.println("FEAR");
		} else if (EMOTION.NONE == emotion) {
			if(isMultiNeutralEmotionMode) {
//				System.out.println("IS MULTI NEUTRAL EMOTION MODE");
				if (EMOTION.SAD == prevEmotion) {
					fflyParams.setSpectrePitch(0.125f);
//					System.out.println("SAD -> NONE");
				} else if (EMOTION.JOY == prevEmotion) {
					fflyParams.setSpectrePitch(0.375f);
//					System.out.println("JOY -> NONE");
				} else if (EMOTION.FEAR == prevEmotion) {
					fflyParams.setSpectrePitch(0.625f);
//					System.out.println("FEAR -> NONE");
				} else if (EMOTION.ANGRY == prevEmotion) {
					fflyParams.setSpectrePitch(0.875f);
//					System.out.println("ANGRY -> NONE");
				}else {
					fflyParams.setSpectrePitch(0.4167f);
//					System.out.println("NONE -> NONE");
				}
			} else {
				fflyParams.setSpectrePitch(0.4167f);
//				System.out.println("IS NOT MULTI NEUTRAL EMOTION MODE");
			}
		}
		prevEmotion = emotion;
	}

	private void translateGestureEnergy(ENERGY energy) {	
		if (ENERGY.ALMOST_NONE == energy) {
			if(!isColorEmotion) fflyParams.setSpectrePitch(0.0f);
			fflyParams.setElectrization(0.1f);
			fflyParams.setSparkleness(0.0f);
		} else if (ENERGY.LOW == energy) {
			if(!isColorEmotion) fflyParams.setSpectrePitch(0.2f);
			fflyParams.setElectrization(0.2f);
			fflyParams.setSparkleness(0.4f);
		} else if (ENERGY.MEDIUM == energy) {
			if(!isColorEmotion) fflyParams.setSpectrePitch(0.5f);	
			fflyParams.setElectrization(0.4f);
			fflyParams.setSparkleness(0.7f);
		} else if (ENERGY.HIGH == energy) {
			if(!isColorEmotion) fflyParams.setSpectrePitch(0.9f);
			fflyParams.setElectrization(0.8f);
			fflyParams.setSparkleness(0.9f);
		} else if (ENERGY.EXTREMELY_HIGH == energy) {
			if(!isColorEmotion) fflyParams.setSpectrePitch(1.0f);	
			fflyParams.setElectrization(1.0f);
			fflyParams.setSparkleness(1.0f);
		}
	}
	
	private void translateGestureTempo(TEMPO tempo) {
		if (TEMPO.ALMOST_STILL == tempo) {
			fflyParams.setFlightSpeed(0.5f*fflyParams.BASIC_SPEED);	
		} else if (TEMPO.SLOW == tempo) {
			fflyParams.setFlightSpeed(0.7f*fflyParams.BASIC_SPEED);	
		} else if (TEMPO.MEDIUM == tempo) {
			fflyParams.setFlightSpeed(1.0f*fflyParams.BASIC_SPEED);	
		} else if (TEMPO.FAST == tempo) {
			fflyParams.setFlightSpeed(2.0f*fflyParams.BASIC_SPEED);	
		} else if (TEMPO.EXTREMELY_FAST == tempo) {
			fflyParams.setFlightSpeed(4.0f*fflyParams.BASIC_SPEED);	
		}
	}
	
	private void translateGestureSmoothness(SMOOTHNESS smoothness) {
		if (SMOOTHNESS.SMOOTH == smoothness) {
			fflyParams.setFade(32.0f);
			fflyParams.setAnxiety(2.0f);
		} else if (SMOOTHNESS.REGULAR == smoothness) {
			fflyParams.setFade(64.0f);
			fflyParams.setAnxiety(4.0f);
		} else if (SMOOTHNESS.STACCATO == smoothness) {
			fflyParams.setFade(190.0f);
			fflyParams.setAnxiety(5.0f);
		}
	}
	
	private void translateTransformPredicates() {
		for (TransformPredicateInterface transform : ongoingGesture.transforms) {
			if (null != TransformPredicates.framePredicateModifiedBy(transform.getClass().getSimpleName())) {
				ArrayList<FramePredicates> predicateHistory = ongoingGesture.All_Predicate_History;
				
				for (FramePredicates predicates : predicateHistory) {
					TransformPredicates.alterFramePredicates(transform, predicates);
				}
			}
					
			if (transform instanceof TRANSFORM_DURATION) {
				boolean hasRepeat = false;
				
				for (TransformPredicateInterface otherTransform : ongoingGesture.transforms) {
					if (otherTransform instanceof ADD_REPEAT) {
						hasRepeat = true;
						break;
					}
				}
					
				if (!hasRepeat) {
					adjustDesiredDuration((TRANSFORM_DURATION) transform);
				}
			} else if (transform instanceof TRANSFORM_TEMPO) {
				adjustTempo((TRANSFORM_TEMPO)transform);
			} else if (transform instanceof ADD_REPEAT) {
				TRANSFORM_TEMPO tempoTransform = null;
				
				for (TransformPredicateInterface otherTransform : ongoingGesture.transforms) {
					if (otherTransform instanceof TRANSFORM_TEMPO) {
						tempoTransform = (TRANSFORM_TEMPO) otherTransform;
						break;
					}
				}
				
				adjustRepeat((ADD_REPEAT)transform, tempoTransform);
			}
		}
	}
	

	private void adjustDesiredDuration(TRANSFORM_DURATION durationTransform) {
		if (TRANSFORM_DURATION.MUCH_SHORTER == durationTransform) {
			desiredDuration = desiredDuration / 3.0f;
		} else if (TRANSFORM_DURATION.SHORTER == durationTransform) {
			desiredDuration = 2.0f * desiredDuration / 3.0f;
		} else if (TRANSFORM_DURATION.LONGER == durationTransform) {
			desiredDuration = 3.0f * desiredDuration / 2.0f;
		} else if (TRANSFORM_DURATION.MUCH_LONGER == durationTransform) {
			desiredDuration = 3.0f * desiredDuration;
		}
	}
	
	private float tempoFactor(TRANSFORM_TEMPO transformTempo) {
		if (TRANSFORM_TEMPO.MUCH_SLOWER == transformTempo) {
			return 1.0f / 3.0f;
		} else if (TRANSFORM_TEMPO.SLOWER == transformTempo) {
			return 2.0f / 3.0f;
		} else if (TRANSFORM_TEMPO.FASTER == transformTempo) {
			return 3.0f / 2.0f;
		} else if (TRANSFORM_TEMPO.MUCH_FASTER == transformTempo) {
			return 3.0f;
		} else {
			return 1.0f;
		}
	}
	
	private void adjustTempo(TRANSFORM_TEMPO transformTempo) {
		ongoingRecording.setReplaySpeed(tempoFactor(transformTempo));
	}
	
	private void adjustRepeat(ADD_REPEAT repeatTimes, TRANSFORM_TEMPO transformTempo) {
		float durationFactor = 1.0f / tempoFactor(transformTempo);
	
		if (ADD_REPEAT.FEW == repeatTimes) {
			durationFactor *= 2;
		} else if (ADD_REPEAT.MEDIUM == repeatTimes) {
			durationFactor *= 3;
		} else if (ADD_REPEAT.MANY == repeatTimes) {
			durationFactor *= 6;
		}
		
		desiredDuration = desiredDuration * durationFactor;
	}
	
	private void toCloudMode() {
		if (!isCloudMode) {
			isCloudMode = true;
		}
	}
	
	private void toNormalMode() {
		if (isCloudMode) {
			isCloudMode = false;
		}
	}
	
	// Global switch
	private static VAIGraphics vgSimpleton;
	public static boolean isVaiAnimating() {
		if (vgSimpleton == null) {
			//DO NOT SPAM PLEASE!!!
//			System.err.println("Warning: No VAI detected!");
			return false;
		} else {
			return vgSimpleton.ongoingRecording != null;
		}
	}
	
	public static PVector getVaiPos() {
		if (vgSimpleton == null || vgSimpleton.vaiBody == null) {
			//DO NOT SPAM PLEASE!!!
//			System.err.println("Warning: No VAI detected!");
			return null;
		} else {
			return vgSimpleton.getScreenPos(vgSimpleton.vaiBody.get(JIDX.TORSO));
		}
	}
}
