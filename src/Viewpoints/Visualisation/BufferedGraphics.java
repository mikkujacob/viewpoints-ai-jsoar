package Viewpoints.Visualisation;

import java.io.File;
import java.io.IOException;

import Viewpoints.FileUtilities.FileUtilities;
import processing.core.PGraphics;
import processing.core.PImage;

public abstract class BufferedGraphics implements Runnable {
	protected PGraphics buffer = null;
	private boolean supplied = false;
	
	public String statusDir = "status";
	public String perceptionActionStatusFileName = "";
	public String PROJECT_HOME;
	
	private long perceptionActionCounter = 0;
	private long iterationCounter = 0;
	private  boolean isMonitorable = true;
	
	protected void init(PGraphics buffer) {
		this.buffer = buffer;
	}
	
	public void preSetup(String statusFileName)
	{
		PROJECT_HOME = System.getProperty("user.dir");
		if(PROJECT_HOME.equalsIgnoreCase(null))
		{
			try
			{
				PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			
			if(PROJECT_HOME.equalsIgnoreCase(null))
			{
				System.out.println("ERROR! PROJECT_HOME IS NULL");
			}
		}
		
		statusDir = PROJECT_HOME + statusDir;
		perceptionActionStatusFileName = statusFileName;
	}
	
	public PImage demand() {
		synchronized (buffer) {
			isMonitorable = false;
			writeHeartBeat();
			while (!supplied) {
				try {buffer.wait();} catch (InterruptedException e) {}
			}
			isMonitorable = true;
			writeHeartBeat();
			supplied = false;
			buffer.notifyAll();
			return buffer.get();
		}
	}
	
	protected void supply() {
		synchronized (buffer) {
			supplied = true;
			buffer.notify();
			isMonitorable = false;
			writeHeartBeat();
			while (supplied) {
				try {buffer.wait();} catch (InterruptedException e) {}
			}
			isMonitorable = true;
			writeHeartBeat();
		}
	}
	
	protected void writeHeartBeat()
	{
		if(isMonitorable)
		{
			if(iterationCounter % 100 == 0)
			{
				FileUtilities.writeToFile(statusDir + File.separator + perceptionActionStatusFileName, perceptionActionCounter + "");
				++perceptionActionCounter;
				if(perceptionActionCounter < Long.MAX_VALUE && perceptionActionCounter > Long.MAX_VALUE - 100)
				{
					perceptionActionCounter = 10;
				}
			}
		}
		else
		{
			FileUtilities.writeToFile(statusDir + File.separator + perceptionActionStatusFileName, -666 + "");
		}
		++iterationCounter;
		if(iterationCounter < Long.MAX_VALUE && iterationCounter > Long.MAX_VALUE - 50)
		{
			iterationCounter = 0;
		}
	}
}
