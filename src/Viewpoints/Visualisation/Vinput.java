package Viewpoints.Visualisation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import SimpleOpenNI.*; 
import Viewpoints.KinectParsing.GestureController;
import Viewpoints.MotionModel.MimicryManager;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.JointSpaceGesture;
import Viewpoints.Shared.LIDX;
import Viewpoints.Shared.LIMB;
import Viewpoints.Shared.LeaderRecord;
import Viewpoints.Shared.PVecUtilities;
import Viewpoints.Shared.Pair;
import processing.core.*;
import polymonkey.time.*;
import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Fireflies.*;
import Viewpoints.Gesture.FramePredicates;
import Viewpoints.Gesture.Gesture;
import Viewpoints.Gesture.Predicates.ENERGY;
import Viewpoints.Gesture.Predicates.SMOOTHNESS;
import Viewpoints.Gesture.Predicates.TEMPO;
import Viewpoints.Gesture.TransformPredicates;
import Viewpoints.Gesture.TransformPredicates.ADD_COPY_LIMB;
import Viewpoints.Gesture.TransformPredicates.ADD_REFLECT_LIMB;
import Viewpoints.Gesture.TransformPredicates.ADD_REPEAT;
import Viewpoints.Gesture.TransformPredicates.ADD_SWITCH_LIMBS;
import Viewpoints.Gesture.TransformPredicates.REFLECT_TYPE;
import Viewpoints.Gesture.TransformPredicates.TRANSFORM_DURATION;
import Viewpoints.Gesture.TransformPredicates.TRANSFORM_TEMPO;
import Viewpoints.Gesture.TransformPredicates.TransformPredicateInterface;

public class Vinput extends PApplet {
	public static final String kinectPredicatesOutPath = "file_communications/KinectPredicatesToJSOAR.txt";
	public static final String kinectJointsOutPath = "file_communications/KinectJointsToJSOAR.txt";

	/**
	 * Generated serial id
	 */
	private static final long serialVersionUID = -6048102105138254150L;
	
	Time time = null;

	final int numJoints = 22; // number of skeleton joint
	
	private SimpleOpenNI kinectInput = null;
	private GestureController gc = null;
	
	public ArrayList<Body> jointFrameList = new ArrayList<Body>();
	public JointSpaceGesture recordedGesture = new JointSpaceGesture();
	
	ShadowGraphics shadowGraphics = null;
	VAIGraphics vaiGraphics = null;

	public String statusDir = "status";
	public String perceptionActionStatusFileName = "PerceptionActionStatus.txt";
	public String PROJECT_HOME;
	
	private long perceptionActionCounter = 0;
	private long iterationCounter = 0;
	
	public void setup()
	{
		time = new Time(this);
		size (displayWidth, displayHeight);
		this.frameRate(30);
		
		preSetup();
		
		gc = new GestureController();
		
		kinectInput = new SimpleOpenNI(this);
		
		if (kinectInput.init()) {
			kinectInput.setMirror(false);
			kinectInput.enableDepth();
			kinectInput.enableUser();
			shadowGraphics = new ShadowGraphics(kinectInput, this);
			vaiGraphics = new VAIGraphics(this, time);
			Thread shadowThread = new Thread(shadowGraphics);
			shadowThread.start();
			Thread vaiThread = new Thread(vaiGraphics);
			vaiThread.start();
		} else {
			kinectInput = null;
			background(255, 0, 0);
			noLoop();
		}
	}
	
	public void preSetup()
	{
		PROJECT_HOME = System.getProperty("user.dir");
		if(PROJECT_HOME.equalsIgnoreCase(null))
		{
			try
			{
				PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			
			if(PROJECT_HOME.equalsIgnoreCase(null))
			{
				System.out.println("ERROR! PROJECT_HOME IS NULL");
			}
		}
		
		statusDir = PROJECT_HOME + statusDir;
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public void draw() {
		if (null == kinectInput) 
			return;
		
		scanInput();
		combineBuffers();
		writeHeartBeat();
	}
	
	private void writeHeartBeat()
	{
		if(iterationCounter % 100 == 0)
		{
			FileUtilities.writeToFile(statusDir + File.separator + perceptionActionStatusFileName, perceptionActionCounter + "");
			++perceptionActionCounter;
			if(perceptionActionCounter < Long.MAX_VALUE && perceptionActionCounter > Long.MAX_VALUE - 100)
			{
				perceptionActionCounter = 10;
			}
		}
		++iterationCounter;
		if(iterationCounter < Long.MAX_VALUE && iterationCounter < Long.MAX_VALUE - 50)
		{
			iterationCounter = 0;
		}
	}
	
	private void scanInput() {
		Body userBody = null;
		
		synchronized (kinectInput) {
			kinectInput.update();

			int[] userList = kinectInput.getUsers();
			
			// draw the skeleton if it's available
			for(int i = 0; i < userList.length; i++)
			{
				int userId = userList[i];
				if(kinectInput.isTrackingSkeleton(userId)) 
				{
					userBody = new Body();
					userBody.initialize(kinectInput, userId, Time.getCountTime());
					jointFrameList.add(userBody);
					
					break;
				}
			}
		}
		if (null != userBody)
			gc.update(userBody);
		
		if(gc.hasGesture())
		{
//			FileUtilities.writeToText(kinectPredicatesOutPath, gc.yieldCurrentGesture());
			Boolean isGestureWritten = FileUtilities.serializeGesture(kinectPredicatesOutPath, gc.yieldCurrentGesture());
			recordedGesture = gc.yieldJSGesture();
			isGestureWritten = isGestureWritten && FileUtilities.serializeJointsGesture
					(kinectJointsOutPath, recordedGesture);
			
			if(!isGestureWritten)
			{
				System.out.println("Error: Did not write Segmented Gesture");
			}
		}
	}

	private void combineBuffers() {
		PImage usrim = shadowGraphics.demand();
		PImage vaiim = vaiGraphics.demand();
		background(vaiim);
		
		float wRatio = ((float)width) / usrim.width;
		float hRatio = ((float)height) / usrim.height;
		float scale  = Math.min(wRatio, hRatio);
		float onScrWidth = usrim.width * scale;
		float onScrHeight = usrim.height * scale;
		float x = (width - onScrWidth) / 2.0f;
		float y = (height - onScrHeight) / 2.0f;
		
		blend(usrim, 0, 0, usrim.width, usrim.height, (int)x, (int)y, (int)onScrWidth, (int)onScrHeight, ADD);
	}
	
	// -----------------------------------------------------------------
	// SimpleOpenNI events
	
	public void onNewUser(SimpleOpenNI context, int userId)
	{
		println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
		vaiGraphics.onNewUser();
	}

	public void onLostUser(SimpleOpenNI context, int userId)
	{
		int[] trackedUsers = null;
		synchronized (kinectInput) {
			trackedUsers = kinectInput.getUsers();
		}
		if (trackedUsers.length == 0) {
			vaiGraphics.onLostUser();
		}
		println("onLostUser - userId: " + userId);
	}
	
	public void onVisibleUser(SimpleOpenNI context, int userId)
	{
	//		  println("onVisibleUser - userId: " + userId);
	}
	
	public static void main(String[] args) {
//		PApplet.main(new String[]{"--present", "Viewpoints.Visualisation.Vinput"});
		PApplet.main("Viewpoints.Visualisation.Vinput", new String[]{"--full-screen", "--display=1"});

	}
}
