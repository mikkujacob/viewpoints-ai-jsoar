package Viewpoints.Visualisation.viz;

import java.awt.Point;

/**
 * Midpoint-smoothing chain of vectors that can be used to fake momentum or
 * create smooth paths. Anchors around goals on opposing sides of the chain.
 * 
 * @author toriscope
 * 
 */
public class MidpointChain {

	private Point.Float[] chain;

	/**
	 * Initialize a chain. Every link will be set to A, initially, except for
	 * the very end link, which is set to B.
	 * 
	 * @param a
	 *            one anchor end
	 * @param b
	 *            other anchor end
	 * @param size
	 *            number of links. Add more for smoother/slower movements
	 */
	public MidpointChain(final Point.Float a, final Point.Float b, final int size) {
		this.chain = new Point.Float[size];
		for (int i = 0; i < this.chain.length; i++) {
			this.chain[i] = a;
		}
		this.chain[chain.length - 1] = b;
	}

	/**
	 * Initialize a chain. Every link will be set to a.
	 * 
	 * @param a
	 *            anchor ends
	 * @param size
	 *            number of links. Add more for smoother/slower movements
	 */
	public MidpointChain(final Point.Float a, final int size) {
		this.chain = new Point.Float[size];
		for (int i = 0; i < this.chain.length; i++) {
			this.chain[i] = a;
		}
		this.chain[chain.length - 1] = a;
	}

	/**
	 * Smoothes chain forward, anchored on a
	 */
	public void smoothTowardA() {
		float x, y;
		for (int i = 1; i < chain.length; i++) {
			x = (chain[i].x + chain[i - 1].x) / 2;
			y = (chain[i].y + chain[i - 1].y) / 2;
			chain[i] = new Point.Float(x, y);
		}
	}

	/**
	 * Smoothes chain backward, anchored on b
	 */
	public void smoothTowardB() {
		float x, y;
		for (int i = chain.length - 2; i >= 0; i--) {
			x = (chain[i].x + chain[i + 1].x) / 2;
			y = (chain[i].y + chain[i + 1].y) / 2;
			chain[i] = new Point.Float(x, y);
		}
	}

	/**
	 * Smoothes toward a and b. This will cause the entire chain to appear to
	 * "shrink" and smooth.
	 */
	public void smooth() {
		smoothTowardB();
		smoothTowardA();
	}

	public void setA(final Point.Float a) {
		chain[0] = a;
	}

	public void setB(final Point.Float b) {
		chain[chain.length - 1] = b;
	}

	public Point.Float getA() {
		return chain[0];
	}

	public Point.Float getB() {
		return chain[chain.length - 1];
	}

	public Point.Float[] getChain() {
		return chain;
	}
}