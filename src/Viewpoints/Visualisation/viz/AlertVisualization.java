package Viewpoints.Visualisation.viz;

import java.awt.Color;
import java.awt.Point;

import polymonkey.time.Time;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.Visualisation.VAIGraphics;
import Viewpoints.Visualisation.viz.AlertVisualization.Side;

/**
 * Render and manage the states of an alert system.
 * 
 * @author toriscope
 * 
 */
public class AlertVisualization {

	public int curr_width, gradient_fine, width_speed;
	public float strength, width_percent;
	public float alertTimeoutms;

	private Side side = Side.LEFT;
	private AnimState animState = AnimState.STOPPED;
	private int width_speed_t;

	public enum Side {
		LEFT, RIGHT
	}

	public enum AnimState {
		EXPANDING, CONTRACTING, STOPPING, STOPPED, STARTED
	}

	public AlertVisualization() {
		curr_width = 0;
		defaultCustomize();
	}

	/**
	 * Set to the default configuration.
	 */
	public void defaultCustomize() {
		width_speed = 30;
		gradient_fine = 4;
		width_percent = .10f;
		strength = 0.05f;
		alertTimeoutms = 2 * 1000;
	}

	/**
	 * Set the aniamtion state.
	 * 
	 * @param state
	 */
	public void setState(final AnimState state) {
		this.animState = state;
	}

	/**
	 * Set the aniamtion state.
	 * 
	 * @param state
	 */
	public void activateIfInactive() {
		if (animState == AnimState.STOPPED) {
			setState(AnimState.STARTED);
		}
	}

	/**
	 * Set the side of screen.
	 * 
	 * @param side
	 */
	public void setSide(final Side side) {
		this.side = side;
	}
	
	private long pastTime = 0;


	/**
	 * Draw the alert.
	 * @param buffer
	 * @param kinectInput
	 * @param depthImage a depth image with the pixels already loaded.
	 */
	public void draw(final PGraphics buffer, final SimpleOpenNI kinectInput, final PImage depthimage) {
		
		long newTime = System.currentTimeMillis();
		if (pastTime == 0) pastTime = newTime;
		final long msSinceLastDetect = newTime - pastTime;

		boolean userTouchingSides = false;
		int detectMarginWidth = 10;

		for (int j = 0; j < depthimage.height; j++) {
			// Examine RIGHT side
			for (int x = depthimage.width - detectMarginWidth; x < depthimage.width; x++) {
				int pixL = x + depthimage.width * j;
				if (buffer.red(depthimage.pixels[pixL]) != buffer.blue(depthimage.pixels[pixL])) {
					this.setSide(Side.RIGHT);
					this.activateIfInactive();
					userTouchingSides = true;
				}
			}
			// Examine LEFT side
			for (int x = 0; x < detectMarginWidth; x++) {
				int pixR = x + depthimage.width * j;
				if (buffer.red(depthimage.pixels[pixR]) != buffer.blue(depthimage.pixels[pixR])) {
					this.setSide(Side.LEFT);
					this.activateIfInactive();
					userTouchingSides = true;
				}
			}
		}

		if (userTouchingSides) {
			pastTime = newTime;
		}

		// Close down alert if been a while since user touched edge.
		if (msSinceLastDetect > alertTimeoutms) {
			this.setState(AnimState.STOPPING);
		}

		this.drawAlert(buffer);
	}

	/**
	 * Manage state transitions.
	 * 
	 * @param buffer
	 */
	private void update(final PGraphics buffer) {
		int w = buffer.width;

		switch (animState) {
		case STARTED:
			animState = AnimState.EXPANDING;
			break;
		case STOPPED:
			break;
		case EXPANDING:
			width_speed_t = width_speed * 1;
			if (curr_width > width_percent * w) {
				animState = AnimState.CONTRACTING;
			}
			break;
		case CONTRACTING:
			width_speed_t = width_speed * -1;
			if (curr_width < 0) {
				animState = AnimState.EXPANDING;
			}
			break;
		case STOPPING:
			width_speed_t = width_speed * -1;
			if (curr_width < 0) {
				animState = AnimState.STOPPED;
			}
			break;
		}
	}

	/**
	 * Perform the draw methods on the PApplet.
	 */
	private void drawAlert(final PGraphics buffer) {
		update(buffer);
		if (animState != AnimState.STOPPED) {
			int w = buffer.width;
			int h = buffer.height;
			buffer.rectMode(PApplet.RADIUS);
			curr_width += width_speed_t;
			for (int i = curr_width; i > 0; i -= gradient_fine) {
				buffer.noStroke();
				buffer.fill(255, 0, 0, 255 * strength);
				int startx = side == Side.RIGHT ? w : 0;
				buffer.rect(startx, 0, i, h);
			}
		}
	}

	public Side getSide() {
		return side;
	}
}
