package Viewpoints.MotionModel;

import java.lang.reflect.ReflectPermission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import processing.core.PVector;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.LIDX;
import Viewpoints.Shared.LIMB;
import Viewpoints.Shared.PVecUtilities;
import Viewpoints.Shared.Pair;
import Viewpoints.Shared.Plane;
import Viewpoints.Gesture.TransformPredicates.REFLECT_TYPE;

public class MimicryManager {
	private Body body;
	private HashMap<LIMB, MimickingPartDriver> partDrivers = new HashMap<LIMB, MimickingPartDriver>();
	private ArrayList<CopyingData> copyInstructions = new ArrayList<CopyingData>();
	//private int watch = 0;
	
	public MimicryManager() {
		for (LIMB limb : LIMB.ALL_LIMBS) {
			JIDX[] partJidx = LIMB.limbJoints(limb);
			partDrivers.put(limb, new MimickingPartDriver(partJidx[0], partJidx[1], partJidx[2]));
		}
	}
	
	public Body doMimicking(Body iBody) {	
		body = (Body) iBody.clone();
		
		for (CopyingData cData : copyInstructions) {
			cData.lazyInit();
		}
		
		for (MimickingPartDriver driver: partDrivers.values()) {
			driver.refresh();
		}
		
		doCopying();
		
		for (MimickingPartDriver driver: partDrivers.values()) {
			driver.apply();
		}
		
		return body;
	}
	
	public void setupReflection(LIMB iBodyPart, REFLECT_TYPE iMode, boolean isOn) 
	{
		MimickingPartDriver driver = partDrivers.get(iBodyPart);
		
		if (REFLECT_TYPE.LONGITUDINALLY == iMode) {
			driver.setLongtitudalReflection(isOn);
		} else if (REFLECT_TYPE.TRANSVERSALLY == iMode) {
			driver.setTransversalReflection(isOn);
		} else if (REFLECT_TYPE.VERTICALLY == iMode) {
			driver.setVerticalReflection(isOn);
		}
	}
	
	public void setupSwitching(LIMB iSwitch1, LIMB iSwitch2, boolean iOn) {
		setupCopy(iSwitch1, iSwitch2, iOn);
		setupCopy(iSwitch2, iSwitch1, iOn);
	}
	
	public void setupCopy(LIMB iSource, LIMB iTarget, boolean iOn) {
		if (iOn) {
			copyInstructions.add(new CopyingData(iSource, iTarget));
		} else {
			for (int i = 0; i < copyInstructions.size() ; i++) {
				if (copyInstructions.get(i).source == iSource && copyInstructions.get(i).target == iTarget) {
					copyInstructions.remove(i--);
				}
			}
		}
	}
	
	public void reset() {
		copyInstructions.clear();
		for (MimickingPartDriver partDriver : partDrivers.values()) {
			partDriver.reset();
		}
	}

	private void doCopying() {
		for (CopyingData copyData : copyInstructions) {
			LIMB source = copyData.source;
			LIMB target = copyData.target;
			MimickingPartDriver sourceDriver = partDrivers.get(source);
			MimickingPartDriver targetDriver = partDrivers.get(target);
			JIDX sourceLeadingJoint = sourceDriver.getLeadingJoint();
			PVector refPoint = mainReferencePoint();
			PVector sourceDelta = PVector.sub(body.get(sourceLeadingJoint), refPoint);
			PVector[] sourceRefFrame = referenceFrameForPartType(source);
			float[] sourceDeltaDissolution = PVecUtilities.dissolve(sourceDelta, sourceRefFrame);
			float[] deltaDissolution = {0.0f, 0.0f, 0.0f};
			
			for (int i = 0; i < 3; i++) {
				deltaDissolution[i] = sourceDeltaDissolution[i] - copyData.sourceReferenceDissolution[i];
			}
			
			if (LIMB.isLeft(source) != LIMB.isLeft(target)) {
				deltaDissolution[0] = -deltaDissolution[0];
			}
			
			float[] targetDeltaDissolution = {0.0f, 0.0f, 0.0f};
			
			for (int i = 0; i < 3; i++) {
				targetDeltaDissolution[i] = deltaDissolution[i] + copyData.targetReferenceDissolution[i];
			}
			
			PVector[] targetRefFrame = referenceFrameForPartType(target);
			PVector targetDelta = PVecUtilities.assemble(targetRefFrame, targetDeltaDissolution);
			PVector targetLocation = PVector.add(refPoint, targetDelta);
			targetDriver.setLeadingPoint(targetLocation);
			
			PVector sourceDirection = sourceDriver.getHintDirection();
			
			if (null != sourceDirection) {
				PVector targetDirection = sourceDirection;
				
				if (LIMB.isLeft(source) != LIMB.isLeft(target)) {
					Plane symmetryPlane = symmetryPlaneForPartType(source);
					targetDirection = PVecUtilities.reflectVec(sourceDirection, symmetryPlane.norm);
				}
				
				if (LIMB.isArm(source) != LIMB.isArm(target)) {
					targetDirection.mult(-1.0f);
				}
				
				targetDriver.setDesiredDirection(targetDirection);
			}
		}
	}
	
	private PVector mainReferencePoint() {
		PVector center = PVector.add(body.get(JIDX.LEFT_HIP), body.get(JIDX.RIGHT_HIP));
		center.mult(0.5f);
		return center;
	}
	
	private PVector[] referenceFrameForPartType(LIMB limb) {
		if (LIMB.isArm(limb)) {
			return handsReferenceFrame();
		} else {
			return feetReferenceFrame();
		}
	}
	
	private PVector[] handsReferenceFrame() {
		PVector symPlaneDir = PVector.sub(body.get(JIDX.RIGHT_SHOULDER), body.get(JIDX.LEFT_SHOULDER));
		symPlaneDir.normalize();
		PVector forwardDir = body.orientation();
		forwardDir = PVecUtilities.ortonormalization(forwardDir, symPlaneDir);
		PVector upwardDir = symPlaneDir.cross(forwardDir);
		PVector[] refFrame = {symPlaneDir, forwardDir, upwardDir};
		return refFrame;
	}
	
	private PVector[] feetReferenceFrame() {
		PVector forwardDir = body.orientation();
		PVector upwardDir = PVecUtilities.UPVEC;
		PVector symPlaneDir = upwardDir.cross(forwardDir);
		PVector[] refFrame = {symPlaneDir, forwardDir, upwardDir};
		return refFrame;
	}
	
	private Plane symmetryPlaneForPartType(LIMB limb) {
		if (LIMB.isArm(limb)) {
			return handsSymmetryPlane();
		} else {
			return feetSymmetryPlane();
		}
	}
	
	private Plane handsSymmetryPlane() {
		PVector center = body.get(JIDX.TORSO);
		PVector symPlaneDir = PVector.sub(body.get(JIDX.LEFT_SHOULDER), body.get(JIDX.RIGHT_SHOULDER));
		symPlaneDir.normalize();
		return new Plane(center, symPlaneDir);
	}
	
	private Plane feetSymmetryPlane() {
		PVector center = PVector.add(body.get(JIDX.LEFT_HIP), body.get(JIDX.RIGHT_HIP));
		center.mult(0.5f);
		PVector sidevec = PVecUtilities.UPVEC.cross(body.orientation());
		sidevec.normalize();
		return new Plane(center, sidevec);
	}
	
	private class MimickingPartDriver {
		private boolean useRelativeUp = false;
		private PVector leadingPoint = null;
		private PVector desiredDirection = null;
		//private PVector hintDirection = null;
		//private int watch = 0;
		
		public MimickingPartDriver
					(JIDX rootJoint, JIDX midJoint, JIDX leadingJoint) {
			super();
			this.rootJoint = rootJoint;
			this.midJoint = midJoint;
			this.leadingJoint = leadingJoint;
			
			if (JIDX.LEFT_HAND == this.leadingJoint || JIDX.RIGHT_HAND == this.leadingJoint) {
				this.useRelativeUp = true;
			}
		}
		
		public void reset() {
			this.longtitudalReflection = false;
			this.transversalReflection = false;
			this.verticalReflection = false;
		}
		
		public JIDX getLeadingJoint() {
			return leadingJoint;
		}
		
		public void setLeadingPoint(PVector iLeadingPoint) {
			leadingPoint = iLeadingPoint;
		}
		
		public void refresh() {
			leadingPoint = null;
		}
		
		public PVector getHintDirection() {
			return PVecUtilities.ortonormalization( PVector.sub(body.get(midJoint), body.get(rootJoint)), 
													PVector.sub(body.get(leadingJoint), body.get(rootJoint)));
		}
		
		public void setDesiredDirection(PVector iDesiredDirection) {
			desiredDirection = iDesiredDirection;
		}
		
		public void unsetDesiredDirection() {
			desiredDirection = null;
		}
		
		public void setLongtitudalReflection(boolean longtitudalReflection) {
			this.longtitudalReflection = longtitudalReflection;
		}
		
		public void setTransversalReflection(boolean transversalReflection) {
			this.transversalReflection = transversalReflection;
		}
		public void setVerticalReflection(boolean verticalReflection) {
			this.verticalReflection = verticalReflection;
		}
		
		/*public PVector calcPartPlaneNorm() {
			PVector rootLimbDir = PVector.sub(body.get(midJoint), body.get(rootJoint));
			PVector farLimbDir  = PVector.sub(body.get(leadingJoint), body.get(midJoint));
			PVector partPlaneNorm = farLimbDir.cross(rootLimbDir);
			
			if (partPlaneNorm.mag() < 0.1f) {
				partPlaneNorm = null;
			} else {
				partPlaneNorm.normalize();
			}
			
			return partPlaneNorm;
		}*/
		
		public void apply() {
			//++watch;
			PVector refPoint = body.get(rootJoint);
			PVector upvec = null;
			
			/*if (watch == 211 && midJoint == JIDX.LEFT_ELBOW) {
				watch = 56;
			}*/
			
			if (this.useRelativeUp) {
				upvec = PVector.sub(body.get(JIDX.NECK), body.get(JIDX.TORSO));
				upvec.normalize();
			} else {
				upvec = PVecUtilities.UPVEC;
			}
			
			PVector longtvec = body.orientation();
			longtvec = PVecUtilities.ortogonalization(longtvec, upvec);
			PVector sidevec = upvec.cross(longtvec);
			float rootLimbLen = PVector.sub(body.get(midJoint), refPoint).mag();
			float farLimbLen  = PVector.sub(body.get(leadingJoint), body.get(midJoint)).mag();
			
			PVector hintDirection = null;
			
			if (null != desiredDirection) {
				hintDirection = desiredDirection;
			} else if (Math.abs(longtvec.mag() - rootLimbLen - farLimbLen) > 10.0) {
				hintDirection = getHintDirection();
			}
			
			if (null != leadingPoint) {
				body.set(leadingJoint, leadingPoint);
			}
			
			boolean[] reflections =   {longtitudalReflection, transversalReflection, verticalReflection};
			PVector[] refPlaneNorms = {longtvec,              sidevec,               upvec};
			
			for (int i = 0; i < 3; i++) {
				if (reflections[i]) {
					reflectJoint(leadingJoint, refPoint, refPlaneNorms[i]);
					
					if (null == hintDirection) {
						reflectJoint(midJoint, refPoint, refPlaneNorms[i]);
					}
				}
			}
			
			if (null != hintDirection) {
				PVector midJointPoint = calculateMidJoint
					(refPoint, body.get(leadingJoint), rootLimbLen, farLimbLen, hintDirection);
				
				/*PVector oldMidJoint = body.get(midJoint);
				
				if (leadingJoint == JIDX.LEFT_HAND) {
					System.out.println(midJointPoint);
				}
				
				if (PVector.sub(body.get(midJoint), midJointPoint).mag() > 10.0f) {
					calculateMidJoint(refPoint, body.get(leadingJoint), rootLimbLen, farLimbLen, hintDirection);
				}*/
				
				body.set(midJoint, midJointPoint);
			}
			
			//body.set(midJoint, body.get(leadingJoint));
		}

		private void reflectJoint(JIDX jidx, PVector refJoint, PVector refPlaneNorm)
		{
			PVector jointPoint = body.get(jidx);
			jointPoint = PVecUtilities.reflectPoint(jointPoint, new Plane(refJoint, refPlaneNorm));
			body.set(jidx, jointPoint);
		}
		
		private PVector calculateMidJoint(PVector rootPoint, PVector leadingPoint,
				  float rootLen, float farLen,
				  PVector hintDirection) 
		{
			/*PVector rj = body.get(rootJoint);
			PVector lj = body.get(leadingJoint);
			PVector mj = body.get(midJoint);
			
			float rl = PVector.sub(rj, mj).mag();
			float fl = PVector.sub(mj, lj).mag();*/
			
			PVector delta = PVector.sub(leadingPoint, rootPoint);
			float interLen = delta.mag();
			
			if (triangleRuleHolds(rootLen, farLen, interLen)) {
				float p = (interLen + rootLen + farLen)/2.0f;
				float sqr = (float)Math.sqrt(p*(p-interLen)*(p-rootLen)*(p-farLen));
				float h = 2.0f * sqr / interLen;
				double hsqr = Math.pow(h, 2);
				float rootX = (float)Math.sqrt(Math.pow(rootLen, 2) - hsqr);
				float farX =  (float)Math.sqrt(Math.pow(farLen, 2) - hsqr);
				
				if (farX > interLen && farX > rootX) {
					rootX = -rootX;
				}
				
				PVector ort = PVecUtilities.ortonormalization(hintDirection, delta);
				delta.normalize();
				PVector resJ = PVector.add(rootPoint, PVector.add(PVector.mult(delta, rootX), PVector.mult(ort, h)));
				return resJ;
			} else {
				float scl = rootLen/(rootLen + farLen);
				return PVector.add(rootPoint, PVector.mult(delta, scl));
			}
		}
		
		private boolean triangleRuleHolds(float side1, float side2, float side3) {
			return side1 < side2 + side3 && side2 < side1 + side3 && side3 < side1 + side2;
		}
		
		private JIDX rootJoint;
		private JIDX midJoint;
		private JIDX leadingJoint;

		private boolean longtitudalReflection;
		private boolean transversalReflection;
		private boolean verticalReflection;
	}
	
	private class CopyingData {
		public CopyingData(LIMB source, LIMB target) {
			this.target = target;
			this.source = source;
		}
		
		public void lazyInit() {
			if (null == sourceReferenceDissolution) {
				JIDX sourceLeadingJoint = partDrivers.get(source).getLeadingJoint();
				JIDX targetLeadingJoint = partDrivers.get(target).getLeadingJoint();
				PVector referencePoint = mainReferencePoint();
				PVector[] sourceRefFrame = referenceFrameForPartType(source);
				PVector sourceDelta = PVector.sub(body.get(sourceLeadingJoint), referencePoint);
				sourceReferenceDissolution = PVecUtilities.dissolve(sourceDelta, sourceRefFrame);
				
				if (LIMB.isArm(source) == LIMB.isArm(target)) {
					targetReferenceDissolution = new float[3];
					System.arraycopy(sourceReferenceDissolution, 0, targetReferenceDissolution, 0, 3);
					targetReferenceDissolution[0] = -targetReferenceDissolution[0];
				} else {
					PVector[] targetRefFrame = referenceFrameForPartType(target);
					PVector targetDelta = PVector.sub(body.get(targetLeadingJoint), referencePoint);
					targetReferenceDissolution = PVecUtilities.dissolve(targetDelta, targetRefFrame);
				}
			}
		}
		
		public LIMB source;
		public LIMB target;
		public float[] sourceReferenceDissolution = null;
		public float[] targetReferenceDissolution = null;
	}
}
