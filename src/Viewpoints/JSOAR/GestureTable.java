
package Viewpoints.JSOAR;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;

import Viewpoints.Shared.Body;
import Viewpoints.Shared.JointSpaceGesture;

/**
 * ===================
 * Gesture Table Class
 * ===================
 * 
 * Stores gestures based on JointSpaceGesture.java
 *
 */
public class GestureTable {
	
	/**
	 * Gesture Table Data
	 * ID -> Gesture (Integer -> Body)
	 */
	private LinkedHashMap<UUID, ArrayList<LinkedHashMap<Integer, Body>>> gestureTable;
	private Integer IDCountSoFar;
	private Boolean initialized = false;
	// Gav TODO: make gesture table persist between sessions; save and load from file
//			A: TODO
	
	/**
	 * Constructor
	 * Called automatically upon creation of class object
	 */
	public GestureTable() {
		if(!this.initialized) {
			initializeGestureTable();
			this.initialized = true;
		}
	}
	
	/**
	 * Initialize Gesture Table
	 * This should only get called once upon class object creation
	 * Private: called by constructor
	 */
	private void initializeGestureTable() {
		this.gestureTable = new LinkedHashMap<UUID, ArrayList<LinkedHashMap<Integer, Body>>>();
		this.IDCountSoFar = 0;
	}
	
	/**
	 * Add new gesture to the gesture table
	 * Called each time a new gesture is to be added to the table
	 */
	public void addGesture(UUID ID, JointSpaceGesture gesture) {
		if(this.initialized && gesture != null) {
			ArrayList<LinkedHashMap<Integer, Body>> temp = this.gestureTable.get(ID);
			if(temp == null) {
				temp = new ArrayList<LinkedHashMap<Integer,Body>>();
			}
			temp.add(gesture.getGesture());
			this.gestureTable.put(ID, temp);
			this.IDCountSoFar++;
		}
	}
	
	/**
	 * Get/Set Functions
	 * Add all future get/set functions to this group below to maintain consistency 
	 */
	public Integer getGestureCount() {
		return this.IDCountSoFar;
	}
	
	public LinkedHashMap<UUID, ArrayList<LinkedHashMap<Integer, Body>>> getGestureTable() {
		return this.gestureTable;
	}
	
	public LinkedHashMap<Integer, Body> getGesture(UUID uniqueID) {
		//Returns the latest gesture with that UUID
		return this.gestureTable.get(uniqueID).get(this.gestureTable.get(uniqueID).size() - 1);
	}
	
	public JointSpaceGesture getJointSpaceGesture(UUID uniqueID) {
		//Returns the latest gesture with that UUID
		return new JointSpaceGesture(this.gestureTable.get(uniqueID).get(this.gestureTable.get(uniqueID).size() - 1));
	}
}
