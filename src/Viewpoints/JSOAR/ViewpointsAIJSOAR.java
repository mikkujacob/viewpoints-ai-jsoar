package Viewpoints.JSOAR;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Callable;

import org.jsoar.kernel.Agent;
import org.jsoar.kernel.SoarException;
import org.jsoar.runtime.ThreadedAgent;
import org.jsoar.util.adaptables.Adaptables;
import org.jsoar.util.commands.SoarCommands;
import org.jsoar.util.events.SoarEvent;
import org.jsoar.util.events.SoarEventListener;
import org.jsoar.kernel.events.InputEvent;
import org.jsoar.kernel.events.OutputEvent;
import org.jsoar.kernel.io.InputOutput;
import org.jsoar.kernel.io.InputWme;
import org.jsoar.kernel.io.InputWmes;
import org.jsoar.kernel.memory.Wme;
import org.jsoar.kernel.symbols.Identifier;
import org.jsoar.kernel.symbols.SymbolFactory;

import ObjectiveParameters.ObjectiveParametersMetric;
import ObjectiveParameters.TargetKeyset;
import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Gesture.FramePredicates;
import Viewpoints.Gesture.Gesture;
import Viewpoints.Gesture.Predicates;
import Viewpoints.Gesture.Predicates.EMOTION;
import Viewpoints.Gesture.TransformPredicates;
import Viewpoints.Gesture.TransformPredicates.*;
import Viewpoints.Gesture.Predicates.*;
import Viewpoints.Shared.JointSpaceGesture;

/**
 * Java environment that encapsulates the JSOAR reasoning module, gesture library, gesture recognition, etc.
 * 
 * @author mjacob6
 *
 */
public class ViewpointsAIJSOAR
{

	private Boolean debugPrintEnabled;
	
	public static String path = "soar_rules/";
	public static String goldenAngryGesturesPath = "training-gestures/angry-golden/";
	public static String goldenFearGesturesPath = "training-gestures/fear-golden/";
	public static String goldenSadGesturesPath = "training-gestures/sad-golden/";
	public static String goldenJoyGesturesPath = "training-gestures/joy-golden/";
	
	//Enable to true to open JSOAR Debugger at runtime
	//Disable to false for normal execution
	public final Boolean DEBUGMODE = false;
	
	//Enable to cheat at Viewpoints AI with Golden gestures
	//Output emotional responses from training_gestures golden gestures instead of from history
	public final Boolean CHEATWITHGOLDMODE = true;
	
	
	public Gesture inputGesture;
	public Gesture outputGesture;
	public Boolean isInputDirty;
	public Boolean isOutputDirty;
	
	public ArrayList<TransformPredicateInterface> transformsList;
	public Boolean isTransformsListDirty;
	
	public RESPONSE_MODE responseMode;
	
	public JointSpaceGesture inputJSGesture;
	public JointSpaceGesture outputJSGesture;
	public Boolean isInputJSGDirty;
	public Boolean isOutputJSGDirty;
	
	public GestureTable gestureTable;
	
	public Agent SOARAgent;
	public ThreadedAgent ThreadedSOARAgent;
	Boolean isSOARFinished;
	
	private ObjectiveParametersMetric objectiveParameterMetric = new ObjectiveParametersMetric(TargetKeyset.getBasicSimilarityKeyset());
	
	UUID currentUUID;
	Boolean isCurrentUUIDDirty;
	UUID closestUUID;

	public InputWme gestureIdWme;
	
	public final String JSOARPredicatesInPath = "file_communications/KinectPredicatesToJSOAR.txt";
	public final String JSOARJointsInPath = "file_communications/KinectJointsToJSOAR.txt";
	public final String JSOARPredicatesOutPath = "file_communications/JSOARPredicatesToVisualization.txt";
	public final String JSOARJointsOutPath = "file_communications/JSOARJointsToVisualization.txt";
	
	public String statusDir = "status";
	public final String reasoningStatusFileName = "ReasoningStatus.txt";
	public final String reasoningAgentStatusFileName = "ReasoningAgentStatus.txt";
	public String PROJECT_HOME;
	
	private long reasoningCounter = 10;
	private long reasoningAgentCounter = 10;
	private long iterationCounter = 0;
	
	public ViewpointsAIJSOAR()
	{
		debugPrintEnabled = true; // Set this to false for production / demo days
		
		PROJECT_HOME = System.getProperty("user.dir");
		if(PROJECT_HOME.equalsIgnoreCase(null))
		{
			try
			{
				PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			
			if(PROJECT_HOME.equalsIgnoreCase(null))
			{
				System.out.println("ERROR! PROJECT_HOME IS NULL");
			}
		}
		
		statusDir = PROJECT_HOME + File.separator + statusDir;
		
		initialiseAgent();
	}
	
	/*
	 * Debug Messages control method. Allows us to leave debug lines in code and
	 * turn off messages with just one Boolean flag (see constructor)
	 */
	public void debugPrint(Object message) {
		if(this.debugPrintEnabled == null) {
			System.out.println("* Debug Print Error! Private variable not initialized, please check constructor. *");
		} else if(this.debugPrintEnabled) {
			System.out.println("> " + message.toString());
		}
	}

	public static void main(String[] args) throws SoarException, InterruptedException
	{
		ViewpointsAIJSOAR runner = new ViewpointsAIJSOAR();
		
//		Boolean isMultiFrame = false;
//
//		//Gesture 1
//
//		System.out.println("\n*****Example Gesture 1*****\n");
//
//		DURATION avgDuration = DURATION.MEDIUM;
//		TEMPO avgTempo = TEMPO.EXTREMELY_FAST;
//		SMOOTHNESS avgSmoothness = SMOOTHNESS.SMOOTH;
//		ENERGY avgEnergy = ENERGY.EXTREMELY_HIGH;
//
//		ArrayList<FramePredicates> All_Predicate_History = new ArrayList<FramePredicates>();
//
//		All_Predicate_History.add(new FramePredicates(TEMPO.EXTREMELY_FAST, SMOOTHNESS.SMOOTH, ENERGY.EXTREMELY_HIGH, 
//				LEFTARM_CURVE.BENT, LEFTHAND_POS.OUT, LEFTHAND_HEIGHT.AT_MOUTH, RIGHTARM_CURVE.STRAIGHT, 
//				RIGHTHAND_POS.IN, RIGHTHAND_HEIGHT.DOWN, BOTHARM_CURVE.BENT, LEFTLEG_CURVE.STRAIGHT, RIGHTLEG_CURVE.STRAIGHT, 
//				HANDS_TOGETHER.APART, ARMS_CROSSED.NOT_CROSSED, BOTH_HANDS_BY_CHEST.NOT_BY_CHEST, 
//				BODY_SYMMETRIC.ASYMMETRIC, FACING.SLIGHT_LEFT, HEIGHT.TALL, QUADRANT.BOTTOM_RIGHT, DIST_CENTER.MEDIUM, SIZE.LARGE));
//
//		Gesture inputGesture = new Gesture(isMultiFrame, avgDuration, avgTempo, avgSmoothness, avgEnergy, All_Predicate_History);
//
//		runner.runAgent(inputGesture);
//
//		//Gesture 2
//
//		System.out.println("\n*****Example Gesture 2*****\n");
//
//		avgDuration = DURATION.MEDIUM;
//		avgTempo = TEMPO.EXTREMELY_FAST;
//		avgSmoothness = SMOOTHNESS.SMOOTH;
//		avgEnergy = ENERGY.EXTREMELY_HIGH;
//
//		All_Predicate_History = new ArrayList<FramePredicates>();
//
//		All_Predicate_History.add(new FramePredicates(TEMPO.EXTREMELY_FAST, SMOOTHNESS.SMOOTH, ENERGY.EXTREMELY_HIGH, 
//				LEFTARM_CURVE.BENT, LEFTHAND_POS.OUT, LEFTHAND_HEIGHT.AT_MOUTH, RIGHTARM_CURVE.STRAIGHT, 
//				RIGHTHAND_POS.IN, RIGHTHAND_HEIGHT.DOWN, BOTHARM_CURVE.BENT, LEFTLEG_CURVE.STRAIGHT, RIGHTLEG_CURVE.STRAIGHT, 
//				HANDS_TOGETHER.APART, ARMS_CROSSED.NOT_CROSSED, BOTH_HANDS_BY_CHEST.NOT_BY_CHEST, 
//				BODY_SYMMETRIC.ASYMMETRIC, FACING.SLIGHT_LEFT, HEIGHT.TALL, QUADRANT.BOTTOM_RIGHT, DIST_CENTER.MEDIUM, SIZE.LARGE));
//
//		All_Predicate_History.add(new FramePredicates(TEMPO.FAST, SMOOTHNESS.STACCATO, ENERGY.LOW, 
//				LEFTARM_CURVE.BENT, LEFTHAND_POS.IN, LEFTHAND_HEIGHT.DOWN, RIGHTARM_CURVE.STRAIGHT, 
//				RIGHTHAND_POS.IN, RIGHTHAND_HEIGHT.UP, BOTHARM_CURVE.BENT, LEFTLEG_CURVE.STRAIGHT, RIGHTLEG_CURVE.STRAIGHT, 
//				HANDS_TOGETHER.APART, ARMS_CROSSED.NOT_CROSSED, BOTH_HANDS_BY_CHEST.NOT_BY_CHEST, 
//				BODY_SYMMETRIC.SYMMETRIC, FACING.SLIGHT_RIGHT, HEIGHT.SHORT, QUADRANT.BOTTOM_LEFT, DIST_CENTER.FAR, SIZE.LARGE));
//
//		inputGesture = new Gesture(isMultiFrame, avgDuration, avgTempo, avgSmoothness, avgEnergy, All_Predicate_History);
//
//		runner.runAgent(inputGesture);
		
		while(true)
		{
			runner.update();
			
			Thread.sleep(500);
			
			runner.writeHeartBeat();
		}
	}
	
	private void writeHeartBeat()
	{
		if(iterationCounter % 10 == 0)
		{
			FileUtilities.writeToFile(this.statusDir + File.separator + reasoningStatusFileName, reasoningCounter + "");
			++reasoningCounter;
			if(reasoningCounter < Long.MAX_VALUE && reasoningCounter > Long.MAX_VALUE - 100)
			{
				reasoningCounter = 10;
			}
//			System.out.println(">>>Reasoning Iteration: " + iterationCounter + "<<<<");
		}
		++iterationCounter;
		if(iterationCounter < Long.MAX_VALUE && iterationCounter > Long.MAX_VALUE - 50)
		{
			iterationCounter = 0;
		}
		
	}
	
	private void writeAgentHeartBeat(Boolean isMonitorable)
	{
		if(isMonitorable)
		{
			if(iterationCounter % 10 == 0)
			{
				FileUtilities.writeToFile(this.statusDir + File.separator + reasoningAgentStatusFileName, reasoningAgentCounter + "");
				++reasoningAgentCounter;
				if(reasoningAgentCounter < Long.MAX_VALUE && reasoningAgentCounter > Long.MAX_VALUE - 100)
				{
					reasoningAgentCounter = 10;
				}
//				System.out.println(">>>ReasoningAgent Iteration: " + iterationCounter + "<<<<");
			}
		}
		else
		{
			FileUtilities.writeToFile(this.statusDir + File.separator + reasoningAgentStatusFileName, -666 + "");
//			System.out.println(">>>ReasoningAgent Iteration: " + iterationCounter + "<<<<");
		}
		++iterationCounter;
		if(iterationCounter < Long.MAX_VALUE && iterationCounter > Long.MAX_VALUE - 50)
		{
			iterationCounter = 0;
		}
		
	}
	
	public void update()
	{
//		Gesture inputGesture = FileUtilities.readFromText(this.JSOARPredicatesInPath);
		Gesture inputGesture = FileUtilities.deserializeGesture(JSOARPredicatesInPath);
		JointSpaceGesture inputJSGesture = FileUtilities.deserializeJointsGesture(JSOARJointsInPath);
		
		if(inputGesture != null && inputJSGesture != null)
		{
//			debugPrint("Input Gesture: " + inputGesture);
//			debugPrint("Input Joint Space Gesture: " + inputJSGesture.toString());
			
			FileUtilities.clearText(this.JSOARPredicatesInPath);
			FileUtilities.clearText(this.JSOARJointsInPath);
			
			runAgent(inputGesture, inputJSGesture);
			
			debugPrint("**************************************************************************\nSOAR Finished Processing One Interaction\n**************************************************************************");
		}
	}

	public void initialiseAgent()
	{
		ThreadedSOARAgent = ThreadedAgent.create();
		ThreadedSOARAgent.setName("JSOAR Agent");
		ThreadedSOARAgent.getPrinter().pushWriter(new OutputStreamWriter(System.out));

		inputGesture = new Gesture();
		isInputDirty = false;
		outputGesture = new Gesture();
		isOutputDirty = false;
		
		inputJSGesture = new JointSpaceGesture();
		isInputJSGDirty = false;
		outputJSGesture = new JointSpaceGesture();
		isOutputJSGDirty = false;
		
		gestureTable = new GestureTable();
		
		transformsList = new ArrayList<TransformPredicateInterface>();
		isTransformsListDirty = false;
		
		responseMode = RESPONSE_MODE.UNSET;
		
		isSOARFinished = false;
		
		final Agent tempAgent = ThreadedSOARAgent.getAgent();
		//Register Custom RHS Function for selecting best output gesture from a matched pattern
		//with multiple possible output gestures
		tempAgent.getRhsFunctions().registerHandler(new SelectBestOutputRhsFunction());
		tempAgent.getRhsFunctions().registerHandler(new SelectBestEmotionOutput());
		tempAgent.getRhsFunctions().registerHandler(new SelectOutputByUUID());
		
		ThreadedSOARAgent.execute(new Callable<Void>()
				{
		    public Void call() throws Exception
		    {
		       loadSOARRules(tempAgent);
		       
		       return null;
		    }
		}, null);

		//ThreadedAgent doesn't need initialize() call
//		SOARAgent.initialize();

//		SOARAgent.getEvents().addListener(InputEvent.class, new SoarEventListener() {
		ThreadedSOARAgent.getEvents().addListener(InputEvent.class, new SoarEventListener() {

			@Override
			public void onEvent(SoarEvent event)
			{
				InputEvent ie = (InputEvent) event;

				//System.out.println("\nInput Event");

				initialiseInputLink(ie);
				
				writeAgentHeartBeat(true);
			}});

//		SOARAgent.getEvents().addListener(OutputEvent.class, new SoarEventListener() {
		ThreadedSOARAgent.getEvents().addListener(OutputEvent.class, new SoarEventListener() {

			@Override
			public void onEvent(SoarEvent event)
			{
				OutputEvent oe = (OutputEvent) event;
				
				//System.out.println("\nOutput Event - Mode " + oe.getMode().name().toString());
				
				if(oe.getMode() == OutputEvent.OutputMode.MODIFIED_OUTPUT_COMMAND)
				{
					//System.out.println("\nAdded Output Command");
					handleOutputEvents(oe);
				}
				
				writeAgentHeartBeat(true);
			}});
		
//		ENABLE WHILE TESTING OR IF YOU NEED SOME GESTURES PRELOADED BEFORE EXECUTION
//		objectiveParameterMetric.load();
		
		writeAgentHeartBeat(false);
	}
	
	public void loadSOARRules(Agent agent)
	{
		String ruleFile;
		File ruleFolder = new File(path);
		File[] ruleFileList = ruleFolder.listFiles(); 

		for (int i = 0; i < ruleFileList.length; i++) 
		{
			if (ruleFileList[i].isFile()) 
			{
				ruleFile = ruleFileList[i].getName();
				if (ruleFile.toLowerCase().endsWith(".soar"))
				{
					System.out.println("Imported SOAR Rule File: " + path + ruleFile);

					try
					{
						SoarCommands.source(agent.getInterpreter(), path + ruleFile);
					}
					catch (SoarException e)
					{
						System.out.println("ERROR: SOAR RULE LOADING SCREWED UP\n");

						e.printStackTrace();
					}
				}
			}
		}

		if(ruleFileList.length == 0)
		{
			System.out.println("No SOAR Rule Files In Folder: " + path);
		}
		
		writeAgentHeartBeat(true);
	}

	public void runAgent(Gesture inputGesture, JointSpaceGesture inputJSGesture)
	{
		this.inputGesture = inputGesture;
		this.isInputDirty = true;
		
		this.inputJSGesture = inputJSGesture;
		this.isInputJSGDirty = true;
		
		debugPrint("Size of inputJSGesture = " + inputJSGesture.getGesture().size());
		UUID uniqueID = UUID.randomUUID();
		JointSpaceGesture closestGesture = objectiveParameterMetric.selectClosest(inputJSGesture, -1000f);
		
		if(closestGesture != null)
		{
			uniqueID = closestGesture.getGestureUUID();
		}
		else
		{
			//Mikhail / Ivan TODO: Memory fall off / forgetting for objective parameters gesture library 
			objectiveParameterMetric.memorizeLastGesture(uniqueID);
		}
		
		JointSpaceGesture closestGestureUnbounded = objectiveParameterMetric.selectClosest(inputJSGesture);
		if(closestGestureUnbounded != null)
		{
			this.closestUUID = closestGestureUnbounded.getGestureUUID();
		}
		else
		{
			this.closestUUID = uniqueID;
		}
		
		this.gestureTable.addGesture(uniqueID, inputJSGesture);
		debugPrint("Number of gestures in Gesture Table: " + this.gestureTable.getGestureCount());
		
		this.currentUUID = uniqueID;
		this.isCurrentUUIDDirty = true;
		
		debugPrint("Gesture Library Returned UUID: " + uniqueID);
		
		ThreadedSOARAgent.runForever();
		
//		debugPrint("About to sleep");
		
		while(!isSOARFinished)
		{
			try
			{
				Thread.sleep(500);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		
//		debugPrint("Main Thread wake from sleep");
		
		sendOutputToVisualization();
		
		responseMode = RESPONSE_MODE.UNSET;
		
		isSOARFinished = false;
	}

	public void disposeAgent()
	{
		ThreadedSOARAgent.dispose();
	}
	
	public void setOutputGesture(UUID uniqueID)
	{
		this.outputJSGesture = this.gestureTable.getJointSpaceGesture(uniqueID);
		this.isOutputJSGDirty = true;
	}
	
	public void setCheatingOutputGesture(String emotionString)
	{
		String gesturePath = ""; 
		if(emotionString.equalsIgnoreCase("JOY"))
		{
			gesturePath = goldenJoyGesturesPath;
		}
		else if(emotionString.equalsIgnoreCase("ANGRY"))
		{
			gesturePath = goldenAngryGesturesPath;
		}
		else if(emotionString.equalsIgnoreCase("FEAR"))
		{
			gesturePath = goldenFearGesturesPath;
		}
		else if(emotionString.equalsIgnoreCase("SAD"))
		{
			gesturePath = goldenSadGesturesPath;
		}
		else
		{
			//Somehow MAGICALLY a NONE emotion sneaked in here.
//			System.out.println("ERROR: NONE EMOTION SNEAKED IN HERE...");
			
			if(this.isCurrentUUIDDirty)
			{
				debugPrint("JSOAR Returned UUID: " + this.currentUUID);
				setOutputGesture(this.currentUUID);
			}
		}
		
		this.outputJSGesture = cheatWithGold(gesturePath);
		
		if(null != this.outputJSGesture)
		{
			if(this.outputJSGesture.getGestureUUID() == null)
			{
				if(this.isCurrentUUIDDirty)
				{
					this.outputJSGesture.setGestureUUID(this.currentUUID);
//					System.out.println("Set Cheating Gesture to Current UUID: " + this.currentUUID);
				}
				else
				{
					this.outputJSGesture.setGestureUUID(UUID.randomUUID());
//					System.out.println("Set Cheating Gesture to random UUID: " + this.outputJSGesture.getGestureUUID());
				}
			}
			debugPrint("JSOAR Cheated With Gesture UUID: " + this.outputJSGesture.getGestureUUID());
			this.isOutputJSGDirty = true;
		}
		else
		{
			//ERROR: Couldn't read in the gesture from training-gestures/<EMOTION>-golden
//			System.out.println("ERROR: Couldn't read in the gesture from " + gesturePath + "...");
			
			if(this.isCurrentUUIDDirty)
			{
				debugPrint("JSOAR Returned UUID: " + this.currentUUID);
				setOutputGesture(this.currentUUID);
			}
		}
	}

	public void sendOutputToVisualization()
	{
		if(!this.isOutputDirty || !this.isOutputJSGDirty)
		{
			//System.out.println("\n\nNo new Output Gesture");

			return;
		}
		
		//System.out.println("\n\nNew Output Gesture\n\n");// + this.outputGesture.outString());
		
		//TODO - FOR TESTING EMOTIONAL RESPONSES ONLY!!! REMOVE / COMMENT AFTER TESTING!!!
//		this.outputGesture.setEmotion(getRandomEmotionSequence());

//		FileUtilities.writeToText(this.JSOARPredicatesOutPath, this.outputGesture);
		FileUtilities.serializeGesture(this.JSOARPredicatesOutPath, this.outputGesture);
		
		FileUtilities.serializeJointsGesture(JSOARJointsOutPath, this.outputJSGesture);
		
		this.isOutputDirty = false;
		this.isOutputJSGDirty = false;
	}
	
	public EMOTION getRandomEmotion(EMOTION emotion)
	{
		if(emotion != null)
		{
			return emotion;
		}
			
		Random rand = new Random();
		
		return Predicates.EMOTION.values()[rand.nextInt(Predicates.EMOTION.values().length)];
	}
	
	private int emotionCounter = 0;
	private EMOTION testEmotion;
	public EMOTION getRandomEmotionSequence()
	{
		if((emotionCounter) % 8 == 0)
		{
			testEmotion = EMOTION.ANGRY;
		}
		else if((emotionCounter) % 8 == 1)
		{
			testEmotion = EMOTION.NONE;
		}
		else if((emotionCounter) % 8 == 2)
		{
			testEmotion = EMOTION.FEAR;
		}
		else if((emotionCounter) % 8 == 3)
		{
			testEmotion = EMOTION.NONE;
		}
		else if((emotionCounter) % 8 == 4)
		{
			testEmotion = EMOTION.JOY;
		}
		else if((emotionCounter) % 8 == 5)
		{
			testEmotion = EMOTION.NONE;
		}
		else if((emotionCounter) % 8 == 6)
		{
			testEmotion = EMOTION.SAD;
		}
		else if((emotionCounter) % 8 == 7)
		{
			testEmotion = EMOTION.NONE;
		}
		++emotionCounter;
		return testEmotion;
	}

	public void initialiseInputLink(InputEvent ie)
	{
		InputOutput io = ie.getInputOutput();
		SymbolFactory symbols = io.getSymbols();

		if(!this.isInputDirty)
		{
			//System.out.println("\n\nNo new Input Gesture");

			//printWmeTree(io.getInputLink());

			return;
		}
		
		clearWmeTree(io.getInputLink());

		//System.out.println("\n\nNew Input Gesture");

		/* InputLink Structure
		 * 	  ^io
		 * 		^ilink
		 * 			^gesture
		 * 				^avgduration
		 * 				^avgtempo
		 * 				^avgsmoothness
		 * 				^avgenergy
		 * 				^emotion
		 * 				^closestuuid
		 * 				^all_predicate_history
		 *    				^multiframe
		 * 					^node
		 * 						^index
		 * 						^multiframeduration
		 * 						^tempo
		 *  					^smoothness
		 *   					^energy
		 *    					^leftarm_curve
		 *    					^lefthand_pos
		 *    					^lefthand_height
		 *    					^rightarm_curve
		 *    					^righthand_pos
		 *    					^righthand_height
		 *    					^botharm_curve
		 *    					^leftleg_curve
		 *    					^rightleg_curve
		 *    					^hands_together
		 *    					^arms_crossed
		 *    					^both_hands_by_chest
		 *    					^body_symmetric
		 *    					^facing
		 *    					^height
		 *    					^quadrant
		 *    					^dist_center
		 *    					^size
		 * */

		Identifier gestureId = symbols.createIdentifier('g');
		this.gestureIdWme = InputWmes.add(io, "gesture", gestureId);
		
		if(this.isCurrentUUIDDirty)
		{
			InputWmes.add(gestureIdWme, "uuid", this.currentUUID.toString());
			this.isCurrentUUIDDirty = false;
		}
		InputWmes.add(gestureIdWme, "avgduration", inputGesture.getAvgDuration().name());
		InputWmes.add(gestureIdWme, "avgtempo", inputGesture.getAvgTempo().name());
		InputWmes.add(gestureIdWme, "avgleftlegstillness", inputGesture.getAvgLeftLegStillness().name());
		InputWmes.add(gestureIdWme, "avgrightlegstillness", inputGesture.getAvgRightLegStillness().name());
		InputWmes.add(gestureIdWme, "avglefthandstillness", inputGesture.getAvgLeftHandStillness().name());
		InputWmes.add(gestureIdWme, "avgrighthandstillness", inputGesture.getAvgRightHandStillness().name());
		InputWmes.add(gestureIdWme, "avgleftlegtransverse", inputGesture.getAvgLeftLegTransverse().name());
		InputWmes.add(gestureIdWme, "avgrightlegtransverse", inputGesture.getAvgRightLegTransverse().name());
		InputWmes.add(gestureIdWme, "avglefthandtransverse", inputGesture.getAvgLeftHandTransverse().name());
		InputWmes.add(gestureIdWme, "avgrighthandtransverse", inputGesture.getAvgRightHandTransverse().name());
		InputWmes.add(gestureIdWme, "avgleftleglongitudinal", inputGesture.getAvgLeftLegLongitudinal().name());
		InputWmes.add(gestureIdWme, "avgrightleglongitudinal", inputGesture.getAvgRightLegLongitudinal().name());
		InputWmes.add(gestureIdWme, "avglefthandlongitudinal", inputGesture.getAvgLeftHandLongitudinal().name());
		InputWmes.add(gestureIdWme, "avgrighthandlongitudinal", inputGesture.getAvgRightHandLongitudinal().name());
		InputWmes.add(gestureIdWme, "avgleftlegvertical", inputGesture.getAvgLeftLegVertical().name());
		InputWmes.add(gestureIdWme, "avgrightlegvertical", inputGesture.getAvgRightLegVertical().name());
		InputWmes.add(gestureIdWme, "avglefthandvertical", inputGesture.getAvgLeftHandVertical().name());
		InputWmes.add(gestureIdWme, "avgrighthandvertical", inputGesture.getAvgRightHandVertical().name());
		InputWmes.add(gestureIdWme, "avgsmoothness", inputGesture.getAvgSmoothness().name());
		InputWmes.add(gestureIdWme, "avgenergy", inputGesture.getAvgEnergy().name());
		InputWmes.add(gestureIdWme, "emotion", inputGesture.getEmotion().name());
//		System.out.println("Soar InputLink Got Emotion: " + inputGesture.getEmotion().name());

		//STUB FOR TESTING EMOTIONS WITHOUT PYTHON ONLY!!!
		//PLEASE REMOVE / COMMENT LATER...
//		String emotion = getRandomEmotion(null).name();
//		InputWmes.add(gestureIdWme, "emotion", emotion);
//		System.out.println("Soar InputLink Got Emotion: " + emotion);
		
		InputWmes.add(gestureIdWme, "closestuuid", this.closestUUID.toString());
		
		//Start code
		int nlegsmoving = 0;
		int narmsmoving = 0;
		int nlimbsmoving = 0;
		int narmsvertical = 0;
		int nlegsvertical = 0;
		int nlimbsvertical = 0;
		int narmstransversal = 0;
		int nlegstransversal = 0;
		int nlimbstransversal = 0;
		int narmslongitudinal = 0;
		int nlegslongitudinal = 0;
		int nlimbslongitudinal = 0;
		
		if(inputGesture.getAvgLeftLegStillness() == LEFT_LEG_STILL.FALSE)
		{
			nlegsmoving++;
			nlimbsmoving++;
			
			if(inputGesture.getAvgLeftLegTransverse() == LEFT_LEG_TRANSVERSE.TRUE)
			{
				nlegstransversal++;
				nlimbstransversal++;
			}
			
			if(inputGesture.getAvgLeftLegLongitudinal() == LEFT_LEG_LONGITUDINAL.TRUE)
			{
				nlegslongitudinal++;
				nlimbslongitudinal++;
			}
			
			if(inputGesture.getAvgLeftLegVertical() == LEFT_LEG_VERTICAL.TRUE)
			{
				nlegsvertical++;
				nlimbsvertical++;
			}
		}
		if(inputGesture.getAvgRightLegStillness() == RIGHT_LEG_STILL.FALSE)
		{
			nlegsmoving++;
			nlimbsmoving++;
			
			if(inputGesture.getAvgRightLegTransverse() == RIGHT_LEG_TRANSVERSE.TRUE)
			{
				nlegstransversal++;
				nlimbstransversal++;
			}
			
			if(inputGesture.getAvgRightLegLongitudinal() == RIGHT_LEG_LONGITUDINAL.TRUE)
			{
				nlegslongitudinal++;
				nlimbslongitudinal++;
			}
			
			if(inputGesture.getAvgRightLegVertical() == RIGHT_LEG_VERTICAL.TRUE)
			{
				nlegsvertical++;
				nlimbsvertical++;
			}
		}
		if(inputGesture.getAvgLeftHandStillness() == LEFT_HAND_STILL.FALSE)
		{
			narmsmoving++;
			nlimbsmoving++;
			
			if(inputGesture.getAvgLeftHandTransverse() == LEFT_HAND_TRANSVERSE.TRUE)
			{
				narmstransversal++;
				nlimbstransversal++;
			}
			
			if(inputGesture.getAvgLeftHandLongitudinal() == LEFT_HAND_LONGITUDINAL.TRUE)
			{
				narmslongitudinal++;
				nlimbslongitudinal++;
			}
			
			if(inputGesture.getAvgLeftHandVertical() == LEFT_HAND_VERTICAL.TRUE)
			{
				narmsvertical++;
				nlimbsvertical++;
			}
		}
		if(inputGesture.getAvgRightHandStillness() == RIGHT_HAND_STILL.FALSE)
		{
			narmsmoving++;
			nlimbsmoving++;
			
			if(inputGesture.getAvgRightHandTransverse() == RIGHT_HAND_TRANSVERSE.TRUE)
			{
				narmstransversal++;
				nlimbstransversal++;
			}
			
			if(inputGesture.getAvgRightHandLongitudinal() == RIGHT_HAND_LONGITUDINAL.TRUE)
			{
				narmslongitudinal++;
				nlimbslongitudinal++;
			}
			
			if(inputGesture.getAvgRightHandVertical() == RIGHT_HAND_VERTICAL.TRUE)
			{
				narmsvertical++;
				nlimbsvertical++;
			}
		}
		
//		debugPrint("DEBUG NLIMBSMOVING:");
//		debugPrint("narmsmoving = " + narmsmoving);
//		debugPrint("nlegsmoving = " + nlegsmoving);
//		debugPrint("nlimbsmoving = " + nlimbsmoving);
//		debugPrint("narmstransversal = " + narmstransversal);
//		debugPrint("nlegstransversal = " + nlegstransversal);
//		debugPrint("nlimbstransversal = " + nlimbstransversal);
//		debugPrint("narmsvertical = " + narmsvertical);
//		debugPrint("nlegsvertical = " + nlegsvertical);
//		debugPrint("nlimbsvertical = " + nlimbsvertical);
//		debugPrint("narmslongitudinal = " + narmslongitudinal);
//		debugPrint("nlegslongitudinal = " + nlegslongitudinal);
//		debugPrint("nlimbslongitudinal = " + nlimbslongitudinal);
//		debugPrint("");
		
		FACING avgfacing = calcAvgFacing(inputGesture.All_Predicate_History);
		
		InputWmes.add(gestureIdWme, "nlegsmoving", nlegsmoving);
		InputWmes.add(gestureIdWme, "narmsmoving", narmsmoving);
		InputWmes.add(gestureIdWme, "nlimbsmoving", nlimbsmoving);
		InputWmes.add(gestureIdWme, "nlegstransversal", nlegstransversal);
		InputWmes.add(gestureIdWme, "narmstransversal", narmstransversal);
		InputWmes.add(gestureIdWme, "nlimbstransversal", nlimbstransversal);
		InputWmes.add(gestureIdWme, "nlegsvertical", nlegsvertical);
		InputWmes.add(gestureIdWme, "narmsvertical", narmsvertical);
		InputWmes.add(gestureIdWme, "nlimbsvertical", nlimbsvertical);
		InputWmes.add(gestureIdWme, "narmslongitudinal", narmslongitudinal);
		InputWmes.add(gestureIdWme, "nlegslongitudinal", nlegslongitudinal);
		InputWmes.add(gestureIdWme, "nlimbslongitudinal", nlimbslongitudinal);
		InputWmes.add(gestureIdWme, "avgfacing", avgfacing.name());

//		debugPrint("DEBUG AVGFACING:");
//		debugPrint("avgfacing = " + avgfacing.name());
		
		Identifier AllPredHistId = symbols.createIdentifier('h');
		InputWme AllPredHistIdWme = InputWmes.add(gestureIdWme, "all_predicate_history", AllPredHistId);
		InputWmes.add(AllPredHistIdWme, "multiframe", inputGesture.isMultiFrame.toString().toUpperCase());

		for(int index = 0; index < inputGesture.All_Predicate_History.size(); index++)
		{
			FramePredicates frame = inputGesture.All_Predicate_History.get(index);

			Identifier nodeid = symbols.createIdentifier('n');
			InputWme nodeIdWme = InputWmes.add(AllPredHistIdWme, "node", nodeid);

			InputWmes.add(nodeIdWme, "index", index);
			InputWmes.add(nodeIdWme, "multiframeduration", frame.multiFrameDuration.name());
			InputWmes.add(nodeIdWme, "tempo", frame.frameTempo.name());
			InputWmes.add(nodeIdWme, "smoothness", frame.frameSmoothness.name());
			InputWmes.add(nodeIdWme, "energy", frame.frameEnergy.name());
			InputWmes.add(nodeIdWme, "leftarm_curve", frame.frameLeftArmCurve.name());
			InputWmes.add(nodeIdWme, "lefthand_pos", frame.frameLeftHandPos.name());
			InputWmes.add(nodeIdWme, "lefthand_height", frame.frameLeftHandHeight.name());
			InputWmes.add(nodeIdWme, "rightarm_curve", frame.frameRightArmCurve.name());
			InputWmes.add(nodeIdWme, "righthand_pos", frame.frameRightHandPos.name());
			InputWmes.add(nodeIdWme, "righthand_height", frame.frameRightHandHeight.name());
			InputWmes.add(nodeIdWme, "botharm_curve", frame.frameBothArmCurve.name());
			InputWmes.add(nodeIdWme, "leftleg_curve", frame.frameLeftLegCurve.name());
			InputWmes.add(nodeIdWme, "rightleg_curve", frame.frameRightLegCurve.name());
			InputWmes.add(nodeIdWme, "hands_together", frame.frameHandsTogether.name());
			InputWmes.add(nodeIdWme, "arms_crossed", frame.frameArmsCrossed.name());
			InputWmes.add(nodeIdWme, "both_hands_by_chest", frame.frameBothHandsByChest.name());
			InputWmes.add(nodeIdWme, "body_symmetric", frame.frameBodySymmetric.name());
			InputWmes.add(nodeIdWme, "facing", frame.frameFacing.name());
			InputWmes.add(nodeIdWme, "height", frame.frameHeight.name());
			InputWmes.add(nodeIdWme, "quadrant", frame.frameQuadrant.name());
			InputWmes.add(nodeIdWme, "dist_center", frame.frameDistCenter.name());
			InputWmes.add(nodeIdWme, "size", frame.frameSize.name());
		}

		this.isInputDirty = false;
		
		if(DEBUGMODE)
		{
			//System.out.println("Open Debugger");

			final Agent tempAgent = ThreadedSOARAgent.getAgent();
			ThreadedSOARAgent.execute(new Callable<Void>()
					{
				public Void call() throws Exception
				{
					tempAgent.openDebuggerAndWait();
					tempAgent.stop();

					return null;
				}
					}, null);
		}

		//printWmeTree(io.getInputLink());
	}
	
	public FACING calcAvgFacing(ArrayList<FramePredicates> preds)
	{
		HashMap<Predicates.FACING,Integer> facingCounts = new HashMap<Predicates.FACING, Integer>();
		
		for(FramePredicates pred : preds)
		{
			FACING currentFacing = pred.getFrameFacing();
			
			if(!facingCounts.containsKey(currentFacing))
			{
				facingCounts.put(currentFacing, 1);
			}
			else
			{
				facingCounts.put(currentFacing, (facingCounts.get(currentFacing) + 1));
			}
		}
		
		FACING maxFacing = FACING.CENTER;
		int maxCount = 0;
		
		for(FACING value : FACING.values())
		{
			if(facingCounts.get(value) == null)
			{
				continue;
			}
			
			int count = facingCounts.get(value).intValue();
			
			if(count > maxCount)
			{
				maxCount = count;
				maxFacing = value;
			}
		}
		
		return maxFacing;
	}

	public void handleOutputEvents(OutputEvent oe)
	{
		InputOutput io = oe.getInputOutput();
		Boolean isSOARDoneProcessingOutputGesture = false;
		Boolean isSOARDone = false;

		//printWmeTree(io.getOutputLink());

		for(Wme command : io.getPendingCommands())
		{
			Iterator<Wme> commandIterator = command.getChildren();

			while(commandIterator.hasNext())
			{
				Wme innerCommand = commandIterator.next();
				
				handleCommand(io.getSymbols(), innerCommand);
				
				//Only clear after getting an output gesture returned to us
				if(innerCommand.getAttribute().toString().equalsIgnoreCase("gesture"))
				{
					isSOARDoneProcessingOutputGesture = true;
				}
				else if(innerCommand.getAttribute().toString().equalsIgnoreCase("emotion"))
				{
					isSOARDoneProcessingOutputGesture = true;
				}
				else if(innerCommand.getAttribute().toString().equalsIgnoreCase("done"))
				{
					isSOARDone = true;
				}
			}
		}
		
//		TODO - Trying to set status of output command as Complete
//		Iterator<Wme> outputIterator = io.getOutputLink().getWmes();
//		
//		while(outputIterator.hasNext())
//		{
//			Wme command = outputIterator.next();
//			
//			???
//		}

		if(isSOARDoneProcessingOutputGesture)
		{
			if(this.isTransformsListDirty)
			{
//				debugPrint(this.transformsList.toString());
				
				this.transformsList = chooseOneFunctionalTransform(transformsList);
				this.outputGesture.setTransforms(this.transformsList);
				
//				debugPrint(this.transformsList.toString());
				
				this.transformsList = new ArrayList<TransformPredicates.TransformPredicateInterface>();
				this.isTransformsListDirty = false;
			}
		}
		
		if(isSOARDone)
		{
			//clearWmeTree(io.getInputLink());
			this.ThreadedSOARAgent.stop();
			
//			System.out.println("Done processing. Wake Up Main Thread.");
			this.isSOARFinished = true;
			
			writeAgentHeartBeat(false);
		}
	}

	private void handleCommand(SymbolFactory symbols, Wme command) {
		String name = command.getAttribute().toString();

		if(name.equalsIgnoreCase("repeat"))
		{
			//System.out.println("\nOutput Command - Repeat\n");
		}
		else if(name.equalsIgnoreCase("emotion"))
		{
			//System.out.println("\nOutput Command - Emotion\n");
			
			if(command.getValue().asIdentifier() != null)
			{
				Identifier value = command.getValue().asIdentifier();
				
				Iterator<Wme> rootIterator = value.getWmes();
				String emotionString = "";

				while(rootIterator.hasNext())
				{
					Wme current = rootIterator.next();
					if(current.getAttribute().asString().getValue().equalsIgnoreCase("value"))
					{
						emotionString = current.getValue().asString().getValue();
//						System.out.println("VAI Output Emotion: " + emotionString);
					}
					else if (current.getAttribute().asString().getValue().equalsIgnoreCase("gesture"))
					{
						this.outputGesture = parseGestureWmeTree(current.getValue().asIdentifier());
						this.isOutputDirty = true;
					}
				}
				
				if(CHEATWITHGOLDMODE)
				{
					setCheatingOutputGesture(emotionString);
				}
				else
				{
					if(this.isCurrentUUIDDirty)
					{
						debugPrint("JSOAR Returned UUID: " + this.currentUUID);
						setOutputGesture(this.currentUUID);
					}
				}
			}
		}
		else if(name.equalsIgnoreCase("new"))
		{
			//System.out.println("\nOutput Command - New\n");
		}
		else if(name.equalsIgnoreCase("vary"))
		{
			//System.out.println("\nOutput Command - Vary\n");
			
			Identifier value = command.getValue().asIdentifier();
			
			Iterator<Wme> rootIterator = value.getWmes();
			ArrayList<Wme> rootAList = new ArrayList<Wme>();

			while(rootIterator.hasNext())
			{
				rootAList.add(0, rootIterator.next());
			}

			for(Wme current : rootAList)
			{
				//System.out.println("\nTRANSFORM PREDICATES: " + current.getAttribute() + " = " + current.getValue());
				
				String enumName = current.getAttribute().asString().getValue();
				String enumValue = current.getValue().asString().getValue();
				
				transformsList.add(TransformPredicates.parseTransformPredicates(enumName, enumValue));
				isTransformsListDirty = true;
			}
			
//			printWmeTree(value);
		}
		else if(name.equalsIgnoreCase("pattern"))
		{
			//System.out.println("\nOutput Command - Pattern\n");
		}
		else if(name.equalsIgnoreCase("noop"))
		{
			//System.out.println("\nOutput Command - NoOp\n");
		}
		else if(name.equalsIgnoreCase("response-mode"))
		{
			//System.out.println("\nOutput Command - Response Mode\n");
			
			String enumValue = command.getValue().asString().getValue();
			
			this.responseMode = RESPONSE_MODE.valueOf(enumValue);
			
			//System.out.println("\nRESPONSE MODE: " + enumValue);
		}
		else if(name.equalsIgnoreCase("gesture"))
		{
			//System.out.println("\nOutput Gesture\n");

			if(command.getValue().asIdentifier() != null)
			{
				Identifier rootId = command.getValue().asIdentifier();

				//printWmeTree(rootId);
//				debugPrint(rootId.toString());
				this.outputGesture = parseGestureWmeTree(rootId);
				this.isOutputDirty = true;
				
				if(this.isCurrentUUIDDirty)
				{
					debugPrint("JSOAR Returned UUID: " + this.currentUUID);
					setOutputGesture(this.currentUUID);
				}
			}
			else
			{
				//System.out.println("ERROR: NO GESTURE DATA OUTPUT\n");
			}
		}
		else if(name.equalsIgnoreCase("done"))
		{
			//System.out.println("\nCurrent Turn - Done\n");
		}
		else
		{
			//System.out.println("Unknown Command - " + name + "\n");
		}
	}

	public ArrayList<TransformPredicateInterface> chooseOneFunctionalTransform(ArrayList<TransformPredicateInterface> transforms)
	{
		ArrayList<TransformPredicateInterface> temp = new ArrayList<TransformPredicates.TransformPredicateInterface>();
		ArrayList<TransformPredicateInterface> results = new ArrayList<TransformPredicates.TransformPredicateInterface>();
		Random r = new Random();
		
		for(TransformPredicateInterface t : transforms)
		{
			if(t.getClass().equals(ADD_COPY_LIMB.class))
			{
				temp.add(t);
			}
			else if(t.getClass().equals(ADD_SWITCH_LIMBS.class))
			{
				temp.add(t);
			}
			else if(t.getClass().equals(ADD_REFLECT_LIMB.class))
			{
				temp.add(t);
			}
			else
			{
				results.add(t);
			}
		}
		
		if(temp.size() < 1)
		{
			return transforms;
		}
		
		TransformPredicateInterface tempClass = temp.get(r.nextInt(temp.size()));
		
//		debugPrint("Chose Type: " + tempClass.getClass());
		
		for(TransformPredicateInterface t : temp)
		{
			if(t.getClass().equals(tempClass.getClass()))
			{
				results.add(t);
			}
			else
			{
//				debugPrint("Excluded transform: " + t.getClass());
			}
		}
		
		return results;
	}
	
	public JointSpaceGesture cheatWithGold(String gesturePath)
	{
		File gestureFolder = new File(gesturePath);
		File[] gestureFileList = gestureFolder.listFiles(); 
		Random rand = new Random();
		File gesturePathFile = gestureFileList[rand.nextInt(gestureFileList.length)];
//		System.out.println("JSOAR Cheated With Golden Gesture: " + gesturePathFile.toString());
		return deserializeJointsGesture(gesturePathFile);
	}
	
	public static JointSpaceGesture deserializeJointsGesture(File file)
	{
		JointSpaceGesture readGesture = new JointSpaceGesture();
		
		try
		{
			FileInputStream fileinstream = new FileInputStream(file);
			
			if(fileinstream.available() == 0)
			{
				return null;
			}
			
			ObjectInputStream objectinstream = new ObjectInputStream(fileinstream);
			readGesture = (JointSpaceGesture) objectinstream.readObject();
			objectinstream.close();
		}
		catch (FileNotFoundException e)
		{
			//e.printStackTrace();
			
			return null;
		}
		catch (EOFException e)
		{
			//e.printStackTrace();
			
			return null;
		}
		catch (IOException e)
		{
			//e.printStackTrace();
			
			return null;
		} 
		catch (ClassNotFoundException e) 
		{
			//e.printStackTrace();
			
			return null;
		}
		
//		System.out.println("Done Reading");
		
		return readGesture;
	}
	
	public void printWmeTree(Identifier rootId)
	{
		printWmeTreeRecurse(rootId, 0);
		System.out.println("");
	}

	public void printWmeTreeRecurse(Identifier rootId, int treeDepth)
	{
		Iterator<Wme> rootIterator = rootId.getWmes();
		Identifier subRootId;
		ArrayList<Wme> rootAList = new ArrayList<Wme>();

		while(rootIterator.hasNext())
		{
			rootAList.add(0, rootIterator.next());
		}

		for(Wme current : rootAList)
		{
			for(int i = 0; i < treeDepth; i++)
			{
				if(i != treeDepth - 1)
				{
					System.out.print("|   ");
				}
				else
				{
					System.out.print("|===");
				}
			}

			//System.out.println(current.getAttribute() + "\t" + current.getValue());

			if((subRootId = current.getValue().asIdentifier()) != null)
			{
				printWmeTreeRecurse(subRootId, treeDepth + 1);
			}
		}
	}

	public void clearWmeTree(Identifier rootId)
	{
		clearWmeTreeRecurse(rootId);
	}

	public void clearWmeTreeRecurse(Identifier rootId)
	{
		Iterator<Wme> rootIterator = rootId.getWmes();
		Identifier subRootId;
		while(rootIterator.hasNext())
		{
			Wme current = rootIterator.next();

			if((subRootId = current.getValue().asIdentifier()) != null)
			{
				clearWmeTreeRecurse(subRootId);
			}

			InputWme inputWme = Adaptables.adapt(current, InputWme.class);
			if(inputWme != null)
			{
				inputWme.remove();
			}
		}
	}

	public Gesture parseGestureWmeTree(Identifier rootId)
	{
		return parseGestureWmeTree(rootId, null);
	}
	
	public Gesture parseGestureWmeTree(Identifier rootId, ArrayList<TransformPredicateInterface> transforms)
	{
		Boolean isMultiFrame = false;

		DURATION avgDuration = DURATION.NONE;
		TEMPO avgTempo = TEMPO.NONE;
		LEFT_LEG_STILL avgLeftLegStillness = LEFT_LEG_STILL.TRUE;
		RIGHT_LEG_STILL avgRightLegStillness = RIGHT_LEG_STILL.TRUE;
		LEFT_HAND_STILL avgLeftHandStillness = LEFT_HAND_STILL.TRUE;
		RIGHT_HAND_STILL avgRightHandStillness = RIGHT_HAND_STILL.TRUE;
		LEFT_LEG_TRANSVERSE avgLeftLegTransverse = LEFT_LEG_TRANSVERSE.TRUE;
		RIGHT_LEG_TRANSVERSE avgRightLegTransverse = RIGHT_LEG_TRANSVERSE.TRUE;
		LEFT_HAND_TRANSVERSE avgLeftHandTransverse = LEFT_HAND_TRANSVERSE.TRUE;
		RIGHT_HAND_TRANSVERSE avgRightHandTransverse = RIGHT_HAND_TRANSVERSE.TRUE;
		LEFT_LEG_LONGITUDINAL avgLeftLegLongitudinal = LEFT_LEG_LONGITUDINAL.TRUE;
		RIGHT_LEG_LONGITUDINAL avgRightLegLongitudinal = RIGHT_LEG_LONGITUDINAL.TRUE;
		LEFT_HAND_LONGITUDINAL avgLeftHandLongitudinal = LEFT_HAND_LONGITUDINAL.TRUE;
		RIGHT_HAND_LONGITUDINAL avgRightHandLongitudinal = RIGHT_HAND_LONGITUDINAL.TRUE;
		LEFT_LEG_VERTICAL avgLeftLegVertical = LEFT_LEG_VERTICAL.TRUE;
		RIGHT_LEG_VERTICAL avgRightLegVertical = RIGHT_LEG_VERTICAL.TRUE;
		LEFT_HAND_VERTICAL avgLeftHandVertical = LEFT_HAND_VERTICAL.TRUE;
		RIGHT_HAND_VERTICAL avgRightHandVertical = RIGHT_HAND_VERTICAL.TRUE;
		SMOOTHNESS avgSmoothness = SMOOTHNESS.NONE;
		ENERGY avgEnergy = ENERGY.NONE;
		EMOTION emotion = EMOTION.NONE;

		ArrayList<FramePredicates> All_Predicate_History = new ArrayList<FramePredicates>();

		Iterator<Wme> gestureIdIterator = rootId.getWmes();

		while(gestureIdIterator.hasNext())
		{
			Wme gestureChildWme = gestureIdIterator.next();
			String gestureChildWmeName = gestureChildWme.getAttribute().toString();
			
			if(gestureChildWmeName.equalsIgnoreCase("uuid"))
			{
				this.currentUUID = UUID.fromString(gestureChildWme.getValue().asString().toString());
				this.isCurrentUUIDDirty = true;
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgduration"))
			{
				avgDuration = DURATION.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgtempo"))
			{
				avgTempo = TEMPO.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgleftlegstillness"))
			{
				avgLeftLegStillness = LEFT_LEG_STILL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrightlegstillness"))
			{
				avgRightLegStillness = RIGHT_LEG_STILL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avglefthandstillness"))
			{
				avgLeftHandStillness = LEFT_HAND_STILL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrighthandstillness"))
			{
				avgRightHandStillness = RIGHT_HAND_STILL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgleftlegtransverse"))
			{
				avgLeftLegTransverse = LEFT_LEG_TRANSVERSE.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrightlegtransverse"))
			{
				avgRightLegTransverse = RIGHT_LEG_TRANSVERSE.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avglefthandtransverse"))
			{
				avgLeftHandTransverse = LEFT_HAND_TRANSVERSE.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrighthandtransverse"))
			{
				avgRightHandTransverse = RIGHT_HAND_TRANSVERSE.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgleftleglongitudinal"))
			{
				avgLeftLegLongitudinal = LEFT_LEG_LONGITUDINAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrightleglongitudinal"))
			{
				avgRightLegLongitudinal = RIGHT_LEG_LONGITUDINAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avglefthandlongitudinal"))
			{
				avgLeftHandLongitudinal = LEFT_HAND_LONGITUDINAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrighthandlongitudinal"))
			{
				avgRightHandLongitudinal = RIGHT_HAND_LONGITUDINAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgleftlegvertical"))
			{
				avgLeftLegVertical = LEFT_LEG_VERTICAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrightlegvertical"))
			{
				avgRightLegVertical = RIGHT_LEG_VERTICAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avglefthandvertical"))
			{
				avgLeftHandVertical = LEFT_HAND_VERTICAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgrighthandvertical"))
			{
				avgRightHandVertical = RIGHT_HAND_VERTICAL.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgsmoothness"))
			{
				avgSmoothness = SMOOTHNESS.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("avgenergy"))
			{
				avgEnergy = ENERGY.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("emotion"))
			{
				emotion = EMOTION.valueOf(gestureChildWme.getValue().asString().toString());
			}
			else if(gestureChildWmeName.equalsIgnoreCase("all_predicate_history"))
			{
				Iterator<Wme> AllPredHistIterator = gestureChildWme.getChildren();
				ArrayList<Wme> AllPredHistAList = new ArrayList<Wme>();
				
				//Brittle Fix: Assuming that iterator returns elements in reverse order of how they were added!!!
				while(AllPredHistIterator.hasNext())
				{
					AllPredHistAList.add(0, AllPredHistIterator.next());
				}

				for(Wme currentHistoryWme : AllPredHistAList)
				{
					String currentHistoryWmeName = currentHistoryWme.getAttribute().toString();

					if(currentHistoryWmeName.equalsIgnoreCase("node"))
					{
						FramePredicates currentFrame = parseFramePredicatesWmeTree(currentHistoryWme.getValue().asIdentifier());
						
						All_Predicate_History.add(currentFrame);
					}
					else if(currentHistoryWmeName.equalsIgnoreCase("multiframe"))
					{
						isMultiFrame = Boolean.parseBoolean(currentHistoryWme.getValue().asString().toString().toLowerCase()); 
					}
					else
					{
//						System.out.println("ERROR: UNKNOWN WME INSIDE ALL_PREDICATE_HISTORY");
					}
				}
			}
		}

		Gesture outputGesture = new Gesture(isMultiFrame, avgDuration, avgTempo, avgLeftLegStillness, avgRightLegStillness, avgLeftHandStillness, avgRightHandStillness, avgLeftLegTransverse, avgRightLegTransverse, avgLeftHandTransverse, avgRightHandTransverse, avgLeftLegLongitudinal, avgRightLegLongitudinal, avgLeftHandLongitudinal, avgRightHandLongitudinal, avgLeftLegVertical, avgRightLegVertical, avgLeftHandVertical, avgRightHandVertical,avgSmoothness, avgEnergy, All_Predicate_History, new ArrayList<TransformPredicateInterface>(),emotion);
		
		//System.out.println("Output Gesture:\n\n" + outputGesture.outString() + "\n\n");
		
		return outputGesture;
	}

	public FramePredicates parseFramePredicatesWmeTree(Identifier rootId)
	{
		FramePredicates gestureFrame;
		
		DURATION multiFrameDuration = DURATION.NONE;
		TEMPO frameTempo = TEMPO.NONE;
		SMOOTHNESS frameSmoothness = SMOOTHNESS.NONE;
		ENERGY frameEnergy = ENERGY.NONE;

		LEFTARM_CURVE frameLeftArmCurve = LEFTARM_CURVE.NONE;
		LEFTHAND_POS frameLeftHandPos = LEFTHAND_POS.NONE;
		LEFTHAND_HEIGHT frameLeftHandHeight = LEFTHAND_HEIGHT.NONE;
		RIGHTARM_CURVE frameRightArmCurve = RIGHTARM_CURVE.NONE;
		RIGHTHAND_POS frameRightHandPos = RIGHTHAND_POS.NONE;
		RIGHTHAND_HEIGHT frameRightHandHeight = RIGHTHAND_HEIGHT.NONE;
		BOTHARM_CURVE frameBothArmCurve = BOTHARM_CURVE.NONE;
		LEFTLEG_CURVE frameLeftLegCurve = LEFTLEG_CURVE.NONE;
		RIGHTLEG_CURVE frameRightLegCurve = RIGHTLEG_CURVE.NONE;
		HANDS_TOGETHER frameHandsTogether = HANDS_TOGETHER.NONE;
		ARMS_CROSSED frameArmsCrossed = ARMS_CROSSED.NONE;
		BOTH_HANDS_BY_CHEST frameBothHandsByChest = BOTH_HANDS_BY_CHEST.NONE;
		BODY_SYMMETRIC frameBodySymmetric = BODY_SYMMETRIC.NONE;
		FACING frameFacing = FACING.NONE;
		HEIGHT frameHeight = HEIGHT.NONE;
		QUADRANT frameQuadrant = QUADRANT.NONE;
		DIST_CENTER frameDistCenter = DIST_CENTER.NONE;
		SIZE frameSize = SIZE.NONE;
		
		Iterator<Wme> AllPredHistIterator = rootId.getWmes();

		while(AllPredHistIterator.hasNext())
		{
			Wme currentHistoryWme = AllPredHistIterator.next();
			String currentHistoryWmeName = currentHistoryWme.getAttribute().toString();

			if(currentHistoryWmeName.equalsIgnoreCase("index"))
			{
				
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("multiframeduration"))
			{
				 multiFrameDuration = DURATION.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("tempo"))
			{
				 frameTempo = TEMPO.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("smoothness"))
			{
				 frameSmoothness = SMOOTHNESS.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("energy"))
			{
				frameEnergy = ENERGY.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("leftarm_curve"))
			{
				frameLeftArmCurve = LEFTARM_CURVE.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("lefthand_pos"))
			{
				frameLeftHandPos = LEFTHAND_POS.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("lefthand_height"))
			{
				frameLeftHandHeight = LEFTHAND_HEIGHT.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("rightarm_curve"))
			{
				frameRightArmCurve = RIGHTARM_CURVE.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("righthand_pos"))
			{
				frameRightHandPos = RIGHTHAND_POS.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("righthand_height"))
			{
				frameRightHandHeight = RIGHTHAND_HEIGHT.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("botharm_curve"))
			{
				frameBothArmCurve = BOTHARM_CURVE.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("leftleg_curve"))
			{
				frameLeftLegCurve = LEFTLEG_CURVE.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("rightleg_curve"))
			{
				frameRightLegCurve = RIGHTLEG_CURVE.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("hands_together"))
			{
				frameHandsTogether = HANDS_TOGETHER.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("arms_crossed"))
			{
				frameArmsCrossed = ARMS_CROSSED.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("both_hands_by_chest"))
			{
				frameBothHandsByChest = BOTH_HANDS_BY_CHEST.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("body_symmetric"))
			{
				frameBodySymmetric = BODY_SYMMETRIC.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("facing"))
			{
				frameFacing = FACING.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("height"))
			{
				frameHeight = HEIGHT.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("quadrant"))
			{
				frameQuadrant = QUADRANT.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("dist_center"))
			{
				frameDistCenter = DIST_CENTER.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else if(currentHistoryWmeName.equalsIgnoreCase("size"))
			{
				frameSize = SIZE.valueOf(currentHistoryWme.getValue().asString().toString());
			}
			else
			{
//				System.out.println("ERROR: UNKNOWN WME INSIDE NODE OF ALL_PREDICATE_HISTORY " + currentHistoryWmeName);
			}
		}
		
		gestureFrame = new FramePredicates(multiFrameDuration, frameTempo, frameSmoothness, frameEnergy,
				frameLeftArmCurve, frameLeftHandPos, frameLeftHandHeight, frameRightArmCurve,
				frameRightHandPos, frameRightHandHeight, frameBothArmCurve, frameLeftLegCurve,
				frameRightLegCurve, frameHandsTogether, frameArmsCrossed, frameBothHandsByChest,
				frameBodySymmetric, frameFacing, frameHeight, frameQuadrant, frameDistCenter, frameSize);
		
		return gestureFrame;
	}
}
