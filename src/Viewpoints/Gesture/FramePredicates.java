package Viewpoints.Gesture;

import java.io.Serializable;

import Viewpoints.Gesture.Predicates.*;

public class FramePredicates implements Serializable
{
	
	/**
	 * Do not change unless you want to intentionally break serializability
	 * with previous versions of this class
	 */
	private static final long serialVersionUID = -2152069985360756062L;
	public static int NumberOfSingleFramePredicates = 21;
	public static int NumberOfMultiFramePredicates = 22;

	//MultiFrame Predicates
	public DURATION multiFrameDuration;

	//Frame ViewPoints Predicates
	public TEMPO frameTempo;
	public SMOOTHNESS frameSmoothness;
	public ENERGY frameEnergy;

	//Frame Gesture Predicates

	//Left Arm
	public LEFTARM_CURVE frameLeftArmCurve;
	public LEFTHAND_POS frameLeftHandPos;
	public LEFTHAND_HEIGHT frameLeftHandHeight;
	//Right Arm
	public RIGHTARM_CURVE frameRightArmCurve;
	public RIGHTHAND_POS frameRightHandPos;
	public RIGHTHAND_HEIGHT frameRightHandHeight;
	//Both Arm
	public BOTHARM_CURVE frameBothArmCurve;
	//Left Leg
	public LEFTLEG_CURVE frameLeftLegCurve;
	//Right Leg
	public RIGHTLEG_CURVE frameRightLegCurve;
	//Hands / Arms Properties
	public HANDS_TOGETHER frameHandsTogether;
	public ARMS_CROSSED frameArmsCrossed;
	public BOTH_HANDS_BY_CHEST frameBothHandsByChest;
	//Body Properties
	public BODY_SYMMETRIC frameBodySymmetric;
	public FACING frameFacing;
	public HEIGHT frameHeight;
	public QUADRANT frameQuadrant;
	public DIST_CENTER frameDistCenter;
	public SIZE frameSize;

	//TODO
	public FramePredicates()
	{
		multiFrameDuration = DURATION.NONE;

		frameTempo = TEMPO.NONE;
		frameSmoothness = SMOOTHNESS.NONE;
		frameEnergy = ENERGY.NONE;

		frameLeftArmCurve = LEFTARM_CURVE.NONE;
		frameLeftHandPos = LEFTHAND_POS.NONE;
		frameLeftHandHeight = LEFTHAND_HEIGHT.NONE;
		frameRightArmCurve = RIGHTARM_CURVE.NONE;
		frameRightHandPos = RIGHTHAND_POS.NONE;
		frameRightHandHeight = RIGHTHAND_HEIGHT.NONE;
		frameBothArmCurve = BOTHARM_CURVE.NONE;
		frameLeftLegCurve = LEFTLEG_CURVE.NONE;
		frameRightLegCurve = RIGHTLEG_CURVE.NONE;
		frameHandsTogether = HANDS_TOGETHER.NONE;
		frameArmsCrossed = ARMS_CROSSED.NONE;
		frameBothHandsByChest = BOTH_HANDS_BY_CHEST.NONE;
		frameBodySymmetric = BODY_SYMMETRIC.NONE;
		frameFacing = FACING.NONE;
		frameHeight = HEIGHT.NONE;
		frameQuadrant = QUADRANT.NONE;
		frameDistCenter = DIST_CENTER.NONE;
		frameSize = SIZE.NONE;
	}

	public FramePredicates(DURATION mfd, TEMPO t, SMOOTHNESS sm, ENERGY e, 
			LEFTARM_CURVE lac, LEFTHAND_POS lhp, LEFTHAND_HEIGHT lhh, 
			RIGHTARM_CURVE rac, RIGHTHAND_POS rhp, RIGHTHAND_HEIGHT rhh, 
			BOTHARM_CURVE bac, LEFTLEG_CURVE llc, RIGHTLEG_CURVE rlc, 
			HANDS_TOGETHER ht, ARMS_CROSSED ac, BOTH_HANDS_BY_CHEST bhbc, 
			BODY_SYMMETRIC bs, FACING f, HEIGHT h, QUADRANT q, DIST_CENTER dc, SIZE s)
	{
		multiFrameDuration = mfd;

		frameTempo = t;
		frameSmoothness = sm;
		frameEnergy = e;

		frameLeftArmCurve = lac;
		frameLeftHandPos = lhp;
		frameLeftHandHeight = lhh;
		frameRightArmCurve = rac;
		frameRightHandPos = rhp;
		frameRightHandHeight = rhh;
		frameBothArmCurve = bac;
		frameLeftLegCurve = llc;
		frameRightLegCurve = rlc;
		frameHandsTogether = ht;
		frameArmsCrossed = ac;
		frameBothHandsByChest = bhbc;
		frameBodySymmetric = bs;
		frameFacing = f;
		frameHeight = h;
		frameQuadrant = q;
		frameDistCenter = dc;
		frameSize = s;
	}

	public FramePredicates(TEMPO t, SMOOTHNESS sm, ENERGY e, 
			LEFTARM_CURVE lac, LEFTHAND_POS lhp, LEFTHAND_HEIGHT lhh, 
			RIGHTARM_CURVE rac, RIGHTHAND_POS rhp, RIGHTHAND_HEIGHT rhh, 
			BOTHARM_CURVE bac, LEFTLEG_CURVE llc, RIGHTLEG_CURVE rlc, 
			HANDS_TOGETHER ht, ARMS_CROSSED ac, BOTH_HANDS_BY_CHEST bhbc, 
			BODY_SYMMETRIC bs, FACING f, HEIGHT h, QUADRANT q, DIST_CENTER dc, SIZE s)
	{
		multiFrameDuration = DURATION.NONE;

		frameTempo = t;
		frameSmoothness = sm;
		frameEnergy = e;

		frameLeftArmCurve = lac;
		frameLeftHandPos = lhp;
		frameLeftHandHeight = lhh;
		frameRightArmCurve = rac;
		frameRightHandPos = rhp;
		frameRightHandHeight = rhh;
		frameBothArmCurve = bac;
		frameLeftLegCurve = llc;
		frameRightLegCurve = rlc;
		frameHandsTogether = ht;
		frameArmsCrossed = ac;
		frameBothHandsByChest = bhbc;
		frameBodySymmetric = bs;
		frameFacing = f;
		frameHeight = h;
		frameQuadrant = q;
		frameDistCenter = dc;
		frameSize = s;
	}
	
	public Object get(String predicate) throws IllegalArgumentException, IllegalAccessException {
		for (java.lang.reflect.Field field : this.getClass().getFields()) {
			if (predicate.equals(field.getType().getSimpleName())) {
				return field.get(this);
			}
		}
		return null;
	}
	
	public void set(String predicate, Object value) throws IllegalArgumentException, IllegalAccessException {
		for (java.lang.reflect.Field field : this.getClass().getFields()) {
			if (predicate.equals(field.getType().getName())) {
				field.set(this, value);
			}
		}
	}

	public DURATION getMultiFrameDuration() {
		return multiFrameDuration;
	}

	public TEMPO getFrameTempo() {
		return frameTempo;
	}

	public SMOOTHNESS getFrameSmoothness() {
		return frameSmoothness;
	}

	public ENERGY getFrameEnergy() {
		return frameEnergy;
	}

	public LEFTARM_CURVE getFrameLeftArmCurve() {
		return frameLeftArmCurve;
	}

	public LEFTHAND_POS getFrameLeftHandPos() {
		return frameLeftHandPos;
	}

	public LEFTHAND_HEIGHT getFrameLeftHandHeight() {
		return frameLeftHandHeight;
	}

	public RIGHTARM_CURVE getFrameRightArmCurve() {
		return frameRightArmCurve;
	}

	public RIGHTHAND_POS getFrameRightHandPos() {
		return frameRightHandPos;
	}

	public RIGHTHAND_HEIGHT getFrameRightHandHeight() {
		return frameRightHandHeight;
	}

	public BOTHARM_CURVE getFrameBothArmCurve() {
		return frameBothArmCurve;
	}

	public LEFTLEG_CURVE getFrameLeftLegCurve() {
		return frameLeftLegCurve;
	}

	public RIGHTLEG_CURVE getFrameRightLegCurve() {
		return frameRightLegCurve;
	}

	public BODY_SYMMETRIC getFrameBodySymmetric() {
		return frameBodySymmetric;
	}

	public HANDS_TOGETHER getFrameHandsTogether() {
		return frameHandsTogether;
	}

	public ARMS_CROSSED getFrameArmsCrossed() {
		return frameArmsCrossed;
	}

	public BOTH_HANDS_BY_CHEST getFrameBothHandsByChest() {
		return frameBothHandsByChest;
	}

	public FACING getFrameFacing() {
		return frameFacing;
	}

	public HEIGHT getFrameHeight() {
		return frameHeight;
	}

	public QUADRANT getFrameQuadrant() {
		return frameQuadrant;
	}

	public DIST_CENTER getFrameDistCenter() {
		return frameDistCenter;
	}

	public SIZE getFrameSize() {
		return frameSize;
	}

	public void setMultiFrameDuration(DURATION multiFrameDuration) {
		this.multiFrameDuration = multiFrameDuration;
	}

	public void setFrameTempo(TEMPO frameTempo) {
		this.frameTempo = frameTempo;
	}

	public void setFrameSmoothness(SMOOTHNESS frameSmoothness) {
		this.frameSmoothness = frameSmoothness;
	}

	public void setFrameEnergy(ENERGY frameEnergy) {
		this.frameEnergy = frameEnergy;
	}

	public void setFrameLeftArmCurve(LEFTARM_CURVE frameLeftArmCurve) {
		this.frameLeftArmCurve = frameLeftArmCurve;
	}

	public void setFrameLeftHandPos(LEFTHAND_POS frameLeftHandPos) {
		this.frameLeftHandPos = frameLeftHandPos;
	}

	public void setFrameLeftHandHeight(LEFTHAND_HEIGHT frameLeftHandHeight) {
		this.frameLeftHandHeight = frameLeftHandHeight;
	}

	public void setFrameRightArmCurve(RIGHTARM_CURVE frameRightArmCurve) {
		this.frameRightArmCurve = frameRightArmCurve;
	}

	public void setFrameRightHandPos(RIGHTHAND_POS frameRightHandPos) {
		this.frameRightHandPos = frameRightHandPos;
	}

	public void setFrameRightHandHeight(RIGHTHAND_HEIGHT frameRightHandHeight) {
		this.frameRightHandHeight = frameRightHandHeight;
	}

	public void setFrameBothArmCurve(BOTHARM_CURVE frameBothArmCurve) {
		this.frameBothArmCurve = frameBothArmCurve;
	}

	public void setFrameLeftLegCurve(LEFTLEG_CURVE frameLeftLegCurve) {
		this.frameLeftLegCurve = frameLeftLegCurve;
	}

	public void setFrameRightLegCurve(RIGHTLEG_CURVE frameRightLegCurve) {
		this.frameRightLegCurve = frameRightLegCurve;
	}

	public void setFrameHandsTogether(HANDS_TOGETHER frameHandsTogether) {
		this.frameHandsTogether = frameHandsTogether;
	}

	public void setFrameArmsCrossed(ARMS_CROSSED frameArmsCrossed) {
		this.frameArmsCrossed = frameArmsCrossed;
	}

	public void setFrameBothHandsByChest(BOTH_HANDS_BY_CHEST frameBothHandsByChest) {
		this.frameBothHandsByChest = frameBothHandsByChest;
	}

	public void setFrameBodySymmetric(BODY_SYMMETRIC frameBodySymmetric) {
		this.frameBodySymmetric = frameBodySymmetric;
	}

	public void setFrameFacing(FACING frameFacing) {
		this.frameFacing = frameFacing;
	}

	public void setFrameHeight(HEIGHT frameHeight) {
		this.frameHeight = frameHeight;
	}

	public void setFrameQuadrant(QUADRANT frameQuadrant) {
		this.frameQuadrant = frameQuadrant;
	}

	public void setFrameDistCenter(DIST_CENTER frameDistCenter) {
		this.frameDistCenter = frameDistCenter;
	}

	public void setFrameSize(SIZE frameSize) {
		this.frameSize = frameSize;
	}

	public String toString() {
		return this.outString();
	}

	//Replace with an overload of Object.toString()
	public String outString() {
		String outStr = "";
//		outStr += multiFrameDuration.toString() + ",";
		outStr += frameTempo.getClass() + "=" + frameTempo.toString() + ",";
		outStr += frameSmoothness.getClass() + "=" + frameSmoothness.toString() + ",";
		outStr += frameEnergy.getClass() + "=" + frameEnergy.toString() + ",";
		outStr += frameLeftArmCurve.getClass() + "=" + frameLeftArmCurve.toString() + ",";
		outStr += frameLeftHandPos.getClass() + "=" + frameLeftHandPos.toString() + ",";
		outStr += frameLeftHandHeight.getClass() + "=" + frameLeftHandHeight.toString() + ",";
		outStr += frameRightArmCurve.getClass() + "=" + frameRightArmCurve.toString() + ",";
		outStr += frameRightHandPos.getClass() + "=" + frameRightHandPos.toString() + ",";
		outStr += frameRightHandHeight.getClass() + "=" + frameRightHandHeight.toString() + ",";
		outStr += frameBothArmCurve.getClass() + "=" + frameBothArmCurve.toString() + ",";
		outStr += frameLeftLegCurve.getClass() + "=" + frameLeftLegCurve.toString() + ",";
		outStr += frameRightLegCurve.getClass() + "=" + frameRightLegCurve.toString() + ",";
		outStr += frameHandsTogether.getClass() + "=" + frameHandsTogether.toString() + ",";
		outStr += frameArmsCrossed.getClass() + "=" + frameArmsCrossed.toString() + ",";
		outStr += frameBothHandsByChest.getClass() + "=" + frameBothHandsByChest.toString() + ",";
		outStr += frameBodySymmetric.getClass() + "=" + frameBodySymmetric.toString() + ",";
		outStr += frameFacing.getClass() + "=" + frameFacing.toString() + ",";
		outStr += frameHeight.getClass() + "=" + frameHeight.toString() + ",";
		outStr += frameQuadrant.getClass() + "=" + frameQuadrant.toString() + ",";
		outStr += frameDistCenter.getClass() + "=" + frameDistCenter.toString() + ",";
		outStr += frameSize.getClass() + "=" + frameSize.toString() + ",";

		return outStr;

	}
}
