package Viewpoints.Gesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.Gesture.Predicates.*;
import Viewpoints.Gesture.Predicates.TEMPO;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;

public class LeftHandVerticalPredicate extends ViewpointPredicate {
	private static final float[] rawLevels    = {0.0f, 30 * ASSUMED_FRAMERATE, 60 * ASSUMED_FRAMERATE};
	private static final float[] scaledLevels = {0.0f, 0.5f,                   1.0f};
	
	public LeftHandVerticalPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}

	public LeftHandVerticalPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public LEFT_HAND_VERTICAL classifyPredicate(float predNumber) {
		if (predNumber < 0.5f) {
//			System.out.println("LEFT HAND STILL");
			return LEFT_HAND_VERTICAL.FALSE;
		} else {
			//System.out.println("LEFT HAND VERTICALLY MOVING");
			return LEFT_HAND_VERTICAL.TRUE;
		}
			
	}
	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 2) {
			throw new Error("calcPredNumber: too few (< 2) skeletons !");
		}
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		
		HashMap<JIDX, PVector> diffSkel = getLeftHandVerticalMvmtDiff(inCurSkel, inPrevSkel);
		
		HashMap<JIDX, Float> tempos = new HashMap<JIDX, Float>();

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				tempos.put(ji, Math.abs(diffSkel.get(ji).mag()));
			}
		}
		float tempoVal = getMax(tempos) * valueMultiplier;
		tempoVal /= (inCurSkel.getTimestamp() - inPrevSkel.getTimestamp());
		//System.out.println("Tempo Val: "+tempoVal);
		return toTargetRange(tempoVal, rawLevels, scaledLevels);
	}
}
