package Viewpoints.Gesture;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Viewpoints.Gesture.Predicates.*;
import Viewpoints.Gesture.TransformPredicates.TransformPredicateInterface;

public class Gesture implements Serializable
{
	/**
	 * Do not change unless you want to intentionally break serializability
	 * with previous versions of this class
	 */
	private static final long serialVersionUID = 9032208058250859931L;

	//MultiFrame Predicates
	public Boolean isMultiFrame;

	//Viewpoints Predicates - Average
	public DURATION avgDuration;
	public TEMPO avgTempo;
	public LEFT_LEG_STILL avgLeftLegStillness;
	public RIGHT_LEG_STILL avgRightLegStillness;
	public LEFT_HAND_STILL avgLeftHandStillness;
	public RIGHT_HAND_STILL avgRightHandStillness;
	public LEFT_LEG_TRANSVERSE avgLeftLegTransverse;
	public RIGHT_LEG_TRANSVERSE avgRightLegTransverse;
	public LEFT_HAND_TRANSVERSE avgLeftHandTransverse;
	public RIGHT_HAND_TRANSVERSE avgRightHandTransverse;
	public LEFT_LEG_LONGITUDINAL avgLeftLegLongitudinal;
	public RIGHT_LEG_LONGITUDINAL avgRightLegLongitudinal;
	public LEFT_HAND_LONGITUDINAL avgLeftHandLongitudinal;
	public RIGHT_HAND_LONGITUDINAL avgRightHandLongitudinal;
	public LEFT_LEG_VERTICAL avgLeftLegVertical;
	public RIGHT_LEG_VERTICAL avgRightLegVertical;
	public LEFT_HAND_VERTICAL avgLeftHandVertical;
	public RIGHT_HAND_VERTICAL avgRightHandVertical;
	public SMOOTHNESS avgSmoothness;
	public ENERGY avgEnergy;
	public EMOTION emotion;

	//Predicates - Per Frame
	public ArrayList<FramePredicates> All_Predicate_History;
	
	public ArrayList<TransformPredicateInterface> transforms;

	public Gesture()
	{
		isMultiFrame = false;
		avgDuration = DURATION.NONE;
		avgTempo = TEMPO.NONE;
		avgLeftLegStillness = LEFT_LEG_STILL.TRUE;
		avgRightLegStillness = RIGHT_LEG_STILL.TRUE;
		avgLeftHandStillness = LEFT_HAND_STILL.TRUE;
		avgRightHandStillness = RIGHT_HAND_STILL.TRUE;
		avgLeftLegTransverse = LEFT_LEG_TRANSVERSE.TRUE;
		avgRightLegTransverse = RIGHT_LEG_TRANSVERSE.TRUE;
		avgLeftHandTransverse = LEFT_HAND_TRANSVERSE.TRUE;
		avgRightHandTransverse = RIGHT_HAND_TRANSVERSE.TRUE;
		avgLeftLegLongitudinal = LEFT_LEG_LONGITUDINAL.TRUE;
		avgRightLegLongitudinal = RIGHT_LEG_LONGITUDINAL.TRUE;
		avgLeftHandLongitudinal = LEFT_HAND_LONGITUDINAL.TRUE;
		avgRightHandLongitudinal = RIGHT_HAND_LONGITUDINAL.TRUE;
		avgLeftLegVertical = LEFT_LEG_VERTICAL.TRUE;
		avgRightLegVertical = RIGHT_LEG_VERTICAL.TRUE;
		avgLeftHandVertical = LEFT_HAND_VERTICAL.TRUE;
		avgRightHandVertical = RIGHT_HAND_VERTICAL.TRUE;
		avgSmoothness = SMOOTHNESS.NONE;
		avgEnergy = ENERGY.NONE;
		emotion = EMOTION.NONE;
		

		All_Predicate_History = new ArrayList<FramePredicates>();
		
		transforms = new ArrayList<TransformPredicateInterface>();
	}

	public Gesture(Boolean isMultiFrame, 
	               DURATION avgDuration, 
	               TEMPO avgTempo, 
	               LEFT_LEG_STILL avgLeftLegStillness, 
	               RIGHT_LEG_STILL avgRightLegStillness, 
	               LEFT_HAND_STILL avgLeftHandStillness, 
	               RIGHT_HAND_STILL avgRightHandStillness,  
	               LEFT_LEG_TRANSVERSE avgLeftLegTransverse, 
	               RIGHT_LEG_TRANSVERSE avgRightLegTransverse, 
	               LEFT_HAND_TRANSVERSE avgLeftHandTransverse, 
	               RIGHT_HAND_TRANSVERSE avgRightHandTransverse,  
	               LEFT_LEG_LONGITUDINAL avgLeftLegLongitudinal, 
	               RIGHT_LEG_LONGITUDINAL avgRightLegLongitudinal, 
	               LEFT_HAND_LONGITUDINAL avgLeftHandLongitudinal, 
	               RIGHT_HAND_LONGITUDINAL avgRightHandLongitudinal,  
	               LEFT_LEG_VERTICAL avgLeftLegVertical, 
	               RIGHT_LEG_VERTICAL avgRightLegVertical, 
	               LEFT_HAND_VERTICAL avgLeftHandVertical, 
	               RIGHT_HAND_VERTICAL avgRightHandVertical, 
	               SMOOTHNESS avgSmoothness, 
	               ENERGY avgEnergy, 
	               ArrayList<FramePredicates> All_Predicate_History, 
	               ArrayList<TransformPredicateInterface> transforms,
	               EMOTION emotion)
	{
		this.avgDuration = avgDuration;

		this.avgTempo = avgTempo;
		this.avgLeftLegStillness = avgLeftLegStillness;
		this.avgRightLegStillness = avgRightLegStillness;
		this.avgLeftHandStillness = avgLeftHandStillness;
		this.avgRightHandStillness = avgRightHandStillness;
		this.avgLeftLegTransverse = avgLeftLegTransverse;
		this.avgRightLegTransverse = avgRightLegTransverse;
		this.avgLeftHandTransverse = avgLeftHandTransverse;
		this.avgRightHandTransverse = avgRightHandTransverse;
		this.avgLeftLegLongitudinal = avgLeftLegLongitudinal;
		this.avgRightLegLongitudinal = avgRightLegLongitudinal;
		this.avgLeftHandLongitudinal = avgLeftHandLongitudinal;
		this.avgRightHandLongitudinal = avgRightHandLongitudinal;
		this.avgLeftLegVertical = avgLeftLegVertical;
		this.avgRightLegVertical = avgRightLegVertical;
		this.avgLeftHandVertical = avgLeftHandVertical;
		this.avgRightHandVertical = avgRightHandVertical;
		this.avgSmoothness = avgSmoothness;
		this.avgEnergy = avgEnergy;
		this.emotion = emotion;

		this.All_Predicate_History = new ArrayList<FramePredicates>(All_Predicate_History);

		this.transforms = new ArrayList<TransformPredicateInterface>((transforms == null) ? (new ArrayList<TransformPredicateInterface>()) : transforms);

		if(!(this.isMultiFrame = isMultiFrame))
		{
			this.isMultiFrame = makeMultiFrame();
		}
	}
	
	public Gesture(ArrayList<TransformPredicateInterface> transforms, 
			       ArrayList<FramePredicates> predHist, 
			       ArrayList<TempoPredicate> tempoPredHist, 
			       ArrayList<LeftLegStillnessPredicate> leftLegStillnessPredHist,
			       ArrayList<RightLegStillnessPredicate> rightLegStillnessPredHist, 
			       ArrayList<LeftHandStillnessPredicate> leftHandStillnessPredHist, 
			       ArrayList<RightHandStillnessPredicate> rightHandStillnessPredHist, 
			       ArrayList<EnergyPredicate> energyPredHist, 
			       ArrayList<SmoothnessPredicate> smoothPredHist,
	               ArrayList<LeftLegTransversePredicate> leftLegTransversePredHist,
	               ArrayList<RightLegTransversePredicate> rightLegTransversePredHist,
	               ArrayList<LeftHandTransversePredicate> leftHandTransversePredHist,
	               ArrayList<RightHandTransversePredicate> rightHandTransversePredHist,
	               ArrayList<LeftLegLongitudinalPredicate> leftLegLongitudinalPredHist,
	               ArrayList<RightLegLongitudinalPredicate> rightLegLongitudinalPredHist,
	               ArrayList<LeftHandLongitudinalPredicate> leftHandLongitudinalPredHist,
	               ArrayList<RightHandLongitudinalPredicate> rightHandLongitudinalPredHist,
	               ArrayList<LeftLegVerticalPredicate> leftLegVerticalPredHist,
	               ArrayList<RightLegVerticalPredicate> rightLegVerticalPredHist,
	               ArrayList<LeftHandVerticalPredicate> leftHandVerticalPredHist,
	               ArrayList<RightHandVerticalPredicate> rightHandVerticalPredHist) {
		if (predHist.size() != tempoPredHist.size() || tempoPredHist.size() != energyPredHist.size() || energyPredHist.size() != smoothPredHist.size()) {
			throw new Error("Gesture constructor: predicate histories do not have same length!"); 
		}
		
		this.transforms = new ArrayList<TransformPredicateInterface>((transforms == null) ? (new ArrayList<TransformPredicateInterface>()) : transforms);
		
		All_Predicate_History = new ArrayList<FramePredicates>(predHist);
		
		this.isMultiFrame = true;
		
		emotion = EMOTION.NONE;
		
		avgDuration = (DURATION) new DurationPredicate(predHist.size()).getPredValue();
		float avgTempoVal = ViewpointPredicate.predicateRunningAvg(tempoPredHist, tempoPredHist.size()-1);
		avgTempo = (TEMPO) new TempoPredicate(avgTempoVal).getPredValue();

		float avgleftLegStillnessVal = ViewpointPredicate.predicateRunningAvg(leftLegStillnessPredHist, leftLegStillnessPredHist.size()-1);
		avgLeftLegStillness = (LEFT_LEG_STILL) new LeftLegStillnessPredicate(avgleftLegStillnessVal).getPredValue();	
		
		float avgrightLegStillnessVal = ViewpointPredicate.predicateRunningAvg(rightLegStillnessPredHist, rightLegStillnessPredHist.size()-1);
		avgRightLegStillness = (RIGHT_LEG_STILL) new RightLegStillnessPredicate(avgrightLegStillnessVal).getPredValue();	

		float avgleftHandStillnessVal = ViewpointPredicate.predicateRunningAvg(leftHandStillnessPredHist, leftHandStillnessPredHist.size()-1);
		avgLeftHandStillness = (LEFT_HAND_STILL) new LeftHandStillnessPredicate(avgleftHandStillnessVal).getPredValue();	
		
		float avgrightHandStillnessVal = ViewpointPredicate.predicateRunningAvg(rightHandStillnessPredHist, rightHandStillnessPredHist.size()-1);
		avgRightHandStillness = (RIGHT_HAND_STILL) new RightHandStillnessPredicate(avgrightHandStillnessVal).getPredValue();	

		float avgleftLegTransverseVal = ViewpointPredicate.predicateRunningAvg(leftLegTransversePredHist, leftLegTransversePredHist.size()-1);
		avgLeftLegTransverse = (LEFT_LEG_TRANSVERSE) new LeftLegTransversePredicate(avgleftLegTransverseVal).getPredValue();	
		
		float avgrightLegTransverseVal = ViewpointPredicate.predicateRunningAvg(rightLegTransversePredHist, rightLegTransversePredHist.size()-1);
		avgRightLegTransverse = (RIGHT_LEG_TRANSVERSE) new RightLegTransversePredicate(avgrightLegTransverseVal).getPredValue();	

		float avgleftHandTransverseVal = ViewpointPredicate.predicateRunningAvg(leftHandTransversePredHist, leftHandTransversePredHist.size()-1);
		avgLeftHandTransverse = (LEFT_HAND_TRANSVERSE) new LeftHandTransversePredicate(avgleftHandTransverseVal).getPredValue();	
		
		float avgrightHandTransverseVal = ViewpointPredicate.predicateRunningAvg(rightHandTransversePredHist, rightHandTransversePredHist.size()-1);
		avgRightHandTransverse = (RIGHT_HAND_TRANSVERSE) new RightHandTransversePredicate(avgrightHandTransverseVal).getPredValue();	

		float avgleftLegLongitudinalVal = ViewpointPredicate.predicateRunningAvg(leftLegLongitudinalPredHist, leftLegLongitudinalPredHist.size()-1);
		avgLeftLegLongitudinal = (LEFT_LEG_LONGITUDINAL) new LeftLegLongitudinalPredicate(avgleftLegLongitudinalVal).getPredValue();	
		
		float avgrightLegLongitudinalVal = ViewpointPredicate.predicateRunningAvg(rightLegLongitudinalPredHist, rightLegLongitudinalPredHist.size()-1);
		avgRightLegLongitudinal = (RIGHT_LEG_LONGITUDINAL) new RightLegLongitudinalPredicate(avgrightLegLongitudinalVal).getPredValue();	

		float avgleftHandLongitudinalVal = ViewpointPredicate.predicateRunningAvg(leftHandLongitudinalPredHist, leftHandLongitudinalPredHist.size()-1);
		avgLeftHandLongitudinal = (LEFT_HAND_LONGITUDINAL) new LeftHandLongitudinalPredicate(avgleftHandLongitudinalVal).getPredValue();	
		
		float avgrightHandLongitudinalVal = ViewpointPredicate.predicateRunningAvg(rightHandLongitudinalPredHist, rightHandLongitudinalPredHist.size()-1);
		avgRightHandLongitudinal = (RIGHT_HAND_LONGITUDINAL) new RightHandLongitudinalPredicate(avgrightHandLongitudinalVal).getPredValue();	

		float avgleftLegVerticalVal = ViewpointPredicate.predicateRunningAvg(leftLegVerticalPredHist, leftLegVerticalPredHist.size()-1);
		avgLeftLegVertical = (LEFT_LEG_VERTICAL) new LeftLegVerticalPredicate(avgleftLegVerticalVal).getPredValue();	
		
		float avgrightLegVerticalVal = ViewpointPredicate.predicateRunningAvg(rightLegVerticalPredHist, rightLegVerticalPredHist.size()-1);
		avgRightLegVertical = (RIGHT_LEG_VERTICAL) new RightLegVerticalPredicate(avgrightLegVerticalVal).getPredValue();	

		float avgleftHandVerticalVal = ViewpointPredicate.predicateRunningAvg(leftHandVerticalPredHist, leftHandVerticalPredHist.size()-1);
		avgLeftHandVertical = (LEFT_HAND_VERTICAL) new LeftHandVerticalPredicate(avgleftHandVerticalVal).getPredValue();	
		
		float avgrightHandVerticalVal = ViewpointPredicate.predicateRunningAvg(rightHandVerticalPredHist, rightHandVerticalPredHist.size()-1);
		avgRightHandVertical = (RIGHT_HAND_VERTICAL) new RightHandVerticalPredicate(avgrightHandVerticalVal).getPredValue();	
		
		float avgEnergyVal = ViewpointPredicate.predicateRunningAvg(energyPredHist, energyPredHist.size()-1);
		avgEnergy = (ENERGY) new EnergyPredicate(avgEnergyVal).getPredValue();
		
		float avgSmoothVal = ViewpointPredicate.predicateRunningAvg(smoothPredHist, smoothPredHist.size()-1);
		avgSmoothness = (SMOOTHNESS) new SmoothnessPredicate(avgSmoothVal).getPredValue();

		// TODO: [cf MJ]: not sure what should replace this
		if(!this.All_Predicate_History.isEmpty() && !this.isMultiFrame)
		{
			isMultiFrame = makeMultiFrame();
		}
	}

	public TEMPO calcAvgTempo(ArrayList<Float> tempoValHist) {
		float avgTempoVal = calcAvgValue(tempoValHist);
		TEMPO out = Predicates.classifyTempo(avgTempoVal);
		return out;
	}

	public ENERGY calcAvgEnergy(ArrayList<Float> energyValHist) {
		float avgEnergyVal = calcAvgValue(energyValHist);
		ENERGY out = Predicates.classifyEnergy(avgEnergyVal);
		return out;
	}

	public SMOOTHNESS calcAvgSmoothness(ArrayList<Float> smoothValHist) {
		float avgSmoothVal = calcAvgValue(smoothValHist);
		SMOOTHNESS out = Predicates.classifySmoothness(avgSmoothVal);
		return out;
	}

	public static Float calcAvgValue(ArrayList<Float> floatVals) {
		float outVal = 0f;
		for (int i=0; i<floatVals.size(); i++) {
			outVal += floatVals.get(i);
		}
		outVal /= floatVals.size();
		return outVal;
	}

	//TODO
	public Boolean makeMultiFrame()
	{

		return false;
	}

	public Boolean getIsMultiFrame() {
		return isMultiFrame;
	}

	public DURATION getAvgDuration() {
		return avgDuration;
	}

	public TEMPO getAvgTempo() {
		return avgTempo;
	}

	public LEFT_LEG_STILL getAvgLeftLegStillness() {
		return avgLeftLegStillness;
	}

	public RIGHT_LEG_STILL getAvgRightLegStillness() {
		return avgRightLegStillness;
	}

	public void setAvgRightLegStillness(RIGHT_LEG_STILL avgRightLegStillness) {
		this.avgRightLegStillness = avgRightLegStillness;
	}

	public LEFT_HAND_STILL getAvgLeftHandStillness() {
		return avgLeftHandStillness;
	}

	public LEFT_LEG_TRANSVERSE getAvgLeftLegTransverse() {
		return avgLeftLegTransverse;
	}

	public void setAvgLeftLegTransverse(LEFT_LEG_TRANSVERSE avgLeftLegTransverse) {
		this.avgLeftLegTransverse = avgLeftLegTransverse;
	}

	public RIGHT_LEG_TRANSVERSE getAvgRightLegTransverse() {
		return avgRightLegTransverse;
	}

	public void setAvgRightLegTransverse(RIGHT_LEG_TRANSVERSE avgRightLegTransverse) {
		this.avgRightLegTransverse = avgRightLegTransverse;
	}

	public LEFT_HAND_TRANSVERSE getAvgLeftHandTransverse() {
		return avgLeftHandTransverse;
	}

	public void setAvgLeftHandTransverse(LEFT_HAND_TRANSVERSE avgLeftHandTransverse) {
		this.avgLeftHandTransverse = avgLeftHandTransverse;
	}

	public RIGHT_HAND_TRANSVERSE getAvgRightHandTransverse() {
		return avgRightHandTransverse;
	}

	public void setAvgRightHandTransverse(
			RIGHT_HAND_TRANSVERSE avgRightHandTransverse) {
		this.avgRightHandTransverse = avgRightHandTransverse;
	}

	public LEFT_LEG_LONGITUDINAL getAvgLeftLegLongitudinal() {
		return avgLeftLegLongitudinal;
	}

	public void setAvgLeftLegLongitudinal(
			LEFT_LEG_LONGITUDINAL avgLeftLegLongitudinal) {
		this.avgLeftLegLongitudinal = avgLeftLegLongitudinal;
	}

	public RIGHT_LEG_LONGITUDINAL getAvgRightLegLongitudinal() {
		return avgRightLegLongitudinal;
	}

	public void setAvgRightLegLongitudinal(
			RIGHT_LEG_LONGITUDINAL avgRightLegLongitudinal) {
		this.avgRightLegLongitudinal = avgRightLegLongitudinal;
	}

	public LEFT_HAND_LONGITUDINAL getAvgLeftHandLongitudinal() {
		return avgLeftHandLongitudinal;
	}

	public void setAvgLeftHandLongitudinal(
			LEFT_HAND_LONGITUDINAL avgLeftHandLongitudinal) {
		this.avgLeftHandLongitudinal = avgLeftHandLongitudinal;
	}

	public RIGHT_HAND_LONGITUDINAL getAvgRightHandLongitudinal() {
		return avgRightHandLongitudinal;
	}

	public void setAvgRightHandLongitudinal(
			RIGHT_HAND_LONGITUDINAL avgRightHandLongitudinal) {
		this.avgRightHandLongitudinal = avgRightHandLongitudinal;
	}

	public LEFT_LEG_VERTICAL getAvgLeftLegVertical() {
		return avgLeftLegVertical;
	}

	public void setAvgLeftLegVertical(LEFT_LEG_VERTICAL avgLeftLegVertical) {
		this.avgLeftLegVertical = avgLeftLegVertical;
	}

	public RIGHT_LEG_VERTICAL getAvgRightLegVertical() {
		return avgRightLegVertical;
	}

	public void setAvgRightLegVertical(RIGHT_LEG_VERTICAL avgRightLegVertical) {
		this.avgRightLegVertical = avgRightLegVertical;
	}

	public LEFT_HAND_VERTICAL getAvgLeftHandVertical() {
		return avgLeftHandVertical;
	}

	public void setAvgLeftHandVertical(LEFT_HAND_VERTICAL avgLeftHandVertical) {
		this.avgLeftHandVertical = avgLeftHandVertical;
	}

	public RIGHT_HAND_VERTICAL getAvgRightHandVertical() {
		return avgRightHandVertical;
	}

	public void setAvgRightHandVertical(RIGHT_HAND_VERTICAL avgRightHandVertical) {
		this.avgRightHandVertical = avgRightHandVertical;
	}

	public void setAvgLeftHandStillness(LEFT_HAND_STILL avgLeftHandStillness) {
		this.avgLeftHandStillness = avgLeftHandStillness;
	}

	public RIGHT_HAND_STILL getAvgRightHandStillness() {
		return avgRightHandStillness;
	}

	public void setAvgRightHandStillness(RIGHT_HAND_STILL avgRightHandStillness) {
		this.avgRightHandStillness = avgRightHandStillness;
	}

	public SMOOTHNESS getAvgSmoothness() {
		return avgSmoothness;
	}

	public ENERGY getAvgEnergy() {
		return avgEnergy;
	}

	public ArrayList<FramePredicates> getAll_Predicate_History() {
		return All_Predicate_History;
	}

	public void setIsMultiFrame(Boolean isMultiFrame) {
		this.isMultiFrame = isMultiFrame;
	}

	public void setAvgDuration(DURATION avgDuration) {
		this.avgDuration = avgDuration;
	}

	public void setAvgTempo(TEMPO avgTempo) {
		this.avgTempo = avgTempo;
	}
	
	public void setAvgLeftLegStillness(LEFT_LEG_STILL avgLeftLegStillness) {
		this.avgLeftLegStillness = avgLeftLegStillness;
	}

	public void setAvgSmoothness(SMOOTHNESS avgSmoothness) {
		this.avgSmoothness = avgSmoothness;
	}

	public void setAvgEnergy(ENERGY avgEnergy) {
		this.avgEnergy = avgEnergy;
	}

	public void setAll_Predicate_History(
			ArrayList<FramePredicates> all_Predicate_History) {
		All_Predicate_History = all_Predicate_History;
	}

	public void setTransforms(
			ArrayList<TransformPredicateInterface> transforms) {
		this.transforms = new 
				ArrayList<TransformPredicates.TransformPredicateInterface>(
						transforms);
	}
	
	public EMOTION getEmotion() {
		return emotion;
	}

	public void setEmotion(EMOTION emotion) {
		this.emotion = emotion;
	}

	public void setEmotionFromString(String listEmotions) {
		String[] emotions = listEmotions.split("\\s+");
		if (emotions.length == 0 || emotions[0].equals("none")) {
			this.emotion = EMOTION.NONE;
		} else if (EMOTION.ANGRY.toString().toLowerCase().equals(emotions[0])) {
			this.emotion = EMOTION.ANGRY;
		} else if (EMOTION.JOY.toString().toLowerCase().equals(emotions[0])) {
			this.emotion = EMOTION.JOY;
		} else if (EMOTION.SAD.toString().toLowerCase().equals(emotions[0])) {
			this.emotion = EMOTION.SAD;
		} else if (EMOTION.FEAR.toString().toLowerCase().equals(emotions[0])) {
			this.emotion = EMOTION.FEAR;
		}
		//System.out.println("VAI Will check the following string :" + listEmotions);
		//System.out.println("VAI GOT THE FOLLOWING EMOTION :" + this.emotion);
	}
	
	@Override
	public String toString() {
		//System.err.println("got a gesture tostring");
		return this.outString();
	}

	//Replace with an overload of Object.toString()
	public String outString() {
		//System.err.println("making an outstring");
		
		String outStr = "GESTURE:\n";
		outStr += "duration =\t\t" + this.avgDuration.name() + "\n";
		outStr += "avgTempo =\t\t" + this.avgTempo.name() + "\n";
		outStr += "avgLeftLegStillness =\t\t" + this.avgLeftLegStillness.name() + "\n";
		outStr += "avgRightLegStillness =\t\t" + this.avgRightLegStillness.name() + "\n";
		outStr += "avgLeftHandStillness =\t\t" + this.avgLeftHandStillness.name() + "\n";
		outStr += "avgRightHandStillness =\t\t" + this.avgRightHandStillness.name() + "\n";
		outStr += "avgLeftLegTransverse =\t\t" + this.avgLeftLegTransverse.name() + "\n";
		outStr += "avgRightLegTransverse =\t\t" + this.avgRightLegTransverse.name() + "\n";
		outStr += "avgLeftHandTransverse =\t\t" + this.avgLeftHandTransverse.name() + "\n";
		outStr += "avgRightHandTransverse =\t\t" + this.avgRightHandTransverse.name() + "\n";
		outStr += "avgLeftLegLongitudinal =\t\t" + this.avgLeftLegLongitudinal.name() + "\n";
		outStr += "avgRightLegLongitudinal =\t\t" + this.avgRightLegLongitudinal.name() + "\n";
		outStr += "avgLeftHandLongitudinal =\t\t" + this.avgLeftHandLongitudinal.name() + "\n";
		outStr += "avgRightHandLongitudinal =\t\t" + this.avgRightHandLongitudinal.name() + "\n";
		outStr += "avgLeftLegVertical =\t\t" + this.avgLeftLegVertical.name() + "\n";
		outStr += "avgRightLegVertical =\t\t" + this.avgRightLegVertical.name() + "\n";
		outStr += "avgLeftHandVertical =\t\t" + this.avgLeftHandVertical.name() + "\n";
		outStr += "avgRightHandVertical =\t\t" + this.avgRightHandVertical.name() + "\n";
		outStr += "avgEnergy = \t\t" + this.avgEnergy.name() + "\n";
		outStr += "avgSmoothness = \t" + this.avgSmoothness.name() + "\n";
		outStr += "emotion = \t" + this.emotion.name() + "\n";

		outStr += "ALL_PREDICATE_HIST: ";
		for (int i=0; i<this.All_Predicate_History.size(); i++){
			outStr += "\n[" + i + "]{ ";
			FramePredicates predT = this.All_Predicate_History.get(i);
			outStr += predT.outString();
			outStr += "}, ";
		}
		
		//TODO - Fix to parse with fromString()
		outStr += "\nTRANSFORMS: { ";
		for (int i=0; i<this.transforms.size(); i++){
			TransformPredicateInterface TransPreds = this.transforms.get(i);
			outStr += TransPreds.getClass() + "=";
			outStr += TransPreds.toString();
			outStr += ",";
		}
		outStr += "}";

//		System.out.println("Average Left leg Stillness = "+avgLeftLegStillness);
//		System.out.println("Average Right leg Stillness = "+avgRightLegStillness);
//		System.out.println("Average Left hand Stillness = "+avgLeftHandStillness);
//		System.out.println("Average Right hand Stillness = "+avgRightHandStillness);
//		System.out.println("Average Left leg Transverse = "+avgLeftLegTransverse);
//		System.out.println("Average Right leg Transverse = "+avgRightLegTransverse);
//		System.out.println("Average Left hand Transverse = "+avgLeftHandTransverse);
//		System.out.println("Average Right hand Transverse = "+avgRightHandTransverse);
//		System.out.println("Average Left leg Longitudinal = "+avgLeftLegLongitudinal);
//		System.out.println("Average Right leg Longitudinal = "+avgRightLegLongitudinal);
//		System.out.println("Average Left hand Longitudinal = "+avgLeftHandLongitudinal);
//		System.out.println("Average Right hand Longitudinal = "+avgRightHandLongitudinal);
//		System.out.println("Average Left leg Vertical = "+avgLeftLegVertical);
//		System.out.println("Average Right leg Vertical = "+avgRightLegVertical);
//		System.out.println("Average Left hand Vertical = "+avgLeftHandVertical);
//		System.out.println("Average Right hand Vertical = "+avgRightHandVertical);
		return outStr;
	}
	
	/**
	 * Converts a gesture string provided by the outString() method into a gesture
	 * @param gestureString
	 * @return
	 */
	public static Gesture fromString(String gestureString)
	{
		if(gestureString.equals(""))
		{
			return null;//new Gesture();
		}
		
		String[] gestStrings = gestureString.split("\n");
//		String outString = "";
		
		Pattern durationPattern = Pattern.compile("duration =[\\s]+([\\w]+)");
		Pattern tempoPattern = Pattern.compile("avgTempo =[\\s]+([\\w]+)");
		Pattern leftLegStillnessPattern = Pattern.compile("avgLeftLegStillness =[\\s]+([\\w]+)");
		Pattern rightLegStillnessPattern = Pattern.compile("avgRightLegStillness =[\\s]+([\\w]+)");
		Pattern leftHandStillnessPattern = Pattern.compile("avgLeftHandStillness =[\\s]+([\\w]+)");
		Pattern rightHandStillnessPattern = Pattern.compile("avgRightHandStillness =[\\s]+([\\w]+)");
		Pattern leftLegTransversePattern = Pattern.compile("avgLeftLegTransverse =[\\s]+([\\w]+)");
		Pattern rightLegTransversePattern = Pattern.compile("avgRightLegTransverse =[\\s]+([\\w]+)");
		Pattern leftHandTransversePattern = Pattern.compile("avgLeftHandTransverse =[\\s]+([\\w]+)");
		Pattern rightHandTransversePattern = Pattern.compile("avgRightHandTransverse =[\\s]+([\\w]+)");
		Pattern leftLegLongitudinalPattern = Pattern.compile("avgLeftLegLongitudinal =[\\s]+([\\w]+)");
		Pattern rightLegLongitudinalPattern = Pattern.compile("avgRightLegLongitudinal =[\\s]+([\\w]+)");
		Pattern leftHandLongitudinalPattern = Pattern.compile("avgLeftHandLongitudinal =[\\s]+([\\w]+)");
		Pattern rightHandLongitudinalPattern = Pattern.compile("avgRightHandLongitudinal =[\\s]+([\\w]+)");
		Pattern leftLegVerticalPattern = Pattern.compile("avgLeftLegVertical =[\\s]+([\\w]+)");
		Pattern rightLegVerticalPattern = Pattern.compile("avgRightLegVertical =[\\s]+([\\w]+)");
		Pattern leftHandVerticalPattern = Pattern.compile("avgLeftHandVertical =[\\s]+([\\w]+)");
		Pattern rightHandVerticalPattern = Pattern.compile("avgRightHandVertical =[\\s]+([\\w]+)");
		Pattern energyPattern = Pattern.compile("avgEnergy =[\\s]+([\\w]+)");
		Pattern smoothPattern = Pattern.compile("avgSmoothness =[\\s]+([\\w]+)");
		Pattern emotionPattern = Pattern.compile("emotion =[\\s]+([\\w]+)");
		Pattern predicateNumberPattern = Pattern.compile("\\[([\\d]+)\\]\\{");
		Pattern frameAndTransformPredicatePattern = Pattern.compile("\\$([\\w\\_]+)=([\\w\\_]+),");
		
		Matcher durationMatcher, 
		        tempoMatcher, 
		        leftLegStillnessMatcher, 
		        rightLegStillnessMatcher, 
		        leftHandStillnessMatcher, 
		        rightHandStillnessMatcher, 
		        leftLegTransverseMatcher, 
		        rightLegTransverseMatcher, 
		        leftHandTransverseMatcher, 
		        rightHandTransverseMatcher,
		        leftLegLongitudinalMatcher, 
		        rightLegLongitudinalMatcher, 
		        leftHandLongitudinalMatcher, 
		        rightHandLongitudinalMatcher, 
		        leftLegVerticalMatcher, 
		        rightLegVerticalMatcher, 
		        leftHandVerticalMatcher, 
		        rightHandVerticalMatcher, 
		        energyMatcher, 
		        smoothMatcher,
		        emotionMatcher,
		        predNumMatcher, 
		        frameAndTransformPredMatcher;
		
		DURATION duration = DURATION.NONE;
		TEMPO avgTempo = TEMPO.NONE;
		LEFT_LEG_STILL avgLeftLegStillness = LEFT_LEG_STILL.FALSE;
		LEFT_HAND_STILL avgLeftHandStillness = LEFT_HAND_STILL.FALSE;	
		RIGHT_LEG_STILL avgRightLegStillness = RIGHT_LEG_STILL.FALSE;	
		RIGHT_HAND_STILL avgRightHandStillness = RIGHT_HAND_STILL.FALSE;	
		LEFT_LEG_TRANSVERSE avgLeftLegTransverse = LEFT_LEG_TRANSVERSE.FALSE;
		LEFT_HAND_TRANSVERSE avgLeftHandTransverse = LEFT_HAND_TRANSVERSE.FALSE;	
		RIGHT_LEG_TRANSVERSE avgRightLegTransverse = RIGHT_LEG_TRANSVERSE.FALSE;	
		RIGHT_HAND_TRANSVERSE avgRightHandTransverse = RIGHT_HAND_TRANSVERSE.FALSE;	
		LEFT_LEG_LONGITUDINAL avgLeftLegLongitudinal = LEFT_LEG_LONGITUDINAL.FALSE;
		LEFT_HAND_LONGITUDINAL avgLeftHandLongitudinal = LEFT_HAND_LONGITUDINAL.FALSE;	
		RIGHT_LEG_LONGITUDINAL avgRightLegLongitudinal = RIGHT_LEG_LONGITUDINAL.FALSE;	
		RIGHT_HAND_LONGITUDINAL avgRightHandLongitudinal = RIGHT_HAND_LONGITUDINAL.FALSE;	
		LEFT_LEG_VERTICAL avgLeftLegVertical = LEFT_LEG_VERTICAL.FALSE;
		LEFT_HAND_VERTICAL avgLeftHandVertical = LEFT_HAND_VERTICAL.FALSE;	
		RIGHT_LEG_VERTICAL avgRightLegVertical = RIGHT_LEG_VERTICAL.FALSE;	
		RIGHT_HAND_VERTICAL avgRightHandVertical = RIGHT_HAND_VERTICAL.FALSE;	
		ENERGY avgEnergy = ENERGY.NONE;
		SMOOTHNESS avgSmooth = SMOOTHNESS.NONE;
		EMOTION emotion = EMOTION.NONE;
		int frameIdx = 0;
		ArrayList<FramePredicates> framePredHist = new ArrayList<FramePredicates>();
		FramePredicates addFramePred = null;
		ArrayList<TransformPredicateInterface> transforms = new ArrayList<TransformPredicates.TransformPredicateInterface>();
		
		for (int i=0; i<gestStrings.length; i++) {
			addFramePred = new FramePredicates();
			
			durationMatcher = durationPattern.matcher(gestStrings[i]);
			tempoMatcher = tempoPattern.matcher(gestStrings[i]);
			leftLegStillnessMatcher = leftLegStillnessPattern.matcher(gestStrings[i]);
			leftHandStillnessMatcher = leftHandStillnessPattern.matcher(gestStrings[i]);
			rightLegStillnessMatcher = rightLegStillnessPattern.matcher(gestStrings[i]);
			rightHandStillnessMatcher = rightHandStillnessPattern.matcher(gestStrings[i]);
			leftLegTransverseMatcher = leftLegTransversePattern.matcher(gestStrings[i]);
			leftHandTransverseMatcher = leftHandTransversePattern.matcher(gestStrings[i]);
			rightLegTransverseMatcher = rightLegTransversePattern.matcher(gestStrings[i]);
			rightHandTransverseMatcher = rightHandTransversePattern.matcher(gestStrings[i]);
			leftLegLongitudinalMatcher = leftLegLongitudinalPattern.matcher(gestStrings[i]);
			leftHandLongitudinalMatcher = leftHandLongitudinalPattern.matcher(gestStrings[i]);
			rightLegLongitudinalMatcher = rightLegLongitudinalPattern.matcher(gestStrings[i]);
			rightHandLongitudinalMatcher = rightHandLongitudinalPattern.matcher(gestStrings[i]);
			leftLegVerticalMatcher = leftLegVerticalPattern.matcher(gestStrings[i]);
			leftHandVerticalMatcher = leftHandVerticalPattern.matcher(gestStrings[i]);
			rightLegVerticalMatcher = rightLegVerticalPattern.matcher(gestStrings[i]);
			rightHandVerticalMatcher = rightHandVerticalPattern.matcher(gestStrings[i]);
			energyMatcher = energyPattern.matcher(gestStrings[i]);
			smoothMatcher = smoothPattern.matcher(gestStrings[i]);
			predNumMatcher = predicateNumberPattern.matcher(gestStrings[i]);
			frameAndTransformPredMatcher = frameAndTransformPredicatePattern.matcher(gestStrings[i]);
			emotionMatcher = emotionPattern.matcher(gestStrings[i]);
			
			if (emotionMatcher.find()) {
				//System.out.println("gesture has duration:\t" + durationMatcher.group(1));
				emotion = EMOTION.valueOf(emotionMatcher.group(1));
			}
			if (durationMatcher.find()) {
				//System.out.println("gesture has duration:\t" + durationMatcher.group(1));
				duration = DURATION.valueOf(durationMatcher.group(1));
			}
			if (tempoMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgTempo = TEMPO.valueOf(tempoMatcher.group(1)); 
			}
			if (leftLegStillnessMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftLegStillness = LEFT_LEG_STILL.valueOf(leftLegStillnessMatcher.group(1)); 
			}
			if (leftHandStillnessMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftHandStillness = LEFT_HAND_STILL.valueOf(leftHandStillnessMatcher.group(1)); 
			}
			if (rightLegStillnessMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightLegStillness = RIGHT_LEG_STILL.valueOf(rightLegStillnessMatcher.group(1)); 
			}
			if (rightHandStillnessMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightHandStillness = RIGHT_HAND_STILL.valueOf(rightHandStillnessMatcher.group(1)); 
			}
			if (leftLegTransverseMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftLegTransverse = LEFT_LEG_TRANSVERSE.valueOf(leftLegTransverseMatcher.group(1)); 
			}
			if (leftHandTransverseMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftHandTransverse = LEFT_HAND_TRANSVERSE.valueOf(leftHandTransverseMatcher.group(1)); 
			}
			if (rightLegTransverseMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightLegTransverse = RIGHT_LEG_TRANSVERSE.valueOf(rightLegTransverseMatcher.group(1)); 
			}
			if (rightHandTransverseMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightHandTransverse = RIGHT_HAND_TRANSVERSE.valueOf(rightHandTransverseMatcher.group(1)); 
			}			
			if (leftLegLongitudinalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftLegLongitudinal = LEFT_LEG_LONGITUDINAL.valueOf(leftLegLongitudinalMatcher.group(1)); 
			}
			if (leftHandLongitudinalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftHandLongitudinal = LEFT_HAND_LONGITUDINAL.valueOf(leftHandLongitudinalMatcher.group(1)); 
			}
			if (rightLegLongitudinalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightLegLongitudinal = RIGHT_LEG_LONGITUDINAL.valueOf(rightLegLongitudinalMatcher.group(1)); 
			}
			if (rightHandLongitudinalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightHandLongitudinal = RIGHT_HAND_LONGITUDINAL.valueOf(rightHandLongitudinalMatcher.group(1)); 
			}
			if (leftLegVerticalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftLegVertical = LEFT_LEG_VERTICAL.valueOf(leftLegVerticalMatcher.group(1)); 
			}
			if (leftHandVerticalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgLeftHandVertical = LEFT_HAND_VERTICAL.valueOf(leftHandVerticalMatcher.group(1)); 
			}
			if (rightLegVerticalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightLegVertical = RIGHT_LEG_VERTICAL.valueOf(rightLegVerticalMatcher.group(1)); 
			}
			if (rightHandVerticalMatcher.find()) {
				//System.out.println("gesture has tempo:\t" + tempoMatcher.group(1));
				avgRightHandVertical = RIGHT_HAND_VERTICAL.valueOf(rightHandVerticalMatcher.group(1)); 
			}
			if (energyMatcher.find()) {
				//System.out.println("gesture has energy:\t" + energyMatcher.group(1));
				avgEnergy = ENERGY.valueOf(energyMatcher.group(1));
			}
			if (smoothMatcher.find()) {
				//System.out.println("gesture has smooth:\t" + smoothMatcher.group(1));
				avgSmooth = SMOOTHNESS.valueOf(smoothMatcher.group(1));
			}
			if (predNumMatcher.find()) {
				//System.out.println("gesture has predicate number:\t" + predNumMatcher.group(1));
				frameIdx = Integer.parseInt(predNumMatcher.group(1));
			}
			while (frameAndTransformPredMatcher.find()) {
//				outString += framePredMatcher.group(1) + " : " + framePredMatcher.group(2) + "; ";
				String enumName = frameAndTransformPredMatcher.group(1);
				String enumValue = frameAndTransformPredMatcher.group(2);
				if (enumName.equals("DURATION")) {
					addFramePred.setMultiFrameDuration(DURATION.valueOf(enumValue));
				} else if (enumName.equals("TEMPO")) {
					addFramePred.setFrameTempo(TEMPO.valueOf(enumValue));
				} else if (enumName.equals("SMOOTHNESS")) {
					addFramePred.setFrameSmoothness(SMOOTHNESS.valueOf(enumValue));
				} else if (enumName.equals("ENERGY")) {
					addFramePred.setFrameEnergy(ENERGY.valueOf(enumValue));
				} else if (enumName.equals("LEFTARM_CURVE")) {
					addFramePred.setFrameLeftArmCurve(LEFTARM_CURVE.valueOf(enumValue));
				} else if (enumName.equals("LEFTHAND_POS")) {
					addFramePred.setFrameLeftHandPos(LEFTHAND_POS.valueOf(enumValue));
				} else if (enumName.equals("LEFTHAND_HEIGHT")) {
					addFramePred.setFrameLeftHandHeight(LEFTHAND_HEIGHT.valueOf(enumValue));
				} else if (enumName.equals("RIGHTARM_CURVE")) {
					addFramePred.setFrameRightArmCurve(RIGHTARM_CURVE.valueOf(enumValue));
				} else if (enumName.equals("RIGHTHAND_POS")) {
					addFramePred.setFrameRightHandPos(RIGHTHAND_POS.valueOf(enumValue));
				} else if (enumName.equals("RIGHTHAND_HEIGHT")) {
					addFramePred.setFrameRightHandHeight(RIGHTHAND_HEIGHT.valueOf(enumValue));
				} else if (enumName.equals("BOTHARM_CURVE")) {
					addFramePred.setFrameBothArmCurve(BOTHARM_CURVE.valueOf(enumValue));
				} else if (enumName.equals("LEFTLEG_CURVE")) {
					addFramePred.setFrameLeftLegCurve(LEFTLEG_CURVE.valueOf(enumValue));
				} else if (enumName.equals("RIGHTLEG_CURVE")) {
					addFramePred.setFrameRightLegCurve(RIGHTLEG_CURVE.valueOf(enumValue));
				} else if (enumName.equals("HANDS_TOGETHER")) {
					addFramePred.setFrameHandsTogether(HANDS_TOGETHER.valueOf(enumValue));
				} else if (enumName.equals("ARMS_CROSSED")) {
					addFramePred.setFrameArmsCrossed(ARMS_CROSSED.valueOf(enumValue));
				} else if (enumName.equals("BOTH_HANDS_BY_CHEST")) {
					addFramePred.setFrameBothHandsByChest(BOTH_HANDS_BY_CHEST.valueOf(enumValue));
				} else if (enumName.equals("BODY_SYMMETRIC")) {
					addFramePred.setFrameBodySymmetric(BODY_SYMMETRIC.valueOf(enumValue));
				} else if (enumName.equals("FACING")) {
					addFramePred.setFrameFacing(FACING.valueOf(enumValue));
				} else if (enumName.equals("HEIGHT")) {
					addFramePred.setFrameHeight(HEIGHT.valueOf(enumValue));
				} else if (enumName.equals("QUADRANT")) {
					addFramePred.setFrameQuadrant(QUADRANT.valueOf(enumValue));
				} else if (enumName.equals("DIST_CENTER")) {
					addFramePred.setFrameDistCenter(DIST_CENTER.valueOf(enumValue));
				} else if (enumName.equals("SIZE")) {
					addFramePred.setFrameSize(SIZE.valueOf(enumValue));
				} else {
					//Got Transform Predicate
					TransformPredicateInterface transformPredicate = TransformPredicates.parseTransformPredicates(enumName, enumValue);
					if(transformPredicate != null) {
						transforms.add(transformPredicate);
					}
					else {
						System.err.println("ERROR: Unknown Value: " + enumName + " : " + enumValue + "; ");
					}
				}
			}
			if(frameIdx < 0 || frameIdx > framePredHist.size())
			{
				System.out.println("ERROR! Inserting element out of order.");
			}
			framePredHist.add(frameIdx, addFramePred);
			addFramePred = new FramePredicates();
//			System.out.println("gesture has frame predicates:\t" + outString);
//			outString = "";
		}
		
		// TODO: not sure about "true" for multiframe
		Gesture outGesture = new Gesture(false, 
		                                 duration, 
		                                 avgTempo, 
		                                 avgLeftLegStillness, 
		                                 avgRightLegStillness, 
		                                 avgLeftHandStillness, 
		                                 avgRightHandStillness, 
		                                 avgLeftLegTransverse,  
		                                 avgRightLegTransverse, 
		                                 avgLeftHandTransverse, 
		                                 avgRightHandTransverse, 
		                                 avgLeftLegLongitudinal, 
		                                 avgRightLegLongitudinal, 
		                                 avgLeftHandLongitudinal, 
		                                 avgRightHandLongitudinal, 
		                                 avgLeftLegVertical,
		                                 avgRightLegVertical, 
		                                 avgLeftHandVertical, 
		                                 avgRightHandVertical, 
		                                 avgSmooth, 
		                                 avgEnergy, 
		                                 framePredHist, 
		                                 transforms,
		                                 emotion);
		
		return outGesture;
	}
}
