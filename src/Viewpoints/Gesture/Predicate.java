package Viewpoints.Gesture;

public interface Predicate {
	String name();
	String toString();
}
