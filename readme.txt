---BEGIN README---

Thanks for downloading Viewpoints AI.

This download is an experimental build / prototype. Support can be reached at mikhail.jacob@gatech.edu

Requirements

EITHER

1. Mac OSX Lion upwards (Snow Leopard support is experimental). Reasonably recent hardware. RAM and CPU as high as possible (obviously!).

OR

2. Windows 7 upwards. Reasonably recent hardware. RAM and CPU as high as possible (obviously!).

Installation

0. Extract the contents of this archive to a new folder.

DO NOT USE SPACES ANYWHERE IN THE PATH TO THAT FOLDER / DIRECTORY OR IN THE FOLDER / DIRECTORY NAME!!!
BAD THINGS WILL HAPPEN IF YOU DO!!!
YOU HAVE BEEN WARNED!!!

1. Download the latest Java 1.7 Runtime. OpenJDK will NOT work on Mac OSX, instead get the Java SE version from http://www.oracle.com/technetwork/java/javase/downloads/index.html .
2. Download the Anaconda Python package from http://continuum.io/downloads .
3. If on Windows, download the latest Microsoft Kinect drivers from http://go.microsoft.com/fwlink/?LinkId=275588 . For Mac users the Kinect drivers are already bundled with this build.
4. You are done installing this build.

Execution

1. In Terminal (Mac OSX) or Command Prompt (Windows) type the following sequence of commands (without quotations):

"cd <full path to the extracted folder containing this ReadMe.txt>"
"java -cp ./bin/ Viewpoints.Monitoring.ProcessMonitor"

2. Enjoy with a beer.

Support

1. You can reach support at mikhail.jacob@gatech.edu . This is definitely an experimental build and for any bugs / feedback / comments / suggestions, please do not hesitate to shoot us a mail.
2. For more information about the workings of the software, videos, etc., visit http://adam.cc.gatech.edu/?page_id=639 .

Acknowledgement

This build would not have been possible without many great people. Thanks to all of you. You know who you are.
	-ADAM Lab

---END README---